<?php	
	class gear1 extends  Foundry_Core_Service {
		function idFile($fileExt) {

				//You CAN ALSO LOAD THE TABLE VALUES MYRACKTYPES AND MYRACK INITIALLY INTO A PERSISTENT OBJECT AND USE IT, INSTEAD OF CALLING THE DATABSE AGAIN & AGAIN
				$select_handle = self::$DB->query("select * from myracktypes where myext='".$fileExt."'");
				if($select_row = mysql_fetch_assoc($select_handle)) {								
					return $select_row;			
				}else { return 0;}
		}//END Function idFile

        function checkFileIntegrity($theFile,$fileExt) {
            $ftype = exec("file -b -i --mime-type ".$theFile);
            $cstatus=false;
            //check Images
            if(stripos ($ftype,"image/jpeg")===0 ||stripos ($ftype,"image/gif")===0 ||stripos ($ftype,"image/png")===0 ||stripos ($ftype,"image/x-ms-bmp")===0) {
                $cstatus=true;
            }

            //check Documents (excluding .doc, .xls)
            if(stripos ($ftype,"application/pdf")===0 ||stripos ($ftype,"application/msword")===0 ||stripos ($ftype,"application/vnd.ms-excel")===0) {
                $cstatus=true;
            }
            //Check Documents (.doc and .xls)
            $pfinfo = finfo_open(FILEINFO_MIME_TYPE);
            $pftype = finfo_file($pfinfo, $theFile);
            if(stripos ($pftype,"application/msword")===0 ||stripos ($pftype,"application/vnd.ms-excel")===0 ) {
                $cstatus=true;
            }

            //check Videos
            if(stripos ($ftype,"video/x-flv")===0 ||stripos ($ftype,"video/x-ms-asf")===0 ||stripos ($ftype,"video/quicktime")===0 ||
                 stripos ($ftype,"application/vnd.rn-realmedia")===0 ||stripos ($ftype,"video/x-msvideo")===0 ||stripos ($ftype,"video/mpeg")===0)  {
                $cstatus=true;
            }

            //check Audio (excluding normal mp3)
            if(stripos ($ftype,"audio/x-wav")===0 ||stripos ($ftype,"audio/x-aiff")===0 || stripos ($ftype,"audio/mpeg")===0)  {
                $cstatus=true;
            }
            //check Audio mp3
            if($fileExt=='mp3' && stripos ($ftype,"application/octet-stream")===0) {
                $cstatus=true;
            }

            return $cstatus;
        }//END Function checkFileIntegrity
		
		function transformImage($iwidth,$iheight) {			
			$rwidth=display_image_width;
			$rheight=display_image_height;					
			if($iwidth<display_image_width && $iheight<display_image_height) {
				$rwidth=$iwidth;
				$rheight=$iheight;
			}
			if($iwidth<display_image_width && $iheight>display_image_height) {
				$jtrans=$iheight-display_image_height;
				$jtrans = ($jtrans/$iheight)*100;
				$jtrans = ($iwidth * $jtrans)/100;
				$rheight=display_image_height;
				$rwidth=$iwidth-$jtrans;
			}
			if($iwidth>display_image_width && $iheight<display_image_height) {
				$jtrans=$iwidth-display_image_width;
				$jtrans = ($jtrans/$iwidth)*100;
				$jtrans = ($iheight * $jtrans)/100;
				$rwidth=display_image_width;
				$rheight=$iheight-$jtrans;
			}			
			if($iwidth>display_image_width && $iheight>display_image_height) {
				if($iwidth>$iheight) {
					$jtrans=$iwidth-display_image_width;
					$jtrans = ($jtrans/$iwidth)*100;
					$jtrans = ($iheight * $jtrans)/100;
					$rwidth=display_image_width;
					$rheight=$iheight-$jtrans;
				}else {
					$jtrans=$iheight-display_image_height;
					$jtrans = ($jtrans/$iheight)*100;
					$jtrans = ($iwidth * $jtrans)/100;
					$rheight=display_image_height;
					$rwidth=$iwidth-$jtrans;
				}
			}
			return array($rwidth,$rheight);
		}
		
		function listNumbers($id,$attributes,$from,$to,$incr) {
			$return_value = html::select($id,$attributes,"");
			for($inc=$from;$inc<$to;) {				
				$return_value.=html::option($inc,$inc,"");				
				$inc+=$incr;
			}			
			$return_value.= html::select_;				
			return $return_value;
		}	
		
		function checkSecret($password) {
			//if(ctype_alnum($password) && strlen($password)>6 && strlen($password)< 16 && preg_match('`[A-Z]`',$password) && preg_match('`[a-z]`',$password)				&& preg_match('`[0-9]`',$password))
            if(strlen($password) >=6 && strlen($password) < 16)
					return true;
			else
				return false; 
		}
		
		function gender($id,$attributes,$selectedIndex) {
			$return_value = html::select($id,$attributes,"");		
			
			if($selectedIndex==0) {
				$return_value.=html::option(0,"Male",array("selected"=>"selected"));
				$return_value.=html::option(1,"Female","");
			}
			else { 
				$return_value.=html::option(0,"Male","");
				$return_value.=html::option(1,"Female",array("selected"=>"selected"));
			}
			
			$return_value.= html::select_;				
			return $return_value;
		}
		
		function array_unique_multidimensional($input){
			$serialized = array_map('serialize', $input);
			$unique = array_unique($serialized);
			return array_intersect_key($input, $unique);
		}		
		
		
		function getRacks(){
			$racks = array();
			$select_handle = self::$DB->query("select * from myrack ORDER BY rackid ASC");
			while($select_row = mysql_fetch_object($select_handle)) {								
				$racks[] = 	$select_row ;	
			}
			return $racks;
		}

        function createThumbs( $imageName,$imagePath, $thumbWidth,$thumbHeight) {
                    $pos = strrpos($imageName, ".");
                    $imgFName = substr($imageName,0,$pos);
                    $fileExt = substr($imageName,$pos+1);
                    if($fileExt=="gif" ||$fileExt=="jpg" ||$fileExt=="png") {
                            $thumbName=$imgFName."_thumb".".".$fileExt;
                            $simg="jpg";
                            // load image and get image size
                            switch($fileExt) {
                                case 'gif':
                                    $simg = imagecreatefromgif($imagePath."/".$imageName);
                                    break;
                                case 'jpg':
                                    $simg = imagecreatefromjpeg($imagePath."/".$imageName);
                                    break;
                                case 'png':
                                    $simg = imagecreatefrompng($imagePath."/".$imageName);
                                    break;
                            }
                            $width = imagesx( $simg );
                            $height = imagesy( $simg );

                            // calculate thumbnail size
                            $new_width = $thumbWidth;
                            $new_height = $thumbHeight;
                            //$new_height = floor( $height * ( $thumbWidth / $width ) );
                            // create a new temporary image
                            $tmp_img = imagecreatetruecolor( $new_width, $new_height );

                            // copy and resize old image into new image
                            imagecopyresized( $tmp_img, $simg, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

                            // save thumbnail into a file
                            imagejpeg( $tmp_img, $imagePath."/".$thumbName );
                    }//Check to see if the media is a Image
        }//END Functionc createThumbs

        function generatePassword($length=9, $strength=0) {
            $vowels = 'aeuy';
            $consonants = 'bdghjmnpqrstvz';
            if ($strength & 1) {
                $consonants .= 'BDGHJLMNPQRSTVWXZ';
            }
            if ($strength & 2) {
                $vowels .= "AEUY";
            }
            if ($strength & 4) {
                $consonants .= '23456789';
            }
            if ($strength & 8) {
                $consonants .= '@#$%';
            }

            $password = '';
            $alt = time() % 2;
            for ($i = 0; $i < $length; $i++) {
                if ($alt == 1) {
                    $password .= $consonants[(rand() % strlen($consonants))];
                    $alt = 0;
                } else {
                    $password .= $vowels[(rand() % strlen($vowels))];
                    $alt = 1;
                }
            }
            return $password;
        }//END Function generatePassword
		
		function voidme() {
			return array('and');
		}//END Function voidme
		
		function addCdataTag($xmlStr, $tagsNeedCdata) {
			foreach( $tagsNeedCdata as $tag )  {
				$xmlStr = str_replace("<".$tag.">", "<".$tag."><![CDATA[", $xmlStr);
				$xmlStr = str_replace("</".$tag.">", "]]></".$tag.">", $xmlStr);
			}
			return $xmlStr;
		}//END Function addCdataTag
		
		function replaceXmlSpecialChars($xmlStr) {
			$xmlStr = str_replace('&', '&amp;', $xmlStr);
			return $xmlStr;
		}//END Function replaceXmlSpecialChars
		
		function replaceXmlFixSpeicalChars($str) {
			$str = str_replace('愦灭;', '&', $str);
			return $str;
		}//END Function replaceXmlFixSpeicalChars
		
		function parseXmlFile($xmlFile, $tagsNeedCdata) {
			$response['error'] = "";
			if( $xmlFile['error'] == 0 )  
			{
				$xmlStr = (string)file_get_contents($xmlFile['tmp_name']);
				//$xmlStr = gear1::addCdataTag($xmlStr, $tagsNeedCdata);
				$xmlStr = gear1::replaceXmlSpecialChars($xmlStr);
				
				$xmlObj = simplexml_load_string($xmlStr);
				//$xmlObj = simplexml_load_file($xmlFile['tmp_name']);
				
				if( $xmlObj )  {
					$response['xmlObj'] = $xmlObj;
				}	else	{
					$response['error'] = "XML Parsing error";
				}
			}	else	{
				$response['error'] = "File is not uploaded successfully.";
			}
			
			return $response;
		}//END Function parseXmlFile
	}
?>