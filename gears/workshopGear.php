<?php
/**
 * Created by Vanitha.
 * User: 10046078
 * Date: 2/3/2015
 * Time: 4:11 PM
 */

class workshopGear extends Foundry_Local_Broker
{

    static function __process($request)
    {

        if(!empty($request[3]))
            $call3 = ucfirst($request[1]).'_'.ucfirst($request[2]).'_'.ucfirst($request[3]);
        if(!empty($request[2]))
            $call2 = ucfirst($request[1]).'_'.ucfirst($request[2]);
        if(!empty($request[1]))
            $call1 = ucfirst($request[1]);

        if(method_exists('workshopGear',$call3))
            self::$call3($request);
        elseif(method_exists('workshopGear',$call2))
            self::$call2($request);
        elseif(method_exists('workshopGear',$call1))
            self::$call1($request);
        else
            self::$request[1]($request);

    }



    function Manage_Modules_Add_New($request)
    {
        $smarty = Foundry_RObjects::get("smarty");
        if(Mold_SAE::GA()['type'] > 1) {
            $data['module'] = ucfirst($request[1]);
            $data['action'] = ucfirst($request[2]);
            $data['id'] = '';
            $data['actions'] = self::get_actions();

            $smarty->assign($data);
            $smarty->display(TEMPLATES_DIR . DS . 'workshop/add_module.tpl');
        }else{
            $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }
    }


    function Manage_Modules($request)
    {
        $data['module'] = ucfirst($request[1]);
        $smarty = Foundry_RObjects::get("smarty");

        if(Mold_SAE::GA()['type'] > 1)
        {
            //get base record for the module
            $select_handle = self::$DB->query("select * from modules WHERE deleted=0 and is_admin  <> 1 ORDER BY name ASC");
            $records = self::$DB->fetchAll($select_handle);

            foreach($records as $record)
                $modules[$record['name']] = $record['name'];

            //GET Layout FOR Module
            $data['total_num'] = count($records);
            $data['actions'] = self::get_actions();
            $data['modules_actions'] = self::get_modules_actions();
            $data['modules'] = $modules;

            //get current page num
            $page_num = ($_GET['page_num']) ? $_GET['page_num'] : 1;
            $data['page_num'] = $page_num;

            //create pagination links
            $page_id = gear3::createPaging($records);

            $data['total_pages'] = gear3::totalPages($page_id, PAGE_SIZE);
            $data['records'] = gear3::sendPaging($page_id, PAGE_SIZE, $page_num);
            $data['PROCESS_OPC_LINK'] = PROCESS_OPC_LINK;

            $smarty->assign($data);

            $smarty->display(TEMPLATES_DIR . DS . 'workshop/alter_modules.tpl');

        }else{
            $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }
    }

    function Manage_Modules_Relate_Modules($request)
    {
        echo 'ddd';
    }

    function Manage_Modules_Save()
    {
        $data = $_POST;

        if(Mold_SAE::GA()['type'] > 1 && $data['name'] != '') {
            $values = array();

            $values['name'] = ucfirst(strtolower(str_replace(' ','_',$data['name'])));
            $values['label'] = $data['label'];
            $values['description'] = $data['description'];
            $values['is_active'] = $data['is_active'];
            $values['modified_by'] = Mold_SAE::GA()['id'];
            $values['is_admin'] = $data['is_admin'];

            if($data['id'] == '') {
                $values['date_entered'] = date('Y-m-d H:i:s', time());
                $values['created_by'] = Mold_SAE::GA()['id'];
            }
            
            $fields = array_keys($values);
            $values = array_values($values);

            if($data['id'] == '') {
                $id = self::$DB->insert('modules', $fields, $values);
                $actions = self::get_actions();
                for($i=0; $i < count($data['actions']);$i++)
                    $data['action'][$i] = $actions[$data['actions'][$i]];
                Foundry_Curl::doCurl(PROCESS_CRMC_LINK . "/New_Module@workshopGear", $data, TRUE);
            }elseif($data['id'] != '' && $data['delete'] != 1){
                $where = "id = " .$data['id'];
                $id=$data['id'];
                self::$DB->update('modules', $fields, $values, $where);
                self::$DB->query("DELETE FROM modules_actions WHERE modules_id=".$data['id']);
            }elseif($data['id'] != '' && $data['delete'] == 1) {
                self::$DB->query("DELETE FROM modules_actions WHERE modules_id=".$data['id']);
                self::$DB->query("DELETE FROM modules WHERE name='{$data['name']}'");
                self::$DB->query("DELETE FROM users_securiy WHERE modules_id={$data['id']}");
                self::$DB->query("DELETE FROM departments_securiy WHERE modules_id={$data['id']}");
                self::$DB->query("DELETE FROM roles_securiy WHERE modules_id={$data['id']}");
                self::$DB->query("DELETE FROM teams_securiy WHERE modules_id={$data['id']}");
                Foundry_Curl::doCurl(PROCESS_CRMC_LINK . "/Delete_Module@workshopGear", $data, TRUE);
            }

            if($data['delete'] != 1) {
                foreach ($data['actions'] as $action) {
                    $fields = array('modules_id', 'actions_id', 'is_active', 'deleted');
                    $values = array($id, $action, 1, 0);
                    self::$DB->insert('modules_actions', $fields, $values);
                }
            }

            $request[0] = 'workshop';
            $request[1] = 'Manage_Modules';
            $request[2] = 'Alter';
            self::Manage_Modules($request);
        }
    }

    function Manage_Fields($request)
    {

        $smarty = Foundry_RObjects::get("smarty");
        if(Mold_SAE::GA()['type'] == 3) {
            $data['module'] = ucfirst($request[2]);
            $data['action'] = ucfirst($request[3]);
            $data['id'] = '';

            //get all dropdown values
            $data['dropdowns'] = self::get_dropdown_lists();

            foreach($data['dropdowns'] as $dropdown){
                if($dropdown != '') {
                    $select_handle = self::$DB->query("select value,label from Dropdown_Values, Dropdown_Options  WHERE Dropdown_Options.deleted=0 AND name='{$dropdown}' AND parent_id=Dropdown_Options.id");
                    $results = self::$DB->fetchAll($select_handle);
                    $data['dropdown_values'][$dropdown]['']='';
                    foreach($results as $record) {
                            $data['dropdown_values'][$dropdown][$record['value']]=$record['label'];
                    }
                }
            }

            //get base record for the module
            $fields =  json_decode(Foundry_Curl::doCurl(PROCESS_CRMC_LINK."/Get_Fields@workshopGear",$data));
            for($i=0; $i < count($fields) ; $i++)
            {
                foreach($fields[$i] as $key=>$value)
                    $records[$i][$key] = $value;
            }

            //GET Layout FOR Module
            $data['total_num'] = count($records);

            //get current page num
            $page_num = ($_GET['page_num']) ? $_GET['page_num'] : 1;
            $data['page_num'] = $page_num;

            //create pagination links
            $page_id = gear3::createPaging($records);

            $data['total_pages'] = gear3::totalPages($page_id, PAGE_SIZE);
            $data['records'] = gear3::sendPaging($page_id, PAGE_SIZE, $page_num);

            $smarty->assign($data);
            $smarty->display(TEMPLATES_DIR . DS . 'workshop/alter_fields.tpl');
        }else{
            $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }
    }


    function Manage_Fields_Save()
    {
        $data = $_POST;
        $data['modified_by'] = Mold_SAE::GA()['id'];
        $data['created_by'] = Mold_SAE::GA()['id'];

        if(Mold_SAE::GA()['type'] == 3) {

            echo Foundry_Curl::doCurl(PROCESS_CRMC_LINK."/Save_Module_Field@workshopGear",$data);

            $request[0] = 'workshop';
            $request[1] = 'Manage_Fields';
            $request[2] = $data['module'];
            self::Manage_Fields($request);
        }else{
            $smarty = Foundry_RObjects::get("smarty");
            $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }
    }

    function Manage_Fields_Add_New($request)
    {
        $smarty = Foundry_RObjects::get("smarty");
        if(Mold_SAE::GA()['type'] == 3) {
            $data['module'] = ucfirst($request[3]);
            $data['id'] = '';

            $types_list = array(
                    ''=> '',
                    'text' => 'Text',
                    'bool' => 'Bool',
                    'relation' => 'Relation',
                    'textarea' => 'Textarea',
                    'dropdown' => 'Dropdown',
                    'multiselect' => 'Multiselect',
                    'date' => 'Date',
                    'datetime' => 'DateTime',
                    'time' => 'Time',
                    'address'=> 'Address'
            );
            $data['types'] = $types_list;

            //get all dropdown values
            $data['dropdown_lists'] = self::get_dropdown_lists();

            //get all modules
            $data['modules'] = array_merge( array('' => ''), self::get_modules());

            $smarty->assign($data);
            $smarty->display(TEMPLATES_DIR . DS . 'workshop/add_fields.tpl');
        }else{
            $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }
    }


    function Manage_Layouts($request)
    {
        $smarty = Foundry_RObjects::get("smarty");
        $layouts = json_decode(Foundry_Curl::doCurl(PROCESS_CRMC_LINK."/Get_Layouts@workshopGear",$request));
        $layout_views = array_keys($layouts);

        $select_handle = self::$DB->query("select A.name from modules_actions as MA, actions AS A WHERE modules_id=(select id from modules WHERE name='{$request[2]}')
  AND MA.deleted=0 and MA.is_active=1 AND MA.actions_id=A.id");

        $results = self::$DB->fetchAll($select_handle);
        foreach($results as $ma)
            $modules_actions[] = $ma['name'];

        foreach($layouts as $key => $value)
        {
            if (!in_array($key, $modules_actions) && $key != 'Search')
                unset($layouts->$key);
        }

        $data['module'] = $request[2];
        $data['layouts'] = $layouts;
        $smarty->assign($data);
        $smarty->display(TEMPLATES_DIR . DS . 'workshop/alter_layouts.tpl');
    }


    function Manage_Layouts_Save()
    {
        $request[0] = 'workshop';
        $request[1] = 'Manage_Layouts';
        $request[2] = $_POST['module'];
        Foundry_Curl::doCurl(PROCESS_CRMC_LINK."/Save_Layouts@workshopGear",$_POST, TRUE);
        self::Manage_Layouts($request);
    }

    function Access($request)
    {
        if($request[2] != 'Users')
            $select_handle = self::$DB->query("select id,name from {$request[2]} WHERE deleted=0 ORDER by name ASC");
        else
            $select_handle = self::$DB->query("select id, concat(name,' ' ,last_name) as name from {$request[2]} WHERE deleted=0 ORDER by name ASC");

        $records = self::$DB->fetchAll($select_handle);
        $access = array(''=>'');
        foreach($records as $record)
            $access[$record['id']] = $record['name'];

        $smarty = Foundry_RObjects::get("smarty");
        $data['accessType'] = $request[2] ;
        $data['accessList'] = $access ;
        $data['accessListID'] = '' ;

        $smarty->assign($data);
        $smarty->display(TEMPLATES_DIR . DS . 'workshop/access.tpl');
    }



    function Access_GetAccess($request)
    {
        if($request[3] != 'Users')
            $select_handle = self::$DB->query("select id,name from {$request[3]} WHERE deleted=0 ORDER by name ASC");
        else
            $select_handle = self::$DB->query("select id, concat(name,' ' ,last_name) as name from {$request[3]} WHERE deleted=0 ORDER by name ASC");
        $records = self::$DB->fetchAll($select_handle);
        $access = array(''=>'');
        foreach($records as $record)
            $access[$record['id']] = $record['name'];

        $smarty = Foundry_RObjects::get("smarty");
        $data['accessType'] = $request[3] ;
        $data['accessList'] = $access ;
        $data['accessListID'] = $request[4] ;
        $data['module_actions'] = self::get_modules_actions() ;
        $data['actions'] = self::get_actions();

        if($request[3] == 'Users')
        {
            $select_handle = self::$DB->query("select count(users_id) as user, roles_id as role , teams_id as team ,departments_id as department
            from Users
            LEFT JOIN departments_security on departments_id=default_department
            LEFT JOIN teams_security on teams_id=default_team
            LEFT JOIN roles_security on roles_id=default_role
            LEFT JOIN users_security on users_id=Users.id
            where Users.id=".$request[4]);
            $records = self::$DB->fetchAll($select_handle);
            if($records[0]['user'] == 0 && $records[0]['role'] > 0) {
                $data['current_access']['type'] = 'Role';
                $data['current_access']['id'] = $records[0]['role'];
            }elseif($records[0]['user'] == 0 && $records[0]['team'] > 0) {
                $data['current_access']['type'] = 'Team';
                $data['current_access']['id'] = $records[0]['team'] ;
            }elseif($records[0]['user'] == 0 && $records[0]['department'] > 0) {
                $data['current_access']['type'] = 'Department';
                $data['current_access']['id'] = $records[0]['department'];
            }
        }

        if($request[3] == 'Roles')
            $where = "where default_role={$data['accessListID']}";
        if($request[3] == 'Teams')
            $where = "where default_team={$data['accessListID']}";
        if($request[3] == 'Departments')
            $where = "where default_department={$data['accessListID']}";


        $select_handle = self::$DB->query("select Users.id,concat(Users.name,' ' ,Users.last_name) as name,
                (select count(departments_id) from departments_security where departments_id=default_department) as department_count,
                (select count(teams_id) from teams_security where teams_id=default_team) as team_count,
                (select count(roles_id) from roles_security where roles_id=default_role) as role_count,
                (select count(users_id) from users_security where users_id=Users.id) as user_count from Users {$where}");
        $records = self::$DB->fetchAll($select_handle);
        $users = array('' => '');

        if($request[3] == 'Roles')
        {
            foreach($records as $record) {
                if($record['user_count'] == 0)
                    $users[$record['id']] = $record['name'];
            }
            $data['users'] = $users;
        }

        if($request[3] == 'Teams')
        {
            foreach($records as $record) {
                if($record['user_count'] == 0 && $record['role_count'] == 0)
                    $users[$record['id']] = $record['name'];
            }
            $data['users'] = $users;
        }

        if($request[3] == 'Departments')
        {
            foreach($records as $record) {
                if($record['user_count'] == 0 && $record['role_count'] == 0 && $record['team_count'] == 0)
                    $users[$record['id']] = $record['name'];
            }
            $data['users'] = $users;
        }

        $smarty = Foundry_RObjects::get("smarty");

        $request[3] = strtolower($request[3]);
        $select_handle = self::$DB->query("select id, label from modules WHERE is_admin <> 1 ORDER BY name ASC");
        $records = self::$DB->fetchAll($select_handle);
        foreach($records as $record)
            $module_list[$record['id']] = $record['label'];
        $data['modules'] = $module_list;

        $select_handle = self::$DB->query("select * from {$request[3]}_security WHERE {$request[3]}_id={$request[4]} AND deleted=0");
        $records = self::$DB->fetchAll($select_handle);
        foreach($records as $record)
            $security_list[$record['modules_id']][$record['actions_id']] = $record['security_levels_id'];
        $data['security_list'] = $security_list;

        $select_handle = self::$DB->query("select level, name from security_levels WHERE deleted=0");
        $records = self::$DB->fetchAll($select_handle);
        foreach($records as $record)
            $security_levels[$record['level']] = $record['name'];
        $data['security_levels'] = $security_levels;

        $smarty->assign($data);
        $smarty->display(TEMPLATES_DIR . DS . 'workshop/access.tpl');
    }

    function Access_Save($request)
    {
        $accessType =strtolower($_POST['accessType']);
        foreach($_POST['mid'] as $mid)
        {
            self::$DB->query("DELETE FROM {$accessType}_security WHERE modules_id={$mid} AND {$accessType}_id={$_POST['accessListID']}");
            foreach($_POST['maction' . $mid] as $key => $action_id)
            {
                self::$DB->query("INSERT INTO {$accessType}_security(`{$accessType}_id`,`modules_id`,`actions_id`,`security_levels_id`) VALUES({$_POST['accessListID']},
{$mid},{$_POST['maction' . $mid][$key]},{$_POST['levels' . $mid][$key]})");
            }
        }

        $request[0] = 'workshop';
        $request[1] = 'Access';
        $request[2] = 'GetAccess';
        $request[3] = $_POST['accessType'];
        $request[4] = $_POST['accessListID'];
        self::__process($request);
    }

    function Access_Revoke($request)
    {
        $accessType =strtolower($request[3]);
        self::$DB->query("DELETE FROM {$accessType}_security WHERE {$accessType}_id={$request[4]}");
        $request[0] = 'workshop';
        $request[1] = 'Access';
        $request[2] = 'GetAccess';
        $request[3] = $request[3];
        $request[4] = $request[4];
        self::__process($request);
    }

    function Access_CopyAccess($request)
    {
        $accessType =strtolower($request[3]);
        $select_handle = self::$DB->query("select * from {$accessType}_security WHERE {$accessType}_id={$request[4]} AND deleted=0");
        $records = self::$DB->fetchAll($select_handle);
        foreach($records as $record)
        {
            self::$DB->query("INSERT INTO users_security(`users_id`,`modules_id`,`actions_id`,`security_levels_id`) VALUES({$request[5]}, {$record['modules_id']},
                              {$record['actions_id']},{$record['security_levels_id']})");
        }

        $request[0] = 'workshop';
        $request[1] = 'Access';
        $request[2] = 'GetAccess';
        $request[3] = 'Users';
        $request[4] = $request[5];
        self::__process($request);
    }

    function Dropdown_Manager($request)
    {
        $data['dropdowns'] = self::get_dropdown_lists();

        $select_handle = self::$DB->query("select value,label from Dropdown_Values WHERE deleted=0 and parent_id=1");
        $results = self::$DB->fetchAll($select_handle);
        foreach($results as $ma)
            $dropdown_values[$ma['value']] = $ma['label'];

        $data['dropdown_values'] = $dropdown_values;

        $smarty = Foundry_RObjects::get("smarty");
        $smarty->assign($data);
        $smarty->display(TEMPLATES_DIR . DS . 'workshop/dropdown_manager.tpl');
    }

    function Dropdown_Manager_Load_Options($request)
    {
        if($request != 'false') {
            $select_handle = self::$DB->query("select value,label from Dropdown_Values, Dropdown_Options  WHERE Dropdown_Options.deleted=0 AND name='{$request}' AND parent_id=Dropdown_Options.id");
            $results = self::$DB->fetchAll($select_handle);
            $html = '';
            foreach ($results as $ma) {
                $html .= '<tr><td>' . $ma['value'] . '</td>' .
                    '<td><input type="text" value="' . $ma['label'] . '" id="' . $ma['value'] . '" name="' . $ma['value'] . '">' .
                    '<input type="hidden" value="'.$request.'##'.$ma['value'].'"  id="del_' . $ma['value'] . '"  name="del_' . $ma['value'] . '">'.
                    '<a  class="remove-search-field" onclick=deletDropdownOptions("del_' . $ma['value'] . '") href="javascript:;"><i class="fa fa-times"></i></a>'.
                    '</td>' .
                    '</tr>';
            }
            $html .= '<tr><td><input type="text" value="" id="key" name="key"></td><td><input type="text" value="" id="label" name="label"></td></tr>' .
                '<tr><td colspan="2"><input type="hidden" value="' . $request . '" id="parent" name="parent"><span class="moon-but-1 btn btn-primary" id="add_options" ' .
                'onclick="saveDropdownOptions()">Save</span></td></tr>';

            echo "CALLBACKS" . UNIQUE_SEPERATOR . "$('#dropdown_values').html('$html')";
        }else{
            echo "CALLBACKS" . UNIQUE_SEPERATOR . "$('#dropdown_values').html('')";
        }
    }

    function Dropdown_Manager_New($request)
    {
        $data = array();

        $select_handle = self::$DB->query("select count(name) as count from Dropdown_Options WHERE deleted=0 AND name='{$_REQUEST['title']}'");
        $count = self::$DB->fetchAll($select_handle);

        if($count[0]['count'] > 0)
        {
            $data['errors'][] = 'Duplicate entry found, did not save record.';
        }else{
            self::$DB->query("INSERT INTO Dropdown_Options(name) VALUES('{$_REQUEST['title']}')");
        }

        self::Dropdown_Manager($request);
    }

    function Dropdown_Manager_Add_Options($request){

        foreach($_REQUEST as $key => $input)
        {
            if($key != 'core_request' && $key != 'REQP' && $key != 'parent')
            {
                if($key == 'key' && $input != 'false')
                {
                    self::$DB->query("INSERT INTO Dropdown_Values(parent_id,value,label) VALUES((select id from Dropdown_Options WHERE name='{$_REQUEST['parent']}'), '{$_REQUEST['key']}','{$_REQUEST['label']}')");
                }elseif(($key != 'key' && $key != 'label') && $key != $input){
                    self::$DB->query("UPDATE Dropdown_Values SET  label='{$_REQUEST[$key]}' WHERE parent_id=(select id from Dropdown_Options WHERE name='{$_REQUEST['parent']}') AND value='{$key}'");
                }
            }
        }
        self::Dropdown_Manager_Load_Options($_REQUEST['parent']);
    }

    function Dropdown_Manager_Delete_Options($request)
    {
        $parent = explode('##',$request);
        self::$DB->query("DELETE FROM Dropdown_Values WHERE parent_id=(select id from Dropdown_Options WHERE name='{$parent[0]}') AND value='{$parent[1]}'");
        self::Dropdown_Manager_Load_Options($parent[0]);
    }


    function get_dropdown_lists()
    {
        //get all dropdown values
        $select_handle = self::$DB->query("select name from Dropdown_Options WHERE deleted=0 ORDER by name asc;");
        $dropdowns = self::$DB->fetchAll($select_handle);
        $dropdown_lists = array(''=>'');
        foreach($dropdowns as $dropdown)
        {
            $dropdown_lists[$dropdown['name']] = $dropdown['name'];
        }
        return $dropdown_lists;
    }




    function get_modules()
    {
        $select_handle = self::$DB->query("select name, label from modules WHERE is_admin=0 ORDER BY name ASC");
        $modules = self::$DB->fetchAll($select_handle);
        foreach($modules as $module)
            $module_list[$module['name']] = $module['label'];
        return $module_list;
    }

    function get_actions()
    {
        //get all actions
        $select_handle = self::$DB->query("select id,name from actions WHERE deleted=0 and is_active=1");
        $results = self::$DB->fetchAll($select_handle);
        foreach($results as $aa)
            $actions[$aa['id']] = $aa['name'];
        return $actions;
    }

    function get_modules_actions()
    {
        //get all module actions
        $select_handle = self::$DB->query("select * from modules_actions WHERE deleted=0 and is_active=1");
        $results = self::$DB->fetchAll($select_handle);
        foreach($results as $ma)
            $modules_actions[$ma['modules_id']][] = $ma['actions_id'];

        return $modules_actions;
    }

    function getModuleAction_JSON($request)
    {
        //get all module actions
        $select_handle = self::$DB->query("select A.name from modules_actions as MA, actions AS A WHERE modules_id=(select id from modules WHERE name='{$request['module']}')
  AND MA.deleted=0 and MA.is_active=1 AND MA.actions_id=A.id");
        $results = self::$DB->fetchAll($select_handle);
        foreach($results as $ma)
            $modules_actions[] = $ma['name'];

       array_push($modules_actions,'Search');

        echo json_encode($modules_actions);
    }



}//END Class workshopGear


?>