<?php	
	class gear3 extends Foundry_Core_Service {
		function createPaging($page_data) {
            $page_id=uniqid();
            Foundry_Memory::set($page_id,$page_data);
            return $page_id;
		}//END function createPaging
		
		function totalPages($page_id,$page_size) {
			$page_data = Foundry_Memory::get($page_id);
			$ceil = ceil(count($page_data) / $page_size);
			if($ceil > 1)
				$total_pages=$ceil;
			else
				$total_pages = 1;
			return $total_pages;
		}//END function totalPages
		
		function sendPaging($page_id,$page_size,$page_num) {
            $page_data = Foundry_Memory::get($page_id);
            //for removing large data from session so session work perfeclty and does not get data limit
            Foundry_Memory::dealloc($page_id);
            $start_pos = ($page_size * $page_num)-$page_size;
            return array_slice($page_data,$start_pos,$page_size);
		}//END function sendPaging



        function diffDateTime($ts1,$ts2) {

            $dt1 = new DateTime();
            $dt1->setTimestamp($ts1);
            $dt2 = new DateTime();
            $dt2->setTimestamp($ts2);
            $diff = $dt2->diff($dt1);
            $hrs = $diff->format('%h');
            $days =$diff->format('%d');
            $months =$diff->format('%m');
            $years =$diff->format('%y');
            $RESP='';
            if($years>0){
                if($years>1)
                    $RESP= $years ." years ago";
                else
                    $RESP= $years ." year ago";
            }else {
                if($months>0) {
                    if($months>1)
                        $RESP= $months ." months ago";
                    else
                        $RESP= $months ." month ago";

                }else {
                    if(days>0) {
                        if($days>1)
                            $RESP= $days ." days ago";
                        elseif($days==7)
                            $RESP= " 1 week ago";
                        else
                            $RESP= $days ." day ago";
                    }else {
                        if($hrs>0) {
                            if($hrs>1)
                                $RESP= $hrs ." hours ago";
                            else
                                $RESP= $hrs ." hour ago";
                        }else {
                            $RESP= " recently";
                        }

                    }

                }
            }

            echo $RESP;

        }//END Function diffDateTime

        function multiArrMerge($mKey,$mValue,$sKey,&$sArray,$cisArr,&$result) {
            foreach($sArray as $tKey => &$tValue) {
                if($cisArr) {
                    if(is_array($tValue)) {
                        if($sKey==$tKey) {
                            if (array_key_exists($mKey,$tValue)==FALSE) {
                                $tValue[$mKey]=$mValue;
                                $result=1;
                            }
                        }else {
                            self::multiArrMerge ($mKey,$mValue,$sKey,$tValue,$cisArr,$result);
                        }
                    }
                }
            }
            return $result;
        }//END Function multiArrMerge
	
	}//END Class gear3
	

?>