<?php
/**
 * Created by Vanitha.
 * User: 10046078
 * Date: 2/3/2015
 * Time: 4:11 PM
 */

class actionGear extends Foundry_Local_Broker {

    static function __process($request)
    {




        apc_store('fook', 'adaddasd');
        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);
        if(method_exists('actionGear',$action)) {
            self::$action($request);
        }else{
            $class = 'Mold_' . $module;
            if (file_exists(CORE_PATH . DS . 'molds/Mold_' . $module . '.php') && (class_exists($class) && method_exists($class, $action))) {
                $class::$action($request);
            }
        }

    }



    function Lists($request)
    {
        session_start();
        print_r($_POST);
        echo '****';
        echo $_SESSION['search_txt'];

        if (apc_exists('fook')) {
             echo apc_fetch('fook');
         } else {
             echo 'BBBBB';
             apc_store('fook', 'GGGGGGG');
          }

        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);
        if(isset($_REQUEST['page_num']))
            $page_num = ($_REQUEST['page_num'] - 1);
        else
            $page_num = 0;

        if(!Mold_AC::validateAccess($module, $action, 5) && Mold_AC::validateAccess($module, $action, 5) != 'No Access' ) {

            if (!file_exists(CORE_PATH . DS . 'molds/Mold_' . $module . '.php') || (class_exists('Mold_' . $module) && !method_exists('Mold_' . $module, $action))) {

                $data['module'] = $module;
                $data['action'] = $action;

                if(!empty($_POST)) {
                    echo '&&&&';
                    $data['search_txt'] = Mold_Mis::renderSearchString($_POST, $module);
                    $_SESSION['search_txt'] = $data['search_txt'];
                }else {
                    echo '%%%';
                    $data['search_txt'] = $_SESSION['search_txt'];
                }
                echo $_SESSION['search_txt'];
                if (Mold_AC::validateAccess($module, $action, 1)) { //Global Access
                    $data['where'] = '';
                } elseif (Mold_AC::validateAccess($module, $action, 2)) { // Department Level Access
                    $data['where'] = " AND department_id IN (" . implode(",",Mold_SAE::RA()['departments']) . ")";
                } elseif (Mold_AC::validateAccess($module, $action, 3)) {  // Team Level Access
                    $data['where'] = " AND team_id IN (" . implode(",",Mold_SAE::RA()['teams']) . ")";
                } elseif (Mold_AC::validateAccess($module, $action, 4)) {  // User Level Access
                    $data['where'] = ' AND created_by=' . Mold_SAE::RA()['id'];
                }

                $where = '';

                if ($data['where'] != '') {
                    $where = $data['where'];
                }

                if ($data['search_txt'] != "") {
                    $search_txt = mysql_real_escape_string(str_replace(array('[', ']'), array('', ''), $data['search_txt']));
                    $where .= ' AND ('. $data['search_txt'] .')';
                }

                //get base record for the module
                $records = self::$PDO->FetchAll($module,$where, $page_num, PAGE_SIZE);

                foreach($records as $key=> $record)
                {
                    if( Mold_AC::validateRecordAccess($module, 'View', $record) == 1)
                        $records[$key]['view_access'] = 1;
                    if( Mold_AC::validateRecordAccess($module, 'Edit', $record) == 1)
                        $records[$key]['edit_access'] = 1;
                    if( Mold_AC::validateRecordAccess($module, 'Delete', $record) == 1)
                        $records[$key]['delete_access'] = 1;
                }

                //GET Layout FOR Module
                $data['search'] = Mold_Mis::getSearchLayout($module);
                $data['lists'] = Mold_Mis::getListsLayout($module);

                $data['total_num'] = self::$PDO->GetCount($module,$where);

                $data['total_pages'] = gear3::totalPages($data['total_num'], PAGE_SIZE);

                $data['records'] =  $records;


            } else {
                $class = 'Mold_' . $module;
                $data = $class::$action($request);
            }

            $smarty = Foundry_RObjects::get("smarty");
            $smarty->assign($data);

            if (file_exists(TEMPLATES_DIR . DS . $module . '/' . $action . '.tpl'))
                $smarty->display(TEMPLATES_DIR . DS . $module . '/' . $action . '.tpl');
            else
                $smarty->display(TEMPLATES_DIR . DS . 'common/' . $action . '.tpl');


        }else{
            echo 'No Access';
        }

    }

    function View($request)
    {

        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);
        $id = $request[2];

        $data['module']= $module;
        $data['action']= $action;
        $data['id']= $id;

        if( $id != "" ){
                $data['record'] =  self::$PDO->FetchByID($module, $id);
        }else {
            $request[0] = $module;
            $request[1] = 'View';

            self::View($request);
        }
        $smarty = Foundry_RObjects::get("smarty");
        if(Mold_AC::validateRecordAccess($module, $action, $record)) {

            $data['layout'] = Mold_Mis::getViewLayout($module);

            $smarty->assign($data);

            if (file_exists(TEMPLATES_DIR . DS . $module . '/' . $action . '.tpl'))
                $smarty->display(TEMPLATES_DIR . DS . $module . '/' . $action . '.tpl');
            else
                $smarty->display(TEMPLATES_DIR . DS . 'common/View.tpl');
        }else{
           $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }
    }


    function Edit($request)
    {
        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);
        $id = $request[2];

        $data['module']= $module;
        $data['action']= $action;
        $data['id']= $id;

        $smarty = Foundry_RObjects::get("smarty");

        if( $id != "" )
        {
            $data['record'] =  self::$PDO->FetchByID($module, $id);
        }else {
            $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }

        if(Mold_AC::validateRecordAccess($module, $action, $data['record'])) {

            $data['layout'] = Mold_Mis::getEditLayout($module);

            $smarty->assign($data);

            if(file_exists(TEMPLATES_DIR.DS.$module.'/'.$action.'.tpl'))
                $smarty->display(TEMPLATES_DIR.DS.$module.'/'.$action.'.tpl' );
            else
                $smarty->display (TEMPLATES_DIR.DS.'common/Edit.tpl' );
        }else{
            $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }

    }

    function Delete($request)
    {
        $request[0] = ucfirst($request[0]);
        $request[1] = 'Lists';
        self::$PDO->RemoveByID($request[0], $request[2]);
        self::Lists($request);
    }

    function Save($request)
    {
        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);
        $id = $_POST['id'];

        if($id == "" )  {
            $values = $_REQUEST;
            unset($values['core_request']);
            unset($values['id']);

            $values['date_entered'] = date('Y-m-d H:i:s', time());
            $values['created_by'] = Mold_SAE::RA()['id'];
            $values['modified_by'] = Mold_SAE::RA()['id'];
            $values['department_id'] = Mold_SAE::RA()['default_department'];
            $values['team_id'] = Mold_SAE::RA()['default_team'];

            $fields = array_keys($values);

            $id = self::$PDO->Insert($module,$fields,$values);
        }else{
            $values = $_REQUEST;
            unset($values['core_request']);

            $values['modified_by'] = Mold_SAE::RA()['id'];
            $fields = array_keys($values);

            $where = "id = :id";

            self::$PDO->Update($module, $fields,$values,$where);

        }

        $request[0] = $module;
        $request[1] = 'View';
        $request[2] = $id;

        self::View($request);
    }

}//END Class actionGear


?>