<?php	
	class securityGear extends Foundry_Core_Service {

        function invokeCS() {
            return md5($_SERVER['HTTP_USER_AGENT'].Foundry_useful::sealit("moon**crm").$_SERVER['REMOTE_ADDR']);
        }


        function getUserSecurity($user_data = array(), $type='JSON')
        {
            if(empty($user_data))
                $user_data = $_REQUEST;

            $user_id =  $user_data['id'];
            $default_role = $user_data['role'];
            $default_team = $user_data['team'];
            $default_department = $user_data['department'];

            //$user_id =   Mold_SAE::RA()['id'];

            //First check user level access
            $select_handle = self::$DB->query("select security_levels_id as Level, modules_id, modules.name as Modules, actions_id, actions.name as Actions, modules.is_admin from users_security
                                                LEFT JOIN modules on modules_id=modules.id
                                                LEFT JOIN actions on actions_id =actions.id WHERE modules.is_active=1 AND users_id=".$user_id);
            $records = self::$DB->fetchAll($select_handle);

            if(empty($records))
            {
                //Check Role Level Access
                $select_handle = self::$DB->query("select security_levels_id as Level, modules_id, modules.name as Modules, actions_id, actions.name as Actions, modules.is_admin from roles_security
                                                LEFT JOIN modules on modules_id=modules.id
                                                LEFT JOIN actions on actions_id =actions.id WHERE modules.is_active=1 AND roles_id={$default_role}");
                $records = self::$DB->fetchAll($select_handle);
            }else{
                if($type == 'JSON')
                    echo self::sortArray($records, $type);
                else
                    return self::sortArray($records, $type);
                exit;
            }

            if(empty($records))
            {
                //Check Team Level Access
                $select_handle = self::$DB->query("select security_levels_id as Level, modules_id, modules.name as Modules, actions_id, actions.name as Actions, modules.is_admin from teams_security
                                                LEFT JOIN modules on modules_id=modules.id
                                                LEFT JOIN actions on actions_id =actions.id WHERE modules.is_active=1 AND  teams_id=".$default_team);
                $records = self::$DB->fetchAll($select_handle);
            }else{
                if($type == 'JSON')
                    echo self::sortArray($records, $type);
                else
                    return self::sortArray($records, $type);
                exit;
            }

            if(empty($records))
            {
                //Check Department Level Access
                $select_handle = self::$DB->query("select security_levels_id as Level, modules_id, modules.name as Modules, actions_id, actions.name as Actions, modules.is_admin from departments_security
                                                LEFT JOIN modules on modules_id=modules.id
                                                LEFT JOIN actions on actions_id =actions.id WHERE modules.is_active=1 AND  departments_id=".$default_department);
                $records = self::$DB->fetchAll($select_handle);
            }else{
                if($type == 'JSON')
                    echo self::sortArray($records, $type);
                else
                    return self::sortArray($records, $type);
                exit;
            }

            //No security set up for this user
            if(empty($records))
            {
                if($type == 'JSON')
                    echo json_encode(array());
                else
                    return array();
                exit;
            }else{
                if($type == 'JSON')
                    echo self::sortArray($records, $type);
                else
                    return self::sortArray($records, $type);
                exit;
            }

        }

        function sortArray($records, $type)
        {
            foreach($records as $record)
            {
                $result[$record['Modules']][$record['Actions']] = $record['Level'];
                if($record['is_admin'] == 1)
                    $result[$record['Modules']]['admin'] = 1;
                else
                    $result[$record['Modules']]['admin'] = 0;

            }
            if($type == 'JSON')
                return json_encode($result);
            else
                return $result;

        }
	}//END Class securityGear
	

?>