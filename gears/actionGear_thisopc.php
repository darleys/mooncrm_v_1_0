<?php
/**
 * Created by Vanitha.
 * User: 10046078
 * Date: 2/3/2015
 * Time: 4:11 PM
 */

class actionGear_OPC_REMOVE extends Foundry_Core_Service
{

    static function __process($request)
    {
        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);

        if($module != 'Workshop')
            self::$action($request);
        else
            workshopGear::__process($request);
    }

    function Lists($request)
    {

        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);

        if(Mold_SAE::GA()['type'] == 3){

            if (!file_exists(CORE_PATH . DS . 'molds/Mold_' . $module . '.php') || (class_exists('Mold_' . $module) && !method_exists('Mold_' . $module, $action)))
            {
                $data['module'] = $module;
                $data['action'] = $action;

                $data['search_txt']  = Mold_Mis::renderSearchString($_POST,$module);

                $where = '';

                if ($data['where'] != '') {
                    $where = $data['where'];
                }

                if ($data['search_txt'] != "") {
                    $search_txt = mysql_real_escape_string(str_replace(array('[', ']'), array('', ''), $data['search_txt']));
                    $where .= ' AND ('. $data['search_txt'] .')';
                }

                //get base record for the module
                $select_handle = self::$DB->query("select * from " . $module . " WHERE deleted=0 " . $where);
                $records = self::$DB->fetchAll($select_handle);

                //GET Layout FOR Module
                $data['search'] = Mold_Mis::getSearchLayout($module);
                $data['lists'] = Mold_Mis::getListsLayout($module);
                $data['total_num'] = count($records);

                //get current page num
                $page_num = ($_GET['page_num']) ? $_GET['page_num'] : 1;
                $data['page_num'] = $page_num;

                //create pagination links
                $page_id = gear3::createPaging($records);

                $data['total_pages'] = gear3::totalPages($page_id, PAGE_SIZE);
                $data['records'] = gear3::sendPaging($page_id, PAGE_SIZE, $page_num);

            } else {
                $class = 'Mold_' . $module;
                $data = $class::$action($request);
            }

            $smarty = Foundry_RObjects::get("smarty");
            $smarty->assign($data);

            if (file_exists(TEMPLATES_DIR . DS . $module . '/' . $action . '.tpl'))
                $smarty->display(TEMPLATES_DIR . DS . $module . '/' . $action . '.tpl');
            else
                $smarty->display(TEMPLATES_DIR . DS . 'common/' . $action . '.tpl');
        }else{
           echo 'No Access';
        }

    }

    function View($request)
    {

        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);
        $id = $request[2];

        $data['module']= $module;
        $data['action']= $action;
        $data['id']= $id;

        if( $id != "" )  {
            $select_handle = self::$DB->query("select * from ".$module." where id = ".$id);
            if($record = mysql_fetch_assoc($select_handle)) {
                $data['record'] =  $record;
            }
        }

        $smarty = Foundry_RObjects::get("smarty");
        if(Mold_SAE::GA()['type'] == 3){

            $data['layout'] = Mold_Mis::getViewLayout($module);

            $smarty->assign($data);

            if (file_exists(TEMPLATES_DIR . DS . $module . '/' . $action . '.tpl'))
                $smarty->display(TEMPLATES_DIR . DS . $module . '/' . $action . '.tpl');
            else
                $smarty->display(TEMPLATES_DIR . DS . 'common/View.tpl');
        }else{
            $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }
    }


    function Edit($request)
    {
        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);
        $id = $request[2];

        $data['module']= $module;
        $data['action']= $action;
        $data['id']= $id;
        $record = array();

        if( $id != "" )  {
            $select_handle = self::$DB->query("select * from ".$module." where id = ".$id);
            if($record = mysql_fetch_assoc($select_handle)) {
                $data['record'] =  $record;
            }
        }else  $data['record'] =  $record;

        $smarty = Foundry_RObjects::get("smarty");

        if(Mold_SAE::GA()['type'] == 3) {

            $data['layout'] = Mold_Mis::getEditLayout($module);

            $smarty->assign($data);

            if(file_exists(TEMPLATES_DIR.DS.$module.'/'.$action.'.tpl'))
                $smarty->display(TEMPLATES_DIR.DS.$module.'/'.$action.'.tpl' );
            else
                $smarty->display (TEMPLATES_DIR.DS.'common/Edit.tpl' );
        }else{
            $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }

    }

    function Delete($request){

        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);
        $id = $request[2];
        $select_handle = self::$DB->query("select * from ".$module." where id = ".$id);
        $record = mysql_fetch_assoc($select_handle);

        $smarty = Foundry_RObjects::get("smarty");

        if(Mold_SAE::GA()['type'] == 3){
            self::$DB->query("UPDATE " . $module . " SET deleted=1 WHERE id=" .$id);
        }else{
            $smarty->display(TEMPLATES_DIR . DS . 'common/NoAccess.tpl');
        }
    }

    function Save($request)
    {
        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);
        $id = $_POST['id'];

        $data= $_POST;

        $field = array();
        $select_handle = self::$DB->query("select * FROM Fields WHERE module='".$module."'");
        while($record = mysql_fetch_assoc($select_handle))
            $field[] = $record['name'];

        if($id == "" )  {
            $values = array();

            foreach($data as $key => $value) {
                if(in_array($key,$field))
                {
                    if(is_array($value))
                        $value = implode(',',$value);
                    $values[$key] = $value;
                }
            }
            $values['date_entered'] = date('Y-m-d H:i:s', time());
            $values['created_by'] = Mold_SAE::GA()['id'];
            $values['modified_by'] = Mold_SAE::GA()['id'];

            $fields = array_keys($values);
            $values = array_values($values);

            $id = self::$DB->insert($module,$fields,$values);
        }else{
            $values = array();

            foreach($data as $key => $value) {
                if(in_array($key,$field)) {
                    if (is_array($value))
                        $value = implode(',', $value);
                    $values[$key] = $value;
                }
            }
            $values['modified_by'] = Mold_SAE::GA()['id'];

            $fields = array_keys($values);
            $values = array_values($values);

            $where = "id = ". $id;
            self::$DB->update($module,$fields,$values, $where);
        }

        $request[0] = $module;
        $request[1] = 'View';
        $request[2] = $id;

        self::View($request);
    }

}//END Class actionGear


?>