CREATE DATABASE  IF NOT EXISTS `mooncrm_v_1_0` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mooncrm_v_1_0`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: mooncrm_v_1_0
-- ------------------------------------------------------
-- Server version	5.6.19-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Accounts`
--

DROP TABLE IF EXISTS `Accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `name` varchar(200) DEFAULT NULL,
  `Website` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Accounts`
--

LOCK TABLES `Accounts` WRITE;
/*!40000 ALTER TABLE `Accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `Accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Accounts_Layouts`
--

DROP TABLE IF EXISTS `Accounts_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Accounts_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Accounts_Layouts`
--

LOCK TABLES `Accounts_Layouts` WRITE;
/*!40000 ALTER TABLE `Accounts_Layouts` DISABLE KEYS */;
INSERT INTO `Accounts_Layouts` VALUES (1,'name','Lists',1,1,'Info'),(2,'name','View',1,1,'Info'),(3,'name','Edit',1,1,'Info'),(4,'name','Export',1,1,'Info'),(5,'name','Import',1,1,'Info'),(6,'name','Search',1,1,''),(7,'Website','Lists',2,0,''),(8,'Website','View',2,0,''),(9,'Website','Edit',2,0,''),(10,'Website','Delete',0,0,''),(11,'Website','Export',0,0,''),(12,'Website','Import',0,0,''),(13,'Website','Search',0,0,'');
/*!40000 ALTER TABLE `Accounts_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Contacts`
--

DROP TABLE IF EXISTS `Contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `name` varchar(200) DEFAULT NULL,
  `Phone` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `Mobile` varchar(250) DEFAULT NULL,
  `Fax` varchar(250) DEFAULT NULL,
  `Description` longtext,
  `Suffix` varchar(250) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Website` varchar(250) DEFAULT NULL,
  `Email` varchar(250) DEFAULT NULL,
  `salutation` varchar(250) DEFAULT NULL,
  `Role` varchar(250) DEFAULT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `Do_Not_Call` tinyint(1) DEFAULT NULL,
  `primary_contact` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Contacts`
--

LOCK TABLES `Contacts` WRITE;
/*!40000 ALTER TABLE `Contacts` DISABLE KEYS */;
INSERT INTO `Contacts` VALUES (1,'0000-00-00 00:00:00','2015-10-28 00:09:30',1,1,1,3,0,'Vanitha','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha','2015-10-24',0,0,0),(2,'2015-06-23 15:25:39','2015-10-28 00:09:30',1,1,1,3,0,'Vanitha','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha','2015-10-24',0,0,0),(3,'2015-06-23 15:25:51','2015-10-28 00:09:30',1,1,1,3,0,'Vanitha','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha','2015-10-24',0,0,0),(4,'2015-06-23 15:26:37','2015-10-28 00:09:30',1,1,1,3,0,'Vanitha','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha','2015-10-24',0,0,0),(5,'2015-06-23 15:27:58','2015-10-28 00:09:30',1,1,1,3,0,'Vanitha','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha','2015-10-24',0,0,0),(6,'2015-06-23 15:28:38','2015-10-28 00:09:30',1,1,1,3,0,'Vanitha','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha','2015-10-24',0,0,0),(7,'2015-06-23 15:29:29','2015-10-28 00:09:30',1,1,1,3,0,'Vanitha','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha','2015-10-24',0,0,0),(8,'2015-06-23 15:29:50','2015-10-28 00:09:30',1,1,1,3,0,'Vanitha','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha','2015-10-24',0,0,0),(9,'2015-06-23 15:31:24','2015-10-28 00:09:30',1,1,1,3,0,'Vanitha','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha','2015-10-24',0,0,0),(10,'2015-06-23 15:46:23','2015-10-28 00:09:30',1,1,1,3,0,'Vanitha','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha','2015-10-24',0,0,0),(12,'2015-10-23 18:38:36','2015-10-28 00:16:13',1,1,3,1,0,'Vanitha111','124-222-2222','E','235234','353434','sdfsfsdfsdfsdf','df','sdfsdf','sdfsfd','ffff','Mrs','Decision_Maker','Vanitha111','2015-10-24',0,0,0),(17,'2015-10-28 00:23:51','2015-10-28 00:23:51',1,1,3,1,0,'sdfsdf','','','','','','','','','','','','',NULL,NULL,NULL,1),(20,'2015-10-28 00:26:21','2015-10-28 00:26:21',1,1,3,1,0,'YYY','','','','','','','','','','','','',NULL,NULL,NULL,1),(22,'2015-10-28 00:29:10','2015-10-28 00:29:10',1,1,3,1,0,'asdasd','','','','','','','','','','','','',NULL,NULL,NULL,1),(23,'2015-10-28 00:29:12','2015-10-28 00:29:12',1,1,3,1,0,'asdasd','','','','','','','','','','','','',NULL,NULL,NULL,1),(24,'2015-10-28 00:29:49','2015-10-28 00:29:49',1,1,3,1,0,'asdasd','','','','','','','','','','','','',NULL,NULL,NULL,1),(25,'2015-10-28 00:30:36','2015-10-28 00:31:18',1,1,3,1,0,'asdasd','','','','','','','','','','','','','1969-12-31',1,1,1),(26,'2015-10-28 00:31:46','2015-10-28 00:31:46',1,1,3,1,0,'bbb','','','','','','','','','','','','bbb',NULL,NULL,NULL,1),(27,'2015-10-28 00:32:27','2015-10-28 15:11:42',1,1,3,1,0,'nnnn','','','','','asdasd','','','','','','','asdasd','1969-12-31',1,1,1),(28,'2015-10-28 00:35:21','2015-10-28 00:35:29',1,1,3,1,0,'VV','','','','','','','','','','','','VV','2015-10-31',1,1,1),(30,'2015-10-28 00:39:30','2015-10-28 00:39:30',1,1,3,1,0,'XXX','124141234324','XXX','1231412421','1234444444','XXXX','sdfsdf','sdfsdf','sdfsdf','XX@XX.com','Mr','Decision_Maker','XXX','1978-01-16',1,1,1),(31,'2015-10-28 00:40:32','2015-10-28 15:07:16',1,1,3,1,0,'vadadadsa','','','','','','','','','','','Gatekeeper','sfsdf','2015-10-17',NULL,NULL,NULL),(32,'2015-10-28 11:32:32','2015-10-28 11:32:32',1,1,3,1,0,'sfgdf','','','','','','','','','','','','gdgdgfd','0000-00-00',1,1,1),(34,'2015-10-28 11:32:42','2015-10-28 11:32:42',1,1,3,1,0,'twtrwr','','','','','','','','','','','','werwrwer','0000-00-00',NULL,NULL,NULL);
/*!40000 ALTER TABLE `Contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Contacts_Layouts`
--

DROP TABLE IF EXISTS `Contacts_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contacts_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=372 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Contacts_Layouts`
--

LOCK TABLES `Contacts_Layouts` WRITE;
/*!40000 ALTER TABLE `Contacts_Layouts` DISABLE KEYS */;
INSERT INTO `Contacts_Layouts` VALUES (1,'name','Lists',1,1,''),(2,'name','View',1,1,''),(3,'name','Edit',1,0,''),(4,'name','Export',1,1,''),(5,'name','Import',1,1,''),(6,'name','Search',1,1,''),(211,'Description','Lists',0,0,''),(212,'Description','View',3,0,''),(213,'Description','Edit',3,0,''),(214,'Description','Delete',0,0,''),(215,'Description','Export',0,0,''),(216,'Description','Import',0,0,''),(217,'Description','Search',0,0,''),(232,'Email','Lists',0,0,''),(233,'Email','View',5,0,''),(234,'Email','Edit',6,0,''),(235,'Email','Delete',0,0,''),(236,'Email','Export',0,0,''),(237,'Email','Import',0,0,''),(238,'Email','Search',0,0,''),(239,'Fax','Lists',0,0,''),(240,'Fax','View',6,0,''),(241,'Fax','Edit',7,0,''),(242,'Fax','Delete',0,0,''),(243,'Fax','Export',0,0,''),(244,'Fax','Import',0,0,''),(245,'Fax','Search',0,0,''),(253,'middle_name','Lists',0,0,''),(254,'middle_name','View',7,0,''),(255,'middle_name','Edit',9,0,''),(256,'middle_name','Delete',0,0,''),(257,'middle_name','Export',0,0,''),(258,'middle_name','Import',0,0,''),(259,'middle_name','Search',0,0,''),(260,'Mobile','Lists',0,0,''),(261,'Mobile','View',8,0,''),(262,'Mobile','Edit',10,0,''),(263,'Mobile','Delete',0,0,''),(264,'Mobile','Export',0,0,''),(265,'Mobile','Import',0,0,''),(266,'Mobile','Search',0,0,''),(267,'Phone','Lists',0,0,''),(268,'Phone','View',9,0,''),(269,'Phone','Edit',11,0,''),(270,'Phone','Delete',0,0,''),(271,'Phone','Export',0,0,''),(272,'Phone','Import',0,0,''),(273,'Phone','Search',0,0,''),(281,'Role','Lists',0,0,''),(282,'Role','View',11,0,''),(283,'Role','Edit',13,0,''),(284,'Role','Delete',0,0,''),(285,'Role','Export',0,0,''),(286,'Role','Import',0,0,''),(287,'Role','Search',0,0,''),(288,'salutation','Lists',0,0,''),(289,'salutation','View',12,0,''),(290,'salutation','Edit',14,0,''),(291,'salutation','Delete',0,0,''),(292,'salutation','Export',0,0,''),(293,'salutation','Import',0,0,''),(294,'salutation','Search',0,0,''),(295,'Suffix','Lists',0,0,''),(296,'Suffix','View',13,0,''),(297,'Suffix','Edit',15,0,''),(298,'Suffix','Delete',0,0,''),(299,'Suffix','Export',0,0,''),(300,'Suffix','Import',0,0,''),(301,'Suffix','Search',0,0,''),(302,'Title','Lists',0,0,''),(303,'Title','View',14,0,''),(304,'Title','Edit',16,0,''),(305,'Title','Delete',0,0,''),(306,'Title','Export',0,0,''),(307,'Title','Import',0,0,''),(308,'Title','Search',0,0,''),(309,'Website','Lists',0,0,''),(310,'Website','View',15,0,''),(311,'Website','Edit',17,0,''),(312,'Website','Delete',0,0,''),(313,'Website','Export',0,0,''),(314,'Website','Import',0,0,''),(315,'Website','Search',0,0,''),(316,'first_name','Lists',0,0,''),(317,'first_name','View',16,0,''),(318,'first_name','Edit',2,0,''),(319,'first_name','Delete',0,0,''),(320,'first_name','Export',0,0,''),(321,'first_name','Import',0,0,''),(322,'first_name','Search',0,0,''),(323,'DOB','Lists',0,0,''),(324,'DOB','View',17,0,''),(325,'DOB','Edit',18,0,''),(326,'DOB','Delete',0,0,''),(327,'DOB','Export',0,0,''),(328,'DOB','Import',0,0,''),(329,'DOB','Search',0,0,''),(337,'Active','Lists',0,0,''),(338,'Active','View',2,0,''),(339,'Active','Edit',2,0,''),(340,'Active','Delete',0,0,''),(341,'Active','Export',0,0,''),(342,'Active','Import',0,0,''),(343,'Active','Search',0,0,''),(351,'Do_Not_Call','Lists',0,0,''),(352,'Do_Not_Call','View',4,0,''),(353,'Do_Not_Call','Edit',4,0,''),(354,'Do_Not_Call','Delete',0,0,''),(355,'Do_Not_Call','Export',0,0,''),(356,'Do_Not_Call','Import',0,0,''),(357,'Do_Not_Call','Search',0,0,''),(365,'primary_contact','Lists',0,0,''),(366,'primary_contact','View',10,0,''),(367,'primary_contact','Edit',3,0,''),(368,'primary_contact','Delete',0,0,''),(369,'primary_contact','Export',0,0,''),(370,'primary_contact','Import',0,0,''),(371,'primary_contact','Search',0,0,'');
/*!40000 ALTER TABLE `Contacts_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Contracts`
--

DROP TABLE IF EXISTS `Contracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Contracts`
--

LOCK TABLES `Contracts` WRITE;
/*!40000 ALTER TABLE `Contracts` DISABLE KEYS */;
/*!40000 ALTER TABLE `Contracts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Contracts_Layouts`
--

DROP TABLE IF EXISTS `Contracts_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contracts_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Contracts_Layouts`
--

LOCK TABLES `Contracts_Layouts` WRITE;
/*!40000 ALTER TABLE `Contracts_Layouts` DISABLE KEYS */;
INSERT INTO `Contracts_Layouts` VALUES (1,'name','Lists',1,1,'Info'),(2,'name','View',1,1,'Info'),(3,'name','Edit',1,1,'Info'),(4,'name','Search',1,1,'');
/*!40000 ALTER TABLE `Contracts_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Departments`
--

DROP TABLE IF EXISTS `Departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `parent` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Departments`
--

LOCK TABLES `Departments` WRITE;
/*!40000 ALTER TABLE `Departments` DISABLE KEYS */;
INSERT INTO `Departments` VALUES (1,'2015-03-26 19:10:19','2015-07-14 20:07:09',1,1,0,'Department 1',2),(2,'2015-03-26 19:29:25','2015-07-14 20:07:09',1,1,0,'Department 22',3),(3,'2015-06-15 19:13:37','2015-07-14 20:07:09',1,1,0,'Department 333',0),(4,'0000-00-00 00:00:00','2015-07-20 15:10:29',1,1,0,'Department 444',1),(5,'2015-07-29 04:59:07','2015-07-29 04:59:07',1,1,0,'Department5',0);
/*!40000 ALTER TABLE `Departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Departments_Layouts`
--

DROP TABLE IF EXISTS `Departments_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Departments_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Departments_Layouts`
--

LOCK TABLES `Departments_Layouts` WRITE;
/*!40000 ALTER TABLE `Departments_Layouts` DISABLE KEYS */;
INSERT INTO `Departments_Layouts` VALUES (1,'name','Lists',1,NULL,NULL),(2,'name','Edit',1,1,'Details'),(3,'name','View',1,1,'Details'),(4,'parent','Lists',1,NULL,NULL),(5,'parent','Edit',1,1,'Details'),(6,'parent','View',1,1,'Details'),(7,'name','Search',1,NULL,NULL);
/*!40000 ALTER TABLE `Departments_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Dropdown_Options`
--

DROP TABLE IF EXISTS `Dropdown_Options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dropdown_Options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Dropdown_Options`
--

LOCK TABLES `Dropdown_Options` WRITE;
/*!40000 ALTER TABLE `Dropdown_Options` DISABLE KEYS */;
INSERT INTO `Dropdown_Options` VALUES (1,'leads_stage_lists',0),(2,'opps_status_lists',0),(7,'opps_stage_lists',0),(8,'bbbbb',0),(9,'asdadsad',0),(14,'',0),(15,'salutation',0),(16,'Contact_Roles',0);
/*!40000 ALTER TABLE `Dropdown_Options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Dropdown_Values`
--

DROP TABLE IF EXISTS `Dropdown_Values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dropdown_Values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Dropdown_Values`
--

LOCK TABLES `Dropdown_Values` WRITE;
/*!40000 ALTER TABLE `Dropdown_Values` DISABLE KEYS */;
INSERT INTO `Dropdown_Values` VALUES (2,1,'Lost','Lost lead'),(3,1,'Processing','Processing Lead'),(4,1,'Test','Testting'),(11,9,'Two','Two 2222222'),(12,9,'Three','Threee 3333'),(13,7,'first','First'),(15,8,'safsdf','sfsfsfsdfsf'),(16,8,'sdfsdf','sfsfsd'),(17,15,'Mr','Mr.'),(18,15,'Mrs','Mrs.'),(20,16,'Initiator','Initiator'),(21,16,'Influencer','Influencer'),(22,16,'Decision_Maker','Decision Maker'),(23,16,'Gatekeeper','Gatekeeper'),(24,16,'User','User'),(25,16,'Buyer','Buyer');
/*!40000 ALTER TABLE `Dropdown_Values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Fields`
--

DROP TABLE IF EXISTS `Fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Fields` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `module` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `label` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `default_value` varchar(45) DEFAULT NULL,
  `required` tinyint(4) NOT NULL DEFAULT '0',
  `extra1` varchar(45) DEFAULT NULL,
  `extra2` varchar(45) DEFAULT NULL,
  `extra3` varchar(45) DEFAULT NULL,
  `extra4` varchar(45) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Fields`
--

LOCK TABLES `Fields` WRITE;
/*!40000 ALTER TABLE `Fields` DISABLE KEYS */;
INSERT INTO `Fields` VALUES (1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'Teams','name','Name','text',NULL,1,NULL,NULL,NULL,NULL,0),(3,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'Roles','name','Name','text',NULL,1,NULL,NULL,NULL,NULL,0),(4,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'Departments','name','Name','text',NULL,1,NULL,NULL,NULL,NULL,0),(5,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'Users','name','First Name','text',NULL,1,NULL,NULL,NULL,NULL,0),(6,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'Users','last_name','Last Name','text',NULL,1,NULL,NULL,NULL,NULL,0),(7,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'Departments','parent','Parent','relation',NULL,0,'Departments',NULL,NULL,NULL,0),(8,'0000-00-00 00:00:00','2015-06-16 02:08:10',0,0,'Users','default_department','Department','relation',NULL,0,'Departments',NULL,NULL,NULL,0),(9,'0000-00-00 00:00:00','2015-06-16 02:06:46',0,0,'Users','default_team','Team','relation',NULL,0,'Teams',NULL,NULL,NULL,0),(10,'0000-00-00 00:00:00','2015-06-16 02:06:46',0,0,'Users','default_role','Role','relation',NULL,0,'Roles',NULL,NULL,NULL,0),(12,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'Departments','name','Name','text',NULL,0,NULL,NULL,NULL,NULL,0),(17,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'Check','name','Name','text',NULL,1,NULL,NULL,NULL,NULL,0),(35,'0000-00-00 00:00:00','2015-06-16 02:06:46',1,1,'Users','reports_to','Reports To','relation',NULL,0,'Users',NULL,NULL,NULL,0),(36,'0000-00-00 00:00:00','2015-06-23 22:41:27',0,0,'Teams','parent','Parent Team','relation',NULL,0,'Teams',NULL,NULL,NULL,0),(37,'0000-00-00 00:00:00','2015-06-23 22:41:58',0,0,'Teams','departments_id','Department','relation',NULL,0,'Departments',NULL,NULL,NULL,0),(45,'2015-06-09 03:40:45','2015-06-09 03:40:45',1,1,'Departments','Parent','Parent Department','relation',NULL,0,'Departments',NULL,NULL,NULL,0),(48,'2015-06-15 18:33:56','2015-06-15 18:33:56',1,1,'Roles','name','Name','text','',1,NULL,NULL,NULL,NULL,0),(49,'2015-06-15 18:34:35','2015-06-15 18:34:35',1,1,'Teams','name','Name','text','',1,NULL,NULL,NULL,NULL,0),(52,'0000-00-00 00:00:00','2015-10-23 18:30:40',0,1,'Contacts','name','Last Name','text','',1,NULL,NULL,NULL,NULL,0),(53,'0000-00-00 00:00:00','2015-06-16 10:53:44',0,0,'Quotes','name','Name','text',NULL,1,NULL,NULL,NULL,NULL,0),(54,'0000-00-00 00:00:00','2015-09-23 01:54:25',0,1,'Leads','name','First Name','text','',1,NULL,NULL,NULL,NULL,0),(58,'2015-06-23 15:21:05','2015-09-22 20:11:35',1,1,'Contacts','Phone','Office Phone','text','',0,NULL,NULL,NULL,NULL,0),(60,'0000-00-00 00:00:00','2015-07-20 19:19:36',0,0,'Inventory','name','Name','text',NULL,1,NULL,NULL,NULL,NULL,0),(63,'0000-00-00 00:00:00','2015-07-29 01:22:04',0,1,'Accounts','name','Account Name','text','',1,NULL,NULL,NULL,NULL,0),(64,'2015-07-28 15:46:38','2015-09-23 01:15:31',1,1,'Leads','last_name','Last Name','text','',1,NULL,NULL,NULL,NULL,0),(66,'0000-00-00 00:00:00','2015-07-28 16:14:16',0,0,'Contracts','name','Name','text',NULL,1,NULL,NULL,NULL,NULL,0),(67,'0000-00-00 00:00:00','2015-07-29 01:40:34',0,0,'Test','name','Name','text',NULL,1,NULL,NULL,NULL,NULL,0),(68,'2015-07-29 03:47:18','2015-07-29 03:47:18',1,1,'Accounts','Website','Website','text','',0,NULL,NULL,NULL,NULL,0),(69,'2015-07-29 14:08:18','2015-07-29 14:08:18',1,1,'','','','',NULL,0,NULL,NULL,NULL,NULL,0),(70,'2015-09-22 20:10:55','2015-09-22 20:10:56',1,1,'Contacts','middle_name','Middle Name','text','',0,NULL,NULL,NULL,NULL,0),(71,'2015-09-22 20:12:02','2015-09-22 20:12:02',1,1,'Contacts','Mobile','Mobile','text','',0,NULL,NULL,NULL,NULL,0),(72,'2015-09-22 20:13:00','2015-09-22 20:13:00',1,1,'Contacts','Fax','Fax','text','',0,NULL,NULL,NULL,NULL,0),(73,'2015-09-22 20:13:12','2015-09-23 05:47:52',1,1,'Contacts','Description','Description','textarea','',0,NULL,NULL,NULL,NULL,0),(75,'2015-09-22 20:13:52','2015-09-22 20:13:52',1,1,'Contacts','Suffix','Suffix','text','',0,NULL,NULL,NULL,NULL,0),(76,'2015-09-22 20:14:06','2015-09-22 20:14:06',1,1,'Contacts','Title','Title','text','',0,NULL,NULL,NULL,NULL,0),(78,'2015-09-22 20:14:36','2015-09-22 20:14:36',1,1,'Contacts','Website','Website','text','',0,NULL,NULL,NULL,NULL,0),(82,'2015-09-22 20:19:08','2015-09-22 20:19:08',1,1,'Contacts','Email','Email','text','',0,NULL,NULL,NULL,NULL,0),(83,'2015-09-22 20:20:52','2015-09-22 22:54:47',1,1,'Contacts','salutation','Sal','dropdown','',0,'salutation',NULL,NULL,NULL,0),(84,'2015-09-23 01:10:20','2015-09-23 01:10:20',1,1,'','','','',NULL,0,NULL,NULL,NULL,NULL,0),(85,'2015-09-23 01:10:55','2015-09-23 01:10:55',1,1,'','','','',NULL,0,NULL,NULL,NULL,NULL,0),(86,'2015-09-23 01:11:04','2015-09-23 01:11:05',1,1,'','','','',NULL,0,NULL,NULL,NULL,NULL,0),(87,'2015-09-23 01:12:38','2015-09-23 01:12:39',1,1,'','','','',NULL,0,NULL,NULL,NULL,NULL,0),(88,'2015-09-23 02:41:05','2015-09-23 02:41:20',1,1,'Contacts','Role','Role','dropdown','User',1,'Contact_Roles',NULL,NULL,NULL,0),(90,'2015-10-23 18:31:02','2015-10-23 18:31:02',1,1,'Contacts','first_name','First Name','text','',0,NULL,NULL,NULL,NULL,0),(91,'2015-10-23 18:37:01','2015-10-23 18:37:01',1,1,'Contacts','dob','DOB','date',NULL,0,NULL,NULL,NULL,NULL,0),(93,'2015-10-27 16:02:10','2015-10-27 16:02:11',1,1,'Contacts','active','Active','bool',NULL,0,NULL,NULL,NULL,NULL,0),(95,'2015-10-27 16:07:17','2015-10-27 16:07:17',1,1,'Contacts','do_not_call','Do Not Call','bool',NULL,0,NULL,NULL,NULL,NULL,0),(97,'2015-10-27 17:19:24','2015-10-27 17:19:24',1,1,'Contacts','primary_contact','Primary','bool',NULL,0,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `Fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Inventory`
--

DROP TABLE IF EXISTS `Inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Inventory`
--

LOCK TABLES `Inventory` WRITE;
/*!40000 ALTER TABLE `Inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `Inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Inventory_Layouts`
--

DROP TABLE IF EXISTS `Inventory_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Inventory_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Inventory_Layouts`
--

LOCK TABLES `Inventory_Layouts` WRITE;
/*!40000 ALTER TABLE `Inventory_Layouts` DISABLE KEYS */;
INSERT INTO `Inventory_Layouts` VALUES (1,'name','Lists',1,1,'Info'),(2,'name','View',1,1,'Info'),(3,'name','Edit',1,1,'Info'),(4,'name','Export',1,1,'Info'),(5,'name','Import',1,1,'Info'),(6,'name','Search',1,1,'');
/*!40000 ALTER TABLE `Inventory_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Leads`
--

DROP TABLE IF EXISTS `Leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `name` varchar(200) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Leads`
--

LOCK TABLES `Leads` WRITE;
/*!40000 ALTER TABLE `Leads` DISABLE KEYS */;
INSERT INTO `Leads` VALUES (1,'2015-06-23 00:38:25','2015-07-28 15:47:29',1,1,1,3,0,'Lead1','Mark'),(2,'2015-06-29 22:42:25','2015-06-29 21:42:25',1,1,1,1,0,'TEST 123',NULL),(3,'2015-07-20 21:01:14','2015-07-20 20:01:14',1,1,3,1,0,'sd',NULL);
/*!40000 ALTER TABLE `Leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Leads_Layouts`
--

DROP TABLE IF EXISTS `Leads_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Leads_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Leads_Layouts`
--

LOCK TABLES `Leads_Layouts` WRITE;
/*!40000 ALTER TABLE `Leads_Layouts` DISABLE KEYS */;
INSERT INTO `Leads_Layouts` VALUES (1,'name','Lists',1,1,'Info'),(2,'name','View',1,1,'Info'),(3,'name','Edit',1,1,'Info'),(4,'name','Export',1,1,'Info'),(5,'name','Import',1,1,'Info'),(6,'name','Search',1,1,''),(16,'last_name','Lists',2,1,'Info'),(17,'last_name','View',2,1,'asdasd'),(18,'last_name','Edit',2,0,''),(19,'last_name','Delete',0,0,''),(20,'last_name','Export',0,0,''),(21,'last_name','Import',0,0,''),(22,'last_name','Search',2,0,'');
/*!40000 ALTER TABLE `Leads_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Quotes`
--

DROP TABLE IF EXISTS `Quotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Quotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Quotes`
--

LOCK TABLES `Quotes` WRITE;
/*!40000 ALTER TABLE `Quotes` DISABLE KEYS */;
/*!40000 ALTER TABLE `Quotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Quotes_Layouts`
--

DROP TABLE IF EXISTS `Quotes_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Quotes_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Quotes_Layouts`
--

LOCK TABLES `Quotes_Layouts` WRITE;
/*!40000 ALTER TABLE `Quotes_Layouts` DISABLE KEYS */;
INSERT INTO `Quotes_Layouts` VALUES (1,'name','Lists',1,1,'Info'),(2,'name','View',1,1,'Info'),(3,'name','Edit',1,1,'Info'),(4,'name','Export',1,1,'Info'),(5,'name','Import',1,1,'Info'),(6,'name','Search',1,1,'');
/*!40000 ALTER TABLE `Quotes_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Roles`
--

DROP TABLE IF EXISTS `Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Roles`
--

LOCK TABLES `Roles` WRITE;
/*!40000 ALTER TABLE `Roles` DISABLE KEYS */;
INSERT INTO `Roles` VALUES (1,'2015-12-12 05:00:00','2015-06-15 19:15:40',1,1,1,3,0,'VP'),(2,'2015-03-24 02:59:45','2015-06-15 19:15:52',0,1,0,0,0,'Sales Manager'),(3,'2015-03-24 03:01:16','2015-06-15 19:16:06',0,1,0,0,0,'RVP-OTC'),(4,'2015-03-24 03:10:12','2015-06-15 19:16:24',0,1,0,0,0,'Sales Rep - OTC'),(5,'2015-03-24 03:11:16','2015-06-15 19:16:37',0,1,0,0,0,'CSR'),(6,'2015-03-24 03:12:05','2015-06-15 19:16:51',1,1,1,3,0,'RVP-OPM'),(7,'2015-03-24 03:12:18','2015-06-15 19:17:03',1,1,1,3,0,'Director-Sales');
/*!40000 ALTER TABLE `Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Roles_Layouts`
--

DROP TABLE IF EXISTS `Roles_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Roles_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Roles_Layouts`
--

LOCK TABLES `Roles_Layouts` WRITE;
/*!40000 ALTER TABLE `Roles_Layouts` DISABLE KEYS */;
INSERT INTO `Roles_Layouts` VALUES (1,'name','Lists',1,NULL,NULL),(2,'name','Edit',1,1,'Details'),(3,'name','View',1,1,'Details'),(8,'name','Search',1,0,'');
/*!40000 ALTER TABLE `Roles_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Teams`
--

DROP TABLE IF EXISTS `Teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `departments_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Teams`
--

LOCK TABLES `Teams` WRITE;
/*!40000 ALTER TABLE `Teams` DISABLE KEYS */;
INSERT INTO `Teams` VALUES (1,'2015-03-26 19:08:56','2015-07-10 21:04:45',1,1,0,'South America',1,2),(2,'2015-06-15 19:20:24','2015-07-13 16:44:59',1,1,0,'US SALES',1,3),(3,'0000-00-00 00:00:00','2015-07-14 19:47:31',1,1,0,'Sales Main',1,0),(4,'0000-00-00 00:00:00','2015-07-10 21:04:03',1,1,0,'North America',1,2);
/*!40000 ALTER TABLE `Teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Teams_Layouts`
--

DROP TABLE IF EXISTS `Teams_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Teams_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Teams_Layouts`
--

LOCK TABLES `Teams_Layouts` WRITE;
/*!40000 ALTER TABLE `Teams_Layouts` DISABLE KEYS */;
INSERT INTO `Teams_Layouts` VALUES (1,'name','Lists',1,NULL,NULL),(2,'name','Edit',1,1,'Details'),(3,'name','View',1,1,'Details'),(4,'name','Search',1,NULL,NULL),(5,'parent','Lists',2,NULL,NULL),(6,'departments_id','Lists',3,NULL,NULL);
/*!40000 ALTER TABLE `Teams_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Test`
--

DROP TABLE IF EXISTS `Test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Test`
--

LOCK TABLES `Test` WRITE;
/*!40000 ALTER TABLE `Test` DISABLE KEYS */;
/*!40000 ALTER TABLE `Test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Test_Layouts`
--

DROP TABLE IF EXISTS `Test_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Test_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Test_Layouts`
--

LOCK TABLES `Test_Layouts` WRITE;
/*!40000 ALTER TABLE `Test_Layouts` DISABLE KEYS */;
INSERT INTO `Test_Layouts` VALUES (1,'name','Lists',1,1,'Info'),(2,'name','View',1,1,'Info'),(3,'name','Edit',1,1,'Info'),(4,'name','Search',1,1,'');
/*!40000 ALTER TABLE `Test_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `username` varchar(45) NOT NULL,
  `mysecret` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `address1` varchar(45) DEFAULT NULL,
  `address2` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1=standard, 2=admin, 3=super admin',
  `reports_to` int(11) NOT NULL,
  `default_department` int(11) DEFAULT NULL,
  `default_team` int(11) DEFAULT NULL,
  `default_role` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'0000-00-00 00:00:00','2015-07-15 19:28:33',NULL,1,'sdsd','d9308f32f8c6cf370ca5aaaeafc0d49b','maulik.bavishi@yahoo.co.in','Maulik','Bavishi','surat','surat','surat','gujrat','845764','India',3,1,1,3,5,0),(2,'0000-00-00 00:00:00','2015-06-16 02:15:46',NULL,1,'maulik1','d9308f32f8c6cf370ca5aaaeafc0d49b','maulik1@yahoo.co.in','Maulik 1111','Bavishi','address 1','address 2','surat','gujrat','395154','India',2,0,1,1,1,0),(3,'0000-00-00 00:00:00','2015-06-15 23:37:00',NULL,NULL,'darleys','fe01ce2a7fbac8fafaed7c982a04e229','darleys@yahoo.co.in','Darley','Stephen','surat','','surat','gujrat','395154','India',1,0,1,2,2,0),(4,'0000-00-00 00:00:00','2015-06-15 23:37:00',NULL,NULL,'darleys1','fe01ce2a7fbac8fafaed7c982a04e229','darleys@yahoo.co.in','Joshua','Stephen','surat','','surat','gujrat','395154','India',1,0,1,3,3,0),(5,'0000-00-00 00:00:00','2015-06-15 23:52:08',NULL,NULL,'vanieme','d9308f32f8c6cf370ca5aaaeafc0d49b','sd@sd.com','Vanitha','Darley','surat','','surat','gujrat','395154','India',3,0,14,2,2,0),(6,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,'mike','d9308f32f8c6cf370ca5aaaeafc0d49b','mike@gmail.com','Mike','Novick',NULL,NULL,NULL,NULL,NULL,NULL,3,1,1,1,1,0),(7,'2015-06-16 02:17:59','2015-06-16 02:17:59',1,1,'',NULL,NULL,'User','123',NULL,NULL,NULL,NULL,NULL,NULL,0,6,1,1,1,0);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users_Layouts`
--

DROP TABLE IF EXISTS `Users_Layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users_Layouts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `layout` varchar(45) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `tab` int(11) DEFAULT NULL,
  `tab_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users_Layouts`
--

LOCK TABLES `Users_Layouts` WRITE;
/*!40000 ALTER TABLE `Users_Layouts` DISABLE KEYS */;
INSERT INTO `Users_Layouts` VALUES (1,'name','Lists',1,NULL,NULL),(2,'name','Edit',1,1,'Details'),(3,'name','View',1,1,'Details'),(4,'name','Search',1,NULL,NULL),(5,'last_name','Lists',1,NULL,NULL),(6,'last_name','Edit',1,1,'Details'),(7,'last_name','View',1,1,'Details'),(8,'last_name','Search',1,NULL,NULL),(9,'default_department','Lists',1,NULL,NULL),(10,'default_department','Edit',1,1,'Details'),(11,'default_department','View',1,1,'Details'),(12,'default_department','Search',1,NULL,NULL),(13,'default_role','Lists',1,NULL,NULL),(14,'default_team','Lists',1,NULL,NULL),(15,'default_role','View',1,1,'Details'),(16,'default_role','Edit',1,1,'Details'),(17,'default_team','View',1,1,'Details'),(18,'default_team','Edit',1,1,'Details'),(19,'reports_to','Lists',1,NULL,NULL),(20,'reports_to','View',1,1,'Details'),(21,'reports_to','Edit',1,1,'Details');
/*!40000 ALTER TABLE `Users_Layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actions`
--

DROP TABLE IF EXISTS `actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actions`
--

LOCK TABLES `actions` WRITE;
/*!40000 ALTER TABLE `actions` DISABLE KEYS */;
INSERT INTO `actions` VALUES (1,'Lists',1,0),(2,'View',1,0),(3,'Edit',1,0),(4,'Delete',1,0),(5,'Export',1,0),(6,'Import',1,0);
/*!40000 ALTER TABLE `actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ccodes`
--

DROP TABLE IF EXISTS `ccodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ccodes` (
  `country` varchar(200) DEFAULT NULL,
  `currency_name` varchar(200) DEFAULT NULL,
  `currency_code` varchar(20) DEFAULT NULL,
  `num_code` varchar(10) DEFAULT NULL,
  `minor_unit` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ccodes`
--

LOCK TABLES `ccodes` WRITE;
/*!40000 ALTER TABLE `ccodes` DISABLE KEYS */;
INSERT INTO `ccodes` VALUES ('AFGHANISTAN','Afghani','AFN','971','2'),('?LAND ISLANDS','Euro','EUR','978','2'),('ALBANIA','Lek','ALL','008','2'),('ALGERIA','Algerian Dinar','DZD','012','2'),('AMERICAN SAMOA','US Dollar','USD','840','2'),('ANDORRA','Euro','EUR','978','2'),('ANGOLA','Kwanza','AOA','973','2'),('ANGUILLA','East Caribbean Dollar','XCD','951','2'),('ANTARCTICA','No universal currency','','',''),('ANTIGUA AND BARBUDA','East Caribbean Dollar','XCD','951','2'),('ARGENTINA','Argentine Peso','ARS','032','2'),('ARMENIA','Armenian Dram','AMD','051','2'),('ARUBA','Aruban Florin','AWG','533','2'),('AUSTRALIA','Australian Dollar','AUD','036','2'),('AUSTRIA','Euro','EUR','978','2'),('AZERBAIJAN','Azerbaijanian Manat','AZN','944','2'),('BAHAMAS (THE)','Bahamian Dollar','BSD','044','2'),('BAHRAIN','Bahraini Dinar','BHD','048','3'),('BANGLADESH','Taka','BDT','050','2'),('BARBADOS','Barbados Dollar','BBD','052','2'),('BELARUS','Belarussian Ruble','BYR','974','0'),('BELGIUM','Euro','EUR','978','2'),('BELIZE','Belize Dollar','BZD','084','2'),('BENIN','CFA Franc BCEAO','XOF','952','0'),('BERMUDA','Bermudian Dollar','BMD','060','2'),('BHUTAN','Ngultrum','BTN','064','2'),('BHUTAN','Indian Rupee','INR','356','2'),('BOLIVIA (PLURINATIONAL STATE OF)','Boliviano','BOB','068','2'),('BOLIVIA (PLURINATIONAL STATE OF)','Mvdol','BOV','984','2'),('BONAIRE, SINT EUSTATIUS AND SABA','US Dollar','USD','840','2'),('BOSNIA AND HERZEGOVINA','Convertible Mark','BAM','977','2'),('BOTSWANA','Pula','BWP','072','2'),('BOUVET ISLAND','Norwegian Krone','NOK','578','2'),('BRAZIL','Brazilian Real','BRL','986','2'),('BRITISH INDIAN OCEAN TERRITORY (THE)','US Dollar','USD','840','2'),('BRUNEI DARUSSALAM','Brunei Dollar','BND','096','2'),('BULGARIA','Bulgarian Lev','BGN','975','2'),('BURKINA FASO','CFA Franc BCEAO','XOF','952','0'),('BURUNDI','Burundi Franc','BIF','108','0'),('CABO VERDE','Cabo Verde Escudo','CVE','132','2'),('CAMBODIA','Riel','KHR','116','2'),('CAMEROON','CFA Franc BEAC','XAF','950','0'),('CANADA','Canadian Dollar','CAD','124','2'),('CAYMAN ISLANDS (THE)','Cayman Islands Dollar','KYD','136','2'),('CENTRAL AFRICAN REPUBLIC (THE)','CFA Franc BEAC','XAF','950','0'),('CHAD','CFA Franc BEAC','XAF','950','0'),('CHILE','Unidad de Fomento','CLF','990','4'),('CHILE','Chilean Peso','CLP','152','0'),('CHINA','Yuan Renminbi','CNY','156','2'),('CHRISTMAS ISLAND','Australian Dollar','AUD','036','2'),('COCOS (KEELING) ISLANDS (THE)','Australian Dollar','AUD','036','2'),('COLOMBIA','Colombian Peso','COP','170','2'),('COLOMBIA','Unidad de Valor Real','COU','970','2'),('COMOROS (THE)','Comoro Franc','KMF','174','0'),('CONGO (THE DEMOCRATIC REPUBLIC OF THE)','Congolese Franc','CDF','976','2'),('CONGO (THE)','CFA Franc BEAC','XAF','950','0'),('COOK ISLANDS (THE)','New Zealand Dollar','NZD','554','2'),('COSTA RICA','Costa Rican Colon','CRC','188','2'),('C?TE D\'IVOIRE','CFA Franc BCEAO','XOF','952','0'),('CROATIA','Kuna','HRK','191','2'),('CUBA','Peso Convertible','CUC','931','2'),('CUBA','Cuban Peso','CUP','192','2'),('CURA?AO','Netherlands Antillean Guilder','ANG','532','2'),('CYPRUS','Euro','EUR','978','2'),('CZECH REPUBLIC (THE)','Czech Koruna','CZK','203','2'),('DENMARK','Danish Krone','DKK','208','2'),('DJIBOUTI','Djibouti Franc','DJF','262','0'),('DOMINICA','East Caribbean Dollar','XCD','951','2'),('DOMINICAN REPUBLIC (THE)','Dominican Peso','DOP','214','2'),('ECUADOR','US Dollar','USD','840','2'),('EGYPT','Egyptian Pound','EGP','818','2'),('EL SALVADOR','El Salvador Colon','SVC','222','2'),('EL SALVADOR','US Dollar','USD','840','2'),('EQUATORIAL GUINEA','CFA Franc BEAC','XAF','950','0'),('ERITREA','Nakfa','ERN','232','2'),('ESTONIA','Euro','EUR','978','2'),('ETHIOPIA','Ethiopian Birr','ETB','230','2'),('EUROPEAN UNION','Euro','EUR','978','2'),('FALKLAND ISLANDS (THE) [MALVINAS]','Falkland Islands Pound','FKP','238','2'),('FAROE ISLANDS (THE)','Danish Krone','DKK','208','2'),('FIJI','Fiji Dollar','FJD','242','2'),('FINLAND','Euro','EUR','978','2'),('FRANCE','Euro','EUR','978','2'),('FRENCH GUIANA','Euro','EUR','978','2'),('FRENCH POLYNESIA','CFP Franc','XPF','953','0'),('FRENCH SOUTHERN TERRITORIES (THE)','Euro','EUR','978','2'),('GABON','CFA Franc BEAC','XAF','950','0'),('GAMBIA (THE)','Dalasi','GMD','270','2'),('GEORGIA','Lari','GEL','981','2'),('GERMANY','Euro','EUR','978','2'),('GHANA','Ghana Cedi','GHS','936','2'),('GIBRALTAR','Gibraltar Pound','GIP','292','2'),('GREECE','Euro','EUR','978','2'),('GREENLAND','Danish Krone','DKK','208','2'),('GRENADA','East Caribbean Dollar','XCD','951','2'),('GUADELOUPE','Euro','EUR','978','2'),('GUAM','US Dollar','USD','840','2'),('GUATEMALA','Quetzal','GTQ','320','2'),('GUERNSEY','Pound Sterling','GBP','826','2'),('GUINEA','Guinea Franc','GNF','324','0'),('GUINEA-BISSAU','CFA Franc BCEAO','XOF','952','0'),('GUYANA','Guyana Dollar','GYD','328','2'),('HAITI','Gourde','HTG','332','2'),('HAITI','US Dollar','USD','840','2'),('HEARD ISLAND AND McDONALD ISLANDS','Australian Dollar','AUD','036','2'),('HOLY SEE (THE)','Euro','EUR','978','2'),('HONDURAS','Lempira','HNL','340','2'),('HONG KONG','Hong Kong Dollar','HKD','344','2'),('HUNGARY','Forint','HUF','348','2'),('ICELAND','Iceland Krona','ISK','352','0'),('INDIA','Indian Rupee','INR','356','2'),('INDONESIA','Rupiah','IDR','360','2'),('INTERNATIONAL MONETARY FUND (IMF)?','SDR (Special Drawing Right)','XDR','960','N.A.'),('IRAN (ISLAMIC REPUBLIC OF)','Iranian Rial','IRR','364','2'),('IRAQ','Iraqi Dinar','IQD','368','3'),('IRELAND','Euro','EUR','978','2'),('ISLE OF MAN','Pound Sterling','GBP','826','2'),('ISRAEL','New Israeli Sheqel','ILS','376','2'),('ITALY','Euro','EUR','978','2'),('JAMAICA','Jamaican Dollar','JMD','388','2'),('JAPAN','Yen','JPY','392','0'),('JERSEY','Pound Sterling','GBP','826','2'),('JORDAN','Jordanian Dinar','JOD','400','3'),('KAZAKHSTAN','Tenge','KZT','398','2'),('KENYA','Kenyan Shilling','KES','404','2'),('KIRIBATI','Australian Dollar','AUD','036','2'),('KOREA (THE DEMOCRATIC PEOPLE?S REPUBLIC OF)','North Korean Won','KPW','408','2'),('KOREA (THE REPUBLIC OF)','Won','KRW','410','0'),('KUWAIT','Kuwaiti Dinar','KWD','414','3'),('KYRGYZSTAN','Som','KGS','417','2'),('LAO PEOPLE?S DEMOCRATIC REPUBLIC (THE)','Kip','LAK','418','2'),('LATVIA','Euro','EUR','978','2'),('LEBANON','Lebanese Pound','LBP','422','2'),('LESOTHO','Loti','LSL','426','2'),('LESOTHO','Rand','ZAR','710','2'),('LIBERIA','Liberian Dollar','LRD','430','2'),('LIBYA','Libyan Dinar','LYD','434','3'),('LIECHTENSTEIN','Swiss Franc','CHF','756','2'),('LITHUANIA','Euro','EUR','978','2'),('LUXEMBOURG','Euro','EUR','978','2'),('MACAO','Pataca','MOP','446','2'),('MACEDONIA (THE FORMER YUGOSLAV REPUBLIC OF)','Denar','MKD','807','2'),('MADAGASCAR','Malagasy Ariary','MGA','969','2'),('MALAWI','Kwacha','MWK','454','2'),('MALAYSIA','Malaysian Ringgit','MYR','458','2'),('MALDIVES','Rufiyaa','MVR','462','2'),('MALI','CFA Franc BCEAO','XOF','952','0'),('MALTA','Euro','EUR','978','2'),('MARSHALL ISLANDS (THE)','US Dollar','USD','840','2'),('MARTINIQUE','Euro','EUR','978','2'),('MAURITANIA','Ouguiya','MRO','478','2'),('MAURITIUS','Mauritius Rupee','MUR','480','2'),('MAYOTTE','Euro','EUR','978','2'),('MEMBER COUNTRIES OF THE AFRICAN DEVELOPMENT BANK GROUP','ADB Unit of Account','XUA','965','N.A.'),('MEXICO','Mexican Peso','MXN','484','2'),('MEXICO','Mexican Unidad de Inversion (UDI)','MXV','979','2'),('MICRONESIA (FEDERATED STATES OF)','US Dollar','USD','840','2'),('MOLDOVA (THE REPUBLIC OF)','Moldovan Leu','MDL','498','2'),('MONACO','Euro','EUR','978','2'),('MONGOLIA','Tugrik','MNT','496','2'),('MONTENEGRO','Euro','EUR','978','2'),('MONTSERRAT','East Caribbean Dollar','XCD','951','2'),('MOROCCO','Moroccan Dirham','MAD','504','2'),('MOZAMBIQUE','Mozambique Metical','MZN','943','2'),('MYANMAR','Kyat','MMK','104','2'),('NAMIBIA','Namibia Dollar','NAD','516','2'),('NAMIBIA','Rand','ZAR','710','2'),('NAURU','Australian Dollar','AUD','036','2'),('NEPAL','Nepalese Rupee','NPR','524','2'),('NETHERLANDS (THE)','Euro','EUR','978','2'),('NEW CALEDONIA','CFP Franc','XPF','953','0'),('NEW ZEALAND','New Zealand Dollar','NZD','554','2'),('NICARAGUA','Cordoba Oro','NIO','558','2'),('NIGER (THE)','CFA Franc BCEAO','XOF','952','0'),('NIGERIA','Naira','NGN','566','2'),('NIUE','New Zealand Dollar','NZD','554','2'),('NORFOLK ISLAND','Australian Dollar','AUD','036','2'),('NORTHERN MARIANA ISLANDS (THE)','US Dollar','USD','840','2'),('NORWAY','Norwegian Krone','NOK','578','2'),('OMAN','Rial Omani','OMR','512','3'),('PAKISTAN','Pakistan Rupee','PKR','586','2'),('PALAU','US Dollar','USD','840','2'),('PALESTINE, STATE OF','No universal currency','','',''),('PANAMA','Balboa','PAB','590','2'),('PANAMA','US Dollar','USD','840','2'),('PAPUA NEW GUINEA','Kina','PGK','598','2'),('PARAGUAY','Guarani','PYG','600','0'),('PERU','Nuevo Sol','PEN','604','2'),('PHILIPPINES (THE)','Philippine Peso','PHP','608','2'),('PITCAIRN','New Zealand Dollar','NZD','554','2'),('POLAND','Zloty','PLN','985','2'),('PORTUGAL','Euro','EUR','978','2'),('PUERTO RICO','US Dollar','USD','840','2'),('QATAR','Qatari Rial','QAR','634','2'),('R?UNION','Euro','EUR','978','2'),('ROMANIA','Romanian Leu','RON','946','2'),('RUSSIAN FEDERATION (THE)','Russian Ruble','RUB','643','2'),('RWANDA','Rwanda Franc','RWF','646','0'),('SAINT BARTH?LEMY','Euro','EUR','978','2'),('SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA','Saint Helena Pound','SHP','654','2'),('SAINT KITTS AND NEVIS','East Caribbean Dollar','XCD','951','2'),('SAINT LUCIA','East Caribbean Dollar','XCD','951','2'),('SAINT MARTIN (FRENCH PART)','Euro','EUR','978','2'),('SAINT PIERRE AND MIQUELON','Euro','EUR','978','2'),('SAINT VINCENT AND THE GRENADINES','East Caribbean Dollar','XCD','951','2'),('SAMOA','Tala','WST','882','2'),('SAN MARINO','Euro','EUR','978','2'),('SAO TOME AND PRINCIPE','Dobra','STD','678','2'),('SAUDI ARABIA','Saudi Riyal','SAR','682','2'),('SENEGAL','CFA Franc BCEAO','XOF','952','0'),('SERBIA','Serbian Dinar','RSD','941','2'),('SEYCHELLES','Seychelles Rupee','SCR','690','2'),('SIERRA LEONE','Leone','SLL','694','2'),('SINGAPORE','Singapore Dollar','SGD','702','2'),('SINT MAARTEN (DUTCH PART)','Netherlands Antillean Guilder','ANG','532','2'),('SISTEMA UNITARIO DE COMPENSACION REGIONAL DE PAGOS \"SUCRE\"','Sucre','XSU','994','N.A.'),('SLOVAKIA','Euro','EUR','978','2'),('SLOVENIA','Euro','EUR','978','2'),('SOLOMON ISLANDS','Solomon Islands Dollar','SBD','090','2'),('SOMALIA','Somali Shilling','SOS','706','2'),('SOUTH AFRICA','Rand','ZAR','710','2'),('SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','No universal currency','','',''),('SOUTH SUDAN','South Sudanese Pound','SSP','728','2'),('SPAIN','Euro','EUR','978','2'),('SRI LANKA','Sri Lanka Rupee','LKR','144','2'),('SUDAN (THE)','Sudanese Pound','SDG','938','2'),('SURINAME','Surinam Dollar','SRD','968','2'),('SVALBARD AND JAN MAYEN','Norwegian Krone','NOK','578','2'),('SWAZILAND','Lilangeni','SZL','748','2'),('SWEDEN','Swedish Krona','SEK','752','2'),('SWITZERLAND','WIR Euro','CHE','947','2'),('SWITZERLAND','Swiss Franc','CHF','756','2'),('SWITZERLAND','WIR Franc','CHW','948','2'),('SYRIAN ARAB REPUBLIC','Syrian Pound','SYP','760','2'),('TAIWAN (PROVINCE OF CHINA)','New Taiwan Dollar','TWD','901','2'),('TAJIKISTAN','Somoni','TJS','972','2'),('TANZANIA, UNITED REPUBLIC OF','Tanzanian Shilling','TZS','834','2'),('THAILAND','Baht','THB','764','2'),('TIMOR-LESTE','US Dollar','USD','840','2'),('TOGO','CFA Franc BCEAO','XOF','952','0'),('TOKELAU','New Zealand Dollar','NZD','554','2'),('TONGA','Pa?anga','TOP','776','2'),('TRINIDAD AND TOBAGO','Trinidad and Tobago Dollar','TTD','780','2'),('TUNISIA','Tunisian Dinar','TND','788','3'),('TURKEY','Turkish Lira','TRY','949','2'),('TURKMENISTAN','Turkmenistan New Manat','TMT','934','2'),('TURKS AND CAICOS ISLANDS (THE)','US Dollar','USD','840','2'),('TUVALU','Australian Dollar','AUD','036','2'),('UGANDA','Uganda Shilling','UGX','800','0'),('UKRAINE','Hryvnia','UAH','980','2'),('UNITED ARAB EMIRATES (THE)','UAE Dirham','AED','784','2'),('UNITED KINGDOM OF GREAT BRITAIN AND NORTHERN IRELAND (THE)','Pound Sterling','GBP','826','2'),('UNITED STATES MINOR OUTLYING ISLANDS (THE)','US Dollar','USD','840','2'),('UNITED STATES OF AMERICA (THE)','US Dollar','USD','840','2'),('UNITED STATES OF AMERICA (THE)','US Dollar (Next day)','USN','997','2'),('URUGUAY','Uruguay Peso en Unidades Indexadas (URUIURUI)','UYI','940','0'),('URUGUAY','Peso Uruguayo','UYU','858','2'),('UZBEKISTAN','Uzbekistan Sum','UZS','860','2'),('VANUATU','Vatu','VUV','548','0'),('VENEZUELA (BOLIVARIAN REPUBLIC OF)','Bolivar','VEF','937','2'),('VIET NAM','Dong','VND','704','0'),('VIRGIN ISLANDS (BRITISH)','US Dollar','USD','840','2'),('VIRGIN ISLANDS (U.S.)','US Dollar','USD','840','2'),('WALLIS AND FUTUNA','CFP Franc','XPF','953','0'),('WESTERN SAHARA','Moroccan Dirham','MAD','504','2'),('YEMEN','Yemeni Rial','YER','886','2'),('ZAMBIA','Zambian Kwacha','ZMW','967','2'),('ZIMBABWE','Zimbabwe Dollar','ZWL','932','2'),('ZZ01_Bond Markets Unit European_EURCO','Bond Markets Unit European Composite Unit (EURCO)','XBA','955','N.A.'),('ZZ02_Bond Markets Unit European_EMU-6','Bond Markets Unit European Monetary Unit (E.M.U.-6)','XBB','956','N.A.'),('ZZ03_Bond Markets Unit European_EUA-9','Bond Markets Unit European Unit of Account 9 (E.U.A.-9)','XBC','957','N.A.'),('ZZ04_Bond Markets Unit European_EUA-17','Bond Markets Unit European Unit of Account 17 (E.U.A.-17)','XBD','958','N.A.'),('ZZ06_Testing_Code','Codes specifically reserved for testing purposes','XTS','963','N.A.'),('ZZ07_No_Currency','The codes assigned for transactions where no currency is involved','XXX','999','N.A.'),('ZZ08_Gold','Gold','XAU','959','N.A.'),('ZZ09_Palladium','Palladium','XPD','964','N.A.'),('ZZ10_Platinum','Platinum','XPT','962','N.A.'),('ZZ11_Silver','Silver','XAG','961','N.A.');
/*!40000 ALTER TABLE `ccodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configs`
--

DROP TABLE IF EXISTS `configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configs` (
  `mykey` varchar(250) NOT NULL,
  `myvalue` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`mykey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configs`
--

LOCK TABLES `configs` WRITE;
/*!40000 ALTER TABLE `configs` DISABLE KEYS */;
INSERT INTO `configs` VALUES ('OPC_DB','moon_opc_mask');
/*!40000 ALTER TABLE `configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract_accounts`
--

DROP TABLE IF EXISTS `contract_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `ccnt_id` varchar(20) NOT NULL,
  `end_date` date NOT NULL COMMENT 'Contrct end date for account',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract_accounts`
--

LOCK TABLES `contract_accounts` WRITE;
/*!40000 ALTER TABLE `contract_accounts` DISABLE KEYS */;
INSERT INTO `contract_accounts` VALUES (1,2,'LAS001','2014-11-27'),(2,2,'007','2015-05-30'),(3,201,'007','2015-03-31'),(4,53,'0009','2014-12-27'),(5,2,'0009','2014-09-30'),(6,52,'0009','2014-09-30'),(7,6,'009-darley','2014-09-30'),(8,6,'124235235','2015-02-28'),(9,217,'LAS001','2014-10-31'),(10,221,'PIX001','2014-10-29');
/*!40000 ALTER TABLE `contract_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract_types`
--

DROP TABLE IF EXISTS `contract_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cnt_id` varchar(20) NOT NULL,
  `cnt_name` varchar(50) NOT NULL,
  `cnt_desc` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract_types`
--

LOCK TABLES `contract_types` WRITE;
/*!40000 ALTER TABLE `contract_types` DISABLE KEYS */;
INSERT INTO `contract_types` VALUES (1,'CM001','Product based discount','Discount based on PRODUCTS only. (DETAILED is important)'),(2,'CM007','Categories Based discount','Discount based on PRODUCTS CATEGORIES only. (DETAILED is important)'),(3,'CM002','Product name based discount','Discount based on PRODUCTS NAME only. (DETAILED is important)');
/*!40000 ALTER TABLE `contract_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contracts`
--

DROP TABLE IF EXISTS `contracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccnt_id` varchar(20) NOT NULL,
  `ccnt_name` varchar(100) NOT NULL,
  `ccnt_desc` varchar(255) NOT NULL,
  `cnt_id` varchar(20) NOT NULL,
  `inputs` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contracts`
--

LOCK TABLES `contracts` WRITE;
/*!40000 ALTER TABLE `contracts` DISABLE KEYS */;
INSERT INTO `contracts` VALUES (12,'LAS001','laser','test','CM002','laser::contains_string::15%::1405900800::1412035200'),(13,'PIX001','pixma pro1','PIXMA PRO-1 coupon','CM001','28::$25::1406955600::1414558800'),(14,'007','TEST007','TEST','CM001','2,3::10%::1409461200::1411966800'),(15,'0009','TEST0009','TEST','CM002','BATTERY::contains_string::$20::1409461200::1411966800'),(16,'EOS001','EOS category discount','Discount on all category which have EOS in its name.','CM007','15,16::15%::1410998400::1417046400'),(17,'009-darley','gps Contract for Spetember','NA','CM002','gps::contains_string::20%::1409461200::1411966800'),(18,'124235235','CATEGORY BASED 1111','TEST','CM007','14,15,16::5%::1412139600::1425103200');
/*!40000 ALTER TABLE `contracts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments_security`
--

DROP TABLE IF EXISTS `departments_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments_security` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `departments_id` int(11) NOT NULL,
  `modules_id` int(11) NOT NULL,
  `actions_id` int(11) NOT NULL,
  `security_levels_id` int(11) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=484 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments_security`
--

LOCK TABLES `departments_security` WRITE;
/*!40000 ALTER TABLE `departments_security` DISABLE KEYS */;
INSERT INTO `departments_security` VALUES (188,2,47,1,2,0),(189,2,47,2,5,0),(190,2,47,3,5,0),(191,2,47,4,5,0),(192,2,47,5,5,0),(193,2,47,6,5,0),(194,3,47,1,2,0),(195,3,47,2,2,0),(196,3,47,3,2,0),(197,3,47,4,2,0),(198,3,47,5,2,0),(199,3,47,6,2,0),(446,1,52,1,2,0),(447,1,52,2,2,0),(448,1,52,3,2,0),(449,1,52,4,2,0),(450,1,52,5,2,0),(451,1,52,6,2,0),(452,1,47,1,2,0),(453,1,47,2,2,0),(454,1,47,3,2,0),(455,1,47,4,2,0),(456,1,47,5,1,0),(457,1,47,6,1,0),(458,1,54,1,2,0),(459,1,54,2,2,0),(460,1,54,3,2,0),(461,1,54,4,2,0),(462,1,51,1,1,0),(463,1,51,2,1,0),(464,1,51,3,1,0),(465,1,51,4,1,0),(466,1,51,5,1,0),(467,1,51,6,1,0),(468,1,50,1,1,0),(469,1,50,2,1,0),(470,1,50,3,1,0),(471,1,50,4,1,0),(472,1,50,5,1,0),(473,1,50,6,1,0),(474,1,49,1,2,0),(475,1,49,2,2,0),(476,1,49,3,2,0),(477,1,49,4,2,0),(478,1,49,5,2,0),(479,1,49,6,2,0),(480,1,55,1,5,0),(481,1,55,2,5,0),(482,1,55,3,5,0),(483,1,55,4,5,0);
/*!40000 ALTER TABLE `departments_security` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_shortcut_fields`
--

DROP TABLE IF EXISTS `module_shortcut_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_shortcut_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_shortcuts_id` int(11) DEFAULT NULL,
  `fields_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_shortcut_fields`
--

LOCK TABLES `module_shortcut_fields` WRITE;
/*!40000 ALTER TABLE `module_shortcut_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_shortcut_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_shortcuts`
--

DROP TABLE IF EXISTS `module_shortcuts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_shortcuts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `module_shortcut_ids` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_shortcuts`
--

LOCK TABLES `module_shortcuts` WRITE;
/*!40000 ALTER TABLE `module_shortcuts` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_shortcuts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `label` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (36,'2015-06-02 22:44:59','2015-06-15 18:31:23',1,1,'Departments','Departments','',1,1,0),(43,'2015-06-15 18:33:05','2015-06-15 18:41:02',1,1,'Teams','Teams','Teams',1,1,0),(44,'2015-06-15 18:33:22','2015-06-15 18:41:02',1,1,'Roles','Roles','Roles',1,1,0),(47,'2015-06-15 19:51:45','2015-07-28 16:06:54',1,1,'Contacts','Contacts','',0,1,0),(48,'2015-06-15 19:51:45','2015-06-16 02:04:23',1,1,'Users','Users','',1,1,0),(49,'2015-06-16 10:53:44','2015-07-28 16:06:23',1,1,'Quotes','Quotes','',2,1,0),(50,'2015-06-23 00:28:45','2015-06-23 00:28:45',1,1,'Leads','Leads','',0,1,0),(51,'2015-07-20 20:19:36','2015-07-28 16:06:23',1,1,'Inventory','Inventory','',2,1,0),(52,'2015-07-28 15:27:48','2015-07-29 01:24:03',1,1,'Accounts','Accounts','Customer Account Details',0,1,0),(54,'2015-07-28 16:14:16','2015-07-28 16:14:16',1,1,'Contracts','Contracts','',2,1,0),(55,'2015-07-29 01:40:33','2015-07-29 01:40:33',1,1,'Test','Test','',0,0,0);
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules_actions`
--

DROP TABLE IF EXISTS `modules_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modules_id` int(11) NOT NULL,
  `actions_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules_actions`
--

LOCK TABLES `modules_actions` WRITE;
/*!40000 ALTER TABLE `modules_actions` DISABLE KEYS */;
INSERT INTO `modules_actions` VALUES (146,36,1,1,0),(147,36,2,1,0),(148,36,3,1,0),(149,36,4,1,0),(150,43,1,1,0),(151,43,2,1,0),(152,43,3,1,0),(153,44,1,1,0),(154,44,2,1,0),(155,44,3,1,0),(165,47,1,1,0),(166,47,2,1,0),(167,47,3,1,0),(168,47,4,1,0),(169,47,5,1,0),(170,47,6,1,0),(171,49,1,1,0),(172,49,2,1,0),(173,49,3,1,0),(174,49,4,1,0),(175,49,5,1,0),(176,49,6,1,0),(177,50,1,1,0),(178,50,2,1,0),(179,50,3,1,0),(180,50,4,1,0),(181,50,5,1,0),(182,50,6,1,0),(183,51,1,1,0),(184,51,2,1,0),(185,51,3,1,0),(186,51,4,1,0),(187,51,5,1,0),(188,51,6,1,0),(201,54,1,1,0),(202,54,2,1,0),(203,54,3,1,0),(204,54,4,1,0),(217,55,1,1,0),(218,55,2,1,0),(219,55,3,1,0),(220,55,4,1,0),(221,52,1,1,0),(222,52,2,1,0),(223,52,3,1,0),(224,52,4,1,0),(225,52,5,1,0),(226,52,6,1,0);
/*!40000 ALTER TABLE `modules_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_departments_rm`
--

DROP TABLE IF EXISTS `moon_departments_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_departments_rm` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_departments_rm`
--

LOCK TABLES `moon_departments_rm` WRITE;
/*!40000 ALTER TABLE `moon_departments_rm` DISABLE KEYS */;
INSERT INTO `moon_departments_rm` VALUES (1,'Department 1','This is sample description'),(2,'Department 2','This is sample description.');
/*!40000 ALTER TABLE `moon_departments_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_eactions_rm`
--

DROP TABLE IF EXISTS `moon_eactions_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_eactions_rm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `iactions` varchar(100) NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_eactions_rm`
--

LOCK TABLES `moon_eactions_rm` WRITE;
/*!40000 ALTER TABLE `moon_eactions_rm` DISABLE KEYS */;
INSERT INTO `moon_eactions_rm` VALUES (9,'Show Opportunities','5',5),(10,'Show Opportunities , Add Notes ','5,6',5),(11,'Show Opportunities and Add,Edit & Delete Notes ','5,6,7',5),(12,'Administrate Opportunities','5,6,7',5),(13,'Opportunities Full Control','5,6,7,8',5);
/*!40000 ALTER TABLE `moon_eactions_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_group_module_relation_rm`
--

DROP TABLE IF EXISTS `moon_group_module_relation_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_group_module_relation_rm` (
  `group_module_rel_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `permission` varchar(20) NOT NULL COMMENT 'READ, WRITE, ADMIN',
  PRIMARY KEY (`group_module_rel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_group_module_relation_rm`
--

LOCK TABLES `moon_group_module_relation_rm` WRITE;
/*!40000 ALTER TABLE `moon_group_module_relation_rm` DISABLE KEYS */;
INSERT INTO `moon_group_module_relation_rm` VALUES (5,2,2,'ADMIN'),(6,2,3,'ADMIN'),(14,3,3,'WRITE'),(18,1,1,'ADMIN'),(19,1,2,'ADMIN'),(20,1,3,'ADMIN'),(21,1,4,'ADMIN');
/*!40000 ALTER TABLE `moon_group_module_relation_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_groups_rm`
--

DROP TABLE IF EXISTS `moon_groups_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_groups_rm` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_groups_rm`
--

LOCK TABLES `moon_groups_rm` WRITE;
/*!40000 ALTER TABLE `moon_groups_rm` DISABLE KEYS */;
INSERT INTO `moon_groups_rm` VALUES (1,'SUPER ADMIN','Have access to all modules'),(2,'ADMIN','Don\'t have access to main groups and group-module assign.'),(3,'SALES team','sales team member who create quotes');
/*!40000 ALTER TABLE `moon_groups_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_iactions_rm`
--

DROP TABLE IF EXISTS `moon_iactions_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_iactions_rm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_iactions_rm`
--

LOCK TABLES `moon_iactions_rm` WRITE;
/*!40000 ALTER TABLE `moon_iactions_rm` DISABLE KEYS */;
INSERT INTO `moon_iactions_rm` VALUES (1,'Add Quotes'),(2,'View Quotes'),(3,'Add Module'),(4,'sss'),(5,'showMeBasic'),(6,'showMePower'),(7,'showMeAdmin'),(8,'showMeSuper');
/*!40000 ALTER TABLE `moon_iactions_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_modules_rm`
--

DROP TABLE IF EXISTS `moon_modules_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_modules_rm` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_modules_rm`
--

LOCK TABLES `moon_modules_rm` WRITE;
/*!40000 ALTER TABLE `moon_modules_rm` DISABLE KEYS */;
INSERT INTO `moon_modules_rm` VALUES (1,'user_groups','User Groups','This module will add main groups and it is accessible by super user only.'),(2,'users','Users','This will be accessible to super admin and admin.'),(3,'quotes','Quotes','To create a quote for customer.'),(4,'modules','Modules','this is modules which will be used in CMS'),(5,'opportunity','OPPORTUNITIES','This is the Opportunities Module');
/*!40000 ALTER TABLE `moon_modules_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_product_category_rm`
--

DROP TABLE IF EXISTS `moon_product_category_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_product_category_rm` (
  `product_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `category_desc` text NOT NULL,
  `parent_category` int(11) NOT NULL,
  PRIMARY KEY (`product_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_product_category_rm`
--

LOCK TABLES `moon_product_category_rm` WRITE;
/*!40000 ALTER TABLE `moon_product_category_rm` DISABLE KEYS */;
INSERT INTO `moon_product_category_rm` VALUES (14,'Camera, Cam Recorder, Lens & Accessories','You can find all camera, cam recorder , lens and all it\'s accessories like lens, battery, wireless file transfer system etc...',0),(15,'EOS Digital SLR Camera','This category includes all EOS Digital SLR Cameras.',14),(16,'EOS Camera Accessories','This category includes all EOS Camera Accessories.',14),(17,'Battery Grip','Battery Grips of EOS cameras',16),(18,'GPS Receiver','All GPS Receiver for EOS Camares.',16),(19,'Wireless File Transmitter','Wireless File Transmitter for EOS cameras.',16),(20,'Digital Camcorder','All Digital Camcorders of canon.',14),(21,'Professional Camcorder','all digital Professional Camcorders.',20),(22,'Consumer Camcorder','all digital Consumer Camcorders',20),(23,'Digital Compact Camera','All IXUS and PowerShot digital cameras.',14),(24,'All printers & Scanners','This category includes all scanner and printer of canon company.',0),(25,'Laser Printer','Contains Laser Printers of all types.',24),(26,'Photo Printer','Contains Photo Printers of all types.',24),(29,'LCD, LED & Televisions','Contains all LCD, LED & Televisions.',0),(30,'LED TVs','All LED TVs',29),(31,'LCD TVs','All LCD TVs',29),(32,'Photo Scanner','All Photo Scanners.',24),(34,'EF Lenses','All types of EF Lenses.',14),(35,'EF Lens Accessories','All type of EF Lens Accessories.',14),(36,'Projector','All type of projectors.',0),(37,'Communication Camera','All Communication Cameras.',14),(38,'Cinema EOS system - camera and lenses','Cinema EOS system cameras and lenses',14),(39,'Cosmetics','Cosmetics',0),(40,'Perfumes','Perfumes',39);
/*!40000 ALTER TABLE `moon_product_category_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_quotes`
--

DROP TABLE IF EXISTS `moon_quotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_quotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL COMMENT 'dweller_id who create quote',
  `modified_by` int(11) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `quote_id` varchar(100) NOT NULL,
  `quote_number` varchar(20) NOT NULL,
  `quote_name` varchar(100) NOT NULL,
  `quote_department` int(11) DEFAULT NULL,
  `xml_path` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `total_amount` float(10,2) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_quotes`
--

LOCK TABLES `moon_quotes` WRITE;
/*!40000 ALTER TABLE `moon_quotes` DISABLE KEYS */;
INSERT INTO `moon_quotes` VALUES (1,'2014-09-18 06:42:33','2015-06-16 11:45:34',1,1,1,1,217,'0uW0HrbVkMZfrcRCp6cf','894453388825','Sample Quote 1',13,'/var/hosting/moon_repo_quotes//quote_541a7ed91ad7e/0uW0HrbVkMZfrcRCp6cf/',3,'Maulik Bavishi',4599.00,0),(2,'2014-09-29 14:36:23','2015-06-16 11:45:34',1,0,1,1,6,'s3MTPCvHrHLwCuv8AOB6','444051813101','my test 001',2,'/var/hosting/moon_repo_quotes//quote_541a7ed91ad7e/s3MTPCvHrHLwCuv8AOB6/',4,'Agnew Lumber Company',65894.00,0),(3,'2014-10-07 10:33:44','2015-06-16 11:45:34',0,0,1,1,2,'4dKHXJ9WhLOZJvHLl6km','643176267389','TEST 001',2,'/var/hosting/moon_repo_quotes//quote_541a7ed91ad7e/4dKHXJ9WhLOZJvHLl6km/',3,'Darleys stephen1',1375.25,0),(4,'2014-10-07 10:38:11','2015-06-16 11:45:34',0,0,1,1,8,'JvL7AoVSFXyDgLGcSK2u','592873047478','TEST 002',2,'/var/hosting/moon_repo_quotes//quote_541a7ed91ad7e/JvL7AoVSFXyDgLGcSK2u/',1,'Albers Mill Construction LLC',1159.00,0),(5,'2014-10-30 16:01:01','2015-06-16 11:45:35',1,1,1,1,3,'LaPK95eeksEL0ARaxrix','936590038333','NEW QUOTE 001',2,'/var/hosting/moon_repo_quotes//quote_541a7ed91ad7e/LaPK95eeksEL0ARaxrix/',2,'A Glass Enterprise',1998.00,0),(6,'2014-10-30 21:45:12','2015-06-16 11:45:53',1,1,1,1,127,'VC7uqhEaB97DVjWpZwI1','413904995797','sample',2,'/var/hosting/moon_repo_quotes//quote_541a7ed91ad7e/VC7uqhEaB97DVjWpZwI1/',6,'Oakville Hardware & Supply',18482.00,0),(7,'2014-11-03 02:15:27','2015-06-16 11:45:35',1,0,1,1,2,'D6bUaGpw5zQ2l63HBnwR','209033967787','fffff',3,'/var/hosting/moon_repo_quotes//quote_541a7ed91ad7e/D6bUaGpw5zQ2l63HBnwR/',2,'Darleys stephen1',1149.00,0),(8,'2014-11-11 15:45:19','2015-06-16 11:45:35',3,0,1,1,115,'xeseUXUou9VnJWdhSj6X','758643008070','dales dept quote 007',1,'/var/hosting/moon_repo_quotes//quote_541a7ed91ad7e/xeseUXUou9VnJWdhSj6X/',1,'Miles Sand & Gravel - Shelton',21445.00,0);
/*!40000 ALTER TABLE `moon_quotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_quotes_products`
--

DROP TABLE IF EXISTS `moon_quotes_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_quotes_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_id` varchar(100) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `unit_price` float(10,2) NOT NULL,
  `currency_code` varchar(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `discount_percentage` int(11) NOT NULL,
  `total` float(10,2) NOT NULL,
  `contract_price` varchar(100) NOT NULL,
  `contract_applied` varchar(100) NOT NULL,
  `contract_code` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_quotes_products`
--

LOCK TABLES `moon_quotes_products` WRITE;
/*!40000 ALTER TABLE `moon_quotes_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `moon_quotes_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_user_group_relation_rm`
--

DROP TABLE IF EXISTS `moon_user_group_relation_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_user_group_relation_rm` (
  `user_group_relation_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `dweller_id` int(11) NOT NULL,
  PRIMARY KEY (`user_group_relation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_user_group_relation_rm`
--

LOCK TABLES `moon_user_group_relation_rm` WRITE;
/*!40000 ALTER TABLE `moon_user_group_relation_rm` DISABLE KEYS */;
INSERT INTO `moon_user_group_relation_rm` VALUES (1,1,1),(2,2,2),(5,3,3);
/*!40000 ALTER TABLE `moon_user_group_relation_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_user_roles_rm`
--

DROP TABLE IF EXISTS `moon_user_roles_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_user_roles_rm` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `eactions_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_user_roles_rm`
--

LOCK TABLES `moon_user_roles_rm` WRITE;
/*!40000 ALTER TABLE `moon_user_roles_rm` DISABLE KEYS */;
INSERT INTO `moon_user_roles_rm` VALUES (25,1,13),(27,4,9);
/*!40000 ALTER TABLE `moon_user_roles_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moon_user_to_department_rm`
--

DROP TABLE IF EXISTS `moon_user_to_department_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moon_user_to_department_rm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moon_user_to_department_rm`
--

LOCK TABLES `moon_user_to_department_rm` WRITE;
/*!40000 ALTER TABLE `moon_user_to_department_rm` DISABLE KEYS */;
INSERT INTO `moon_user_to_department_rm` VALUES (6,1,1),(7,1,2),(8,2,1),(11,4,1),(12,4,2),(13,3,1),(14,3,2);
/*!40000 ALTER TABLE `moon_user_to_department_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes_rm`
--

DROP TABLE IF EXISTS `notes_rm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes_rm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ntype` int(11) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `notes` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  `entity` varchar(45) DEFAULT NULL COMMENT ' ',
  `entity_id` int(11) DEFAULT NULL,
  `nfilename` varchar(250) DEFAULT NULL,
  `nfiletype` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes_rm`
--

LOCK TABLES `notes_rm` WRITE;
/*!40000 ALTER TABLE `notes_rm` DISABLE KEYS */;
INSERT INTO `notes_rm` VALUES (1,1,'2014-07-10 00:00:00',1,'this is note1!!!',1,'opportunity',1,'',''),(10,NULL,'2014-07-10 08:08:03',1,'file test working!!!',1,'opportunity',1,'',''),(11,NULL,'2014-07-11 18:00:39',1,'',0,'',0,'',''),(12,NULL,'2014-07-11 18:19:09',1,'dfsf',1,'opportunity',1,'',''),(13,NULL,'2014-07-15 11:55:03',1,'676ttyty',1,'opportunity',1,'',''),(14,NULL,'2014-07-15 11:56:25',1,'was',1,'opportunity',1,'110824 - Account narrative.pdf;(Scoop - FT019) Release and Deployment Plan.docx','application/pdf;application/vnd.openxmlformats-officedocument.wordprocessingml.document'),(15,NULL,'2014-07-15 11:59:26',1,'hhh',1,'opportunity',1,'',''),(16,NULL,'2014-07-15 12:01:57',1,'wded',1,'opportunity',1,'bigstock-Business-meeting-in-an-office-21258410.gif','image/gif'),(17,NULL,'2014-07-15 12:02:53',1,'w2222',1,'opportunity',1,'test-564.txt','text/plain'),(18,NULL,'2014-09-17 06:32:41',1,'PNG file upload test',0,'opportunity',1,'649_1.png','image/png'),(19,NULL,'2014-09-17 06:49:56',1,'test file uploading...',0,'opportunity',1,'Eons_of_Silence_by_AngelAsylum.jpg','image/jpeg'),(20,NULL,'2014-10-06 10:56:37',1,'ssfd',0,'opportunity',1,'Capture.PNG','image/png'),(21,NULL,'2014-10-06 10:57:00',1,'sfsdf',0,'opportunity',1,'quote stat.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document'),(22,NULL,'2014-10-06 14:19:30',1,'my test',0,'opportunity',1,'','');
/*!40000 ALTER TABLE `notes_rm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_templates`
--

DROP TABLE IF EXISTS `quote_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote_templates`
--

LOCK TABLES `quote_templates` WRITE;
/*!40000 ALTER TABLE `quote_templates` DISABLE KEYS */;
INSERT INTO `quote_templates` VALUES (31,'MntpWyqGLZvPzsl4cuWqqlraT','TPL 001','TPL001','/var/hosting/moon_repo_templates//MntpWyqGLZvPzsl4cuWqqlraT/',1,'2014-09-18 06:36:57'),(32,'i0tGrItOcSKKirvhWlQoGVBaR','TPL 002','TPL002','/var/hosting/moon_repo_templates//i0tGrItOcSKKirvhWlQoGVBaR/',1,'2014-09-18 06:40:39'),(33,'P5IXXRrBkURwu5B5kfyp6V1RI','TEST1111','TPL001','/var/hosting/moon_repo_templates//P5IXXRrBkURwu5B5kfyp6V1RI/',1,'2014-09-30 17:11:46'),(34,'UYZjdBiEuW1OJ6EfT7ePd1Lk5','dsdsdsd','TPL001','/var/hosting/moon_repo_templates//UYZjdBiEuW1OJ6EfT7ePd1Lk5/',0,'2014-10-08 09:09:53');
/*!40000 ALTER TABLE `quote_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_security`
--

DROP TABLE IF EXISTS `roles_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_security` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roles_id` int(11) NOT NULL,
  `modules_id` int(11) NOT NULL,
  `actions_id` int(11) NOT NULL,
  `security_levels_id` int(11) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_security`
--

LOCK TABLES `roles_security` WRITE;
/*!40000 ALTER TABLE `roles_security` DISABLE KEYS */;
INSERT INTO `roles_security` VALUES (84,1,47,1,1,0),(85,1,47,2,1,0),(86,1,47,3,2,0),(87,1,47,4,2,0),(88,1,47,5,1,0),(89,1,47,6,1,0);
/*!40000 ALTER TABLE `roles_security` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `security_levels`
--

DROP TABLE IF EXISTS `security_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `security_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `level` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `security_levels`
--

LOCK TABLES `security_levels` WRITE;
/*!40000 ALTER TABLE `security_levels` DISABLE KEYS */;
INSERT INTO `security_levels` VALUES (1,'Global',1,0),(2,'Department',2,0),(3,'Team',3,0),(4,'Users',4,0),(6,'No Access',5,0);
/*!40000 ALTER TABLE `security_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams_security`
--

DROP TABLE IF EXISTS `teams_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams_security` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `teams_id` int(11) NOT NULL,
  `modules_id` int(11) NOT NULL,
  `actions_id` int(11) NOT NULL,
  `security_levels_id` int(11) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams_security`
--

LOCK TABLES `teams_security` WRITE;
/*!40000 ALTER TABLE `teams_security` DISABLE KEYS */;
INSERT INTO `teams_security` VALUES (74,2,47,1,3,0),(75,2,47,2,3,0),(76,2,47,3,3,0),(77,2,47,4,3,0),(78,2,47,5,5,0),(79,2,47,6,5,0),(152,1,47,1,3,0),(153,1,47,2,3,0),(154,1,47,3,3,0),(155,1,47,4,5,0),(156,1,47,5,5,0),(157,1,47,6,5,0),(158,1,50,1,2,0),(159,1,50,2,3,0),(160,1,50,3,3,0),(161,1,50,4,3,0),(162,1,50,5,3,0),(163,1,50,6,3,0),(164,1,49,1,3,0),(165,1,49,2,3,0),(166,1,49,3,3,0),(167,1,49,4,3,0),(168,1,49,5,3,0),(169,1,49,6,5,0);
/*!40000 ALTER TABLE `teams_security` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_security`
--

DROP TABLE IF EXISTS `users_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_security` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `modules_id` int(11) NOT NULL,
  `actions_id` int(11) NOT NULL,
  `security_levels_id` int(11) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_security`
--

LOCK TABLES `users_security` WRITE;
/*!40000 ALTER TABLE `users_security` DISABLE KEYS */;
INSERT INTO `users_security` VALUES (37,3,47,1,3,0),(38,3,47,2,3,0),(39,3,47,3,3,0),(40,3,47,4,3,0),(41,3,47,5,5,0),(42,3,47,6,5,0);
/*!40000 ALTER TABLE `users_security` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-01 18:29:29
