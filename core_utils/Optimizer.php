<?php
/**
 * Optimizer
 *
 * LICENSE
 *
Copyright 2015 Virtuous Consulting Services

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 * @copyright  Copyright (c) 2015 Virtuous Consulting Services.  (http://www.virtuouscs.com)
 * @Owner     Darley Stephen (http://www.darleys.org)
 * @Author     Darley Stephen (http://www.darleys.org)
 */


class Optimizer  {
    public static $jsLNK;
    public static function psHEAD() {
        global $optimizer;
        //self::psLNKs("JS");

    }//END Function process Head psHEAD
    /*
     * Function Process Link(s) is a function to auto Load Javascript , CSS and Fonts
     * lnkType is the type of Header component you want to optimize , CSS , JS
     * //$lnkFORMAT - LINK Format is the format of file you want to include 1==normal version , 2==min version, 3==gzip
    */

    /*
     * 000000000000000000000000000000000000000000 COMPLETE THE PS_LNK_SPLIT 0000000000000000000000000
     */
    static function psLNKs($LNK,$lnkTyp,$repoTyp) {
        global $optimizer;


        foreach($LNK as $eLNK) {
            if($repoTyp==3) {
                //echo "***************************************************<BR>";
                //echo $eLNK;
                //echo "***************************************************<BR>";
                switch($lnkTyp) {
                    case "JS" :
                        echo "<script src='".$eLNK."'></script>";
                        break;
                    case "CSS" :
                        echo "<link rel='stylesheet' href='".$eLNK."'/>";
                        break;
                }//END SWITCH for link type (css , js,...)
            }
            else {
                $lnkDIRs = Foundry_ManageDirectories::listDirectoriesByPath(unserialize(constant($lnkTyp.'_REPO'))[$repoTyp]);
                array_unshift($lnkDIRs, unserialize(constant($lnkTyp.'_REPO'))[$repoTyp].DS);
                $eLNK=explode("/",$eLNK);
                $lnkN=$eLNK[0];
                $lnkFORMAT=$eLNK[1];
                switch($lnkFORMAT) {
                    case null :
                        $fEXT = ".".strtolower($lnkTyp);
                        break;
                    case 1 :
                        $fEXT = ".min.".strtolower($lnkTyp);
                        break;
                }

                $path=null;
                if(count($lnkDIRs)<=0) {
                    $lnkFILEs = Foundry_ManageFiles::listFilesByName(unserialize(constant($lnkTyp.'_REPO'))[$repoTyp]);
                    $lowlnkFILEs = array_map("strtolower", $lnkFILEs);
                    $fIndex = array_search(strtolower($lnkN).$fEXT, $lowlnkFILEs);
                    $path = unserialize(constant($lnkTyp.'_REPO'))[$repoTyp].DS.$lnkFILEs[$fIndex];
                }else {

                    foreach($lnkDIRs as $lnkDIR){
                        $matchFile = strtolower($lnkDIR.$lnkN.$fEXT);
                        //echo $lnkDIR;
                        //echo "#########<br>";
                        //echo strtolower($lnkTyp);
                        $aFiles = glob($lnkDIR.'*.'.strtolower($lnkTyp));

                        $caFiles = array_map("strtolower", $aFiles);
                        //echo "<pre>";
                        //print_r($caFiles);
                        $fIndex = array_search($matchFile, $caFiles);
                        if($fIndex===0 OR $fIndex>0) {
                            $path = $aFiles[$fIndex];
                            break;
                        }
                    }//END FOR EACH DIRECTORY in the REPO
                }//END ELSE FOR searching directories


                    if($path!==null) {
                        switch($lnkTyp) {
                            case "CSS":
                                echo "<style >".file_get_contents ($path)."</style>";
                                break;
                            case "JS":
                                    echo "<script>".file_get_contents ($path)."</script>";
                                break;
                        }//END SWITCH FOR LINK TYPE (JS,CSS....)
                    }//END IF FOR File Exists
                }//END FOREACH for each sub directory within the JS REPO (JS Main Directory)

            }//END  for AUTO LOAD LINK Files from REPO


    }//END Function psLNKs

} //END Class Loader