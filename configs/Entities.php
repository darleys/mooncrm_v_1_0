<?php
/**
 * Entities
 *
 * LICENSE
 *
Copyright 2015 Virtuous Consulting Services

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 * @copyright  Copyright (c) 2015 Virtuous Consulting Services.  (http://www.virtuouscs.com)
 * @Owner     Darley Stephen (http://www.darleys.org)
 * @Author     Darley Stephen (http://www.darleys.org)
 */


//connection string delimiter
define('PS_LNK_SPLIT','|#**#|');

//Auto Load CSS  0==manual 1== auto
define('AUTO_LOAD_CSS',1);
//CSS_AUTO_REPO , 0==local_base_repo (common folder), 1==local_special_repo (common folder/special_css) , 2==common_repo_type1...type2.....type_n
define('CSS_REPO',serialize(array(COMMON_DIR.'base_css',COMMON_DIR.'special_css','/var/hosting/bower/bower_components')));


//Auto Load JS  0==manual 1== auto
define('AUTO_LOAD_JS',1);
//JS_AUTO_REPO , 0==local_base_repo (common folder), 1==local_special_repo (common folder/special_css) , 2==common_repo_type1...type2.....type_n
define('JS_REPO',serialize(array(COMMON_DIR.'base_js',COMMON_DIR.'special_js','/var/hosting/bower/bower_components')));
