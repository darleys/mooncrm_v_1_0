<?php
require_once "sdk.class.php";

class S3
{
	private $s3;
	private $bucket;
	
	public function __construct($bucket, $accessKey, $secretKey)
	{
		$this->bucket = $bucket;
		$this->s3 = new AmazonS3(array("key" => $accessKey, "secret" => $secretKey));
		$this->s3->use_ssl = FALSE;
	}
	
	public function uploadFile($src, $dest)
	{
		$response = $this->s3->create_object(
			$this->bucket, $dest,
			array("fileUpload" => $src, "acl" => AmazonS3::ACL_PUBLIC));
		
		return $response;
	}
	
	public function fileExists($filename)
	{
		return $this->s3->if_object_exists($this->bucket, $filename);
	}
	
	public function getFileUrl($filename)
	{
		return $this->s3->get_object_url($this->bucket, $filename);
	}
	
	public function deleteFile($filename)
	{
		$response = $this->s3->delete_object($this->bucket, $filename);
		return $response;
	}
	
	public function get_file($filename)
	{
		$response = $this->s3->get_object($this->bucket, $filename);
		return $response;
	}
}
?>