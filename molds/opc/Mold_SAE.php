<?php
//Single Access Engine
class Mold_SAE extends Foundry_Core_Service {
	function RA() {
        $RAID= $_REQUEST['RAID'];
        if($RAID)
            echo base64_encode(rawurlencode(json_encode(Self::processUS($RAID))));
        else
            echo base64_encode(rawurlencode(json_encode("false")));
	}//END Function RA (Request Access)
    function GA() {
        $noAccess= "<script>window.location = '".SAE_LOGIN."'</script>";
        $accessData = self::processUS(self::invokeCS());
        if($accessData!=false) {
            if($accessData['status']=="logged") {
                return $accessData;
            }else {
                echo $noAccess;
                exit();
            }
        }else {
            echo $noAccess;
            exit();
        }
    }//END Function GA (Gain Access)
    function UserSeal($userData) {
        $sealID = md5($_SERVER['HTTP_USER_AGENT'].Foundry_useful::sealit("moon**crm").$_SERVER['REMOTE_ADDR']);
        apc_store($sealID,$userData,SAE_TIMEOUT);
    }//END Function to set SAE Data using the SEAL ID and Timeout
    function killSeal($sealID) {
        apc_delete($sealID);
    }//END Function to delete the SEAL data
    function invokeCS() {
        return md5($_SERVER['HTTP_USER_AGENT'].Foundry_useful::sealit("moon**crm").$_SERVER['REMOTE_ADDR']);
    }//END Function invokeCS which generates the secured ID according to the client information
    function processUS($sealID) {
        //apc_clear_cache() ;
        $UD = apc_fetch($sealID);
        return $UD;
    }//END Function processUS to retreive the SAE information according to the SEAL ID
}//END Class Mold_SAE
?>