<?php
//Miscellaneous Controller
class Mold_Mis extends Foundry_Core_Service {

    function getFieldLabel($field,$module) {
        $select_handle = self::$DB->query("select label FROM Fields WHERE module='$module' AND name='$field'");
        $records = self::$DB->fetchAll($select_handle);
        return $records[0]['label'];
    }

    function getSearchLayout($module){
        $select_handle = self::$DB->query("select * FROM ".$module."_Layouts WHERE layout='Search' AND sort_order > 0 ORDER BY sort_order ASC");
        $rows= self::$DB->fetchAll($select_handle);
        return $rows;
    }

    function getListsLayout($module){
        $select_handle = self::$DB->query("select * FROM ".$module."_Layouts WHERE layout='Lists' AND sort_order > 0 ORDER BY sort_order ASC");
        $rows= self::$DB->fetchAll($select_handle);
        return $rows;
    }

    function getViewLayout($module){
        $select_handle = self::$DB->query("select * FROM ".$module."_Layouts WHERE layout='View' AND sort_order > 0 ORDER BY sort_order ASC");
        $rows= self::$DB->fetchAll($select_handle);
        $i=0;
        $view = array();
        foreach($rows as $row)
        {
            $view[$row['tab']]['name'] = $row['tab_name'];
            $view[$row['tab']]['fields'][$i]['name'] = $row['name'];
            $view[$row['tab']]['fields'][$i]['type'] = $row['type'];
            $i++;

        }
        return $view;
    }

    function getEditLayout($module){
        $select_handle = self::$DB->query("select * FROM ".$module."_Layouts WHERE layout='Edit' AND sort_order > 0 ORDER BY sort_order ASC");
        $rows= self::$DB->fetchAll($select_handle);
        $i=0;
        $edit = array();
        foreach($rows as $row)
        {
            $edit['fields'][$i]['name'] = $row['name'];
            $edit['fields'][$i]['type'] = $row['type'];
            $i++;

        }
        return $edit;
    }

    function renderSearchString($post,$module)
    {
        $search_txt = array();
        foreach($post as $key=>$value)
        {
            if($value != '')
            {
                $select_handle = self::$DB->query("select * FROM Fields WHERE module='".$module."' AND name='".$key."'");
                $record = self::$DB->fetchAll($select_handle);

                switch($record[0]['type'])
                {
                    case 'text';
                    case 'textarea':
                        $search_txt[] =  $key." LIKE '".$value."'";
                        break;

                    case 'multiselect';
                    case 'dropdown':
                        foreach($value as $k => $v) {
                            $option_text[] = $key." LIKE '%".$v."%'";
                        }
                        $search_txt[] =  "(" .implode(' OR ',$option_text) .")";
                        break;

                    default:
                        $search_txt[] =  $key."='".$value."'";
                }
            }
        }

        if(!empty($search_txt))
        {
            $search_text  = implode(' AND ',$search_txt);
            return $search_text;
        }else return '';
    }


    function renderField($field,$value, $module,$action) {
        $select_handle = self::$DB->query("select * FROM Fields WHERE module='$module' AND name='$field'");
        $record = self::$DB->fetchAll($select_handle);

        switch($action){
            case 'Lists';
                switch($record[0]['type'])
                {
                    case 'bool':
                        if($value == 0)
                            $rendered_text = '<input type="checkbox"  value="0" disabled="true" >';
                        else
                            $rendered_text = '<input type="checkbox"  value="1" disabled="true" checked="true">';
                        return $rendered_text;
                        break;

                    case 'textarea':
                        return substr($value, 0, 60).' ...';

                    case 'dropdown';
                        include(COMMON_DIR . DS .'dropdown_options/'.$record[0]['extra1'].'.php');
                        return $options[$value];
                        break;

                    case 'multiselect';
                        include(COMMON_DIR . DS .'dropdown_options/'.$record[0]['extra1'].'.php');
                        $values =  explode(',',$value);
                        foreach($values  as $val)
                            $text[] = $options[$val];

                        return implode(', ',$text);
                        break;

                    case 'relation';
                        $select_handle = self::$DB->query("select name FROM {$record[0]['extra1']} WHERE id='$value' AND deleted=0");
                        if($row = self::$DB->fetchAll($select_handle))
                            return "<a href='{$record[0]['extra1']}/View/{$value}'>".$row[0]['name']."</a>";
                        else
                            return '';
                        break;

                    default:
                        return $value;

                }
                break;

            case 'Search';
                switch($record[0]['type'])
                {
                    case 'text';
                    case 'textarea';
                        $rendered_text = '<input type="text" name="'.$field.'" id="'.$field.'" value="">';
                        return $rendered_text;
                        break;

                    case 'bool';
                        $rendered_text = "<select  name='".$field."' id='".$field."'>
                                            <option value=''></option>
                                            <option value=1>Yes</option>
                                            <option value=0>No</option>
                                            </select>";
                        return $rendered_text;
                        break;

                    case 'dropdown';
                    case 'multiselect';
                        include(COMMON_DIR . DS .'dropdown_options/'.$record[0]['extra1'].'.php');
                        $rendered_text = "<select  multiple name='".$field."[]' id='".$field."'>";
                        foreach($options as $key => $val)
                            $rendered_text .= "<option value='".$key."'>".$val."</option>";
                        $rendered_text .= "</select>";
                        return $rendered_text;
                        break;
                }
                break;


            case 'View';
                switch($record[0]['type'])
                {
                    case 'bool':
                        if($value == 0)
                            $rendered_text = '<input type="checkbox"  value="0" disabled="true" >';
                        else
                            $rendered_text = '<input type="checkbox"  value="1" disabled="true" checked="true">';
                        return $rendered_text;
                        break;

                    case 'textarea':
                        return substr($value, 0, 60).' ...';

                    case 'dropdown';
                        include(COMMON_DIR . DS .'dropdown_options/'.$record[0]['extra1'].'.php');
                        return $options[$value];
                        break;

                    case 'multiselect';
                        include(COMMON_DIR . DS .'dropdown_options/'.$record[0]['extra1'].'.php');
                        $values =  explode(',',$value);
                        foreach($values  as $val)
                            $text[] = $options[$val];

                        return implode(', ',$text);
                        break;

                    case 'relation';
                        $select_handle = self::$DB->query("select name FROM {$record[0]['extra1']} WHERE id='$value' AND deleted=0");
                        if($row = self::$DB->fetchAll($select_handle))
                            return "<a href='{$record[0]['extra1']}/View/{$value}'>".$row[0]['name']."</a>";
                        else
                            return '';
                        break;

                    default:
                        return $value;

                }
                break;

            case 'Edit';
                switch($record[0]['type'])
                {
                    case 'text':
                        $rendered_text = '<input type="text" name="'.$field.'" id="'.$field.'" value="'.$value.'">';
                        return $rendered_text;
                        break;

                    case 'textarea':
                        $rendered_text = '<textarea name="'.$field.'" id="'.$field.'">'.$value.'</textarea>';
                        return $rendered_text;
                        break;

                    case 'bool';
                        if($value == 0)
                            $rendered_text = '<input type="checkbox"  value="0" name="'.$field.'" id="'.$field.'">';
                        else
                            $rendered_text = '<input type="hidden" value="0"  name="'.$field.'" />
                            <input type="checkbox"  value="1" checked="true"  name="'.$field.'" id="'.$field.'">';
                        return $rendered_text;
                        break;

                    case 'dropdown';
                        $rendered_text = "<select name='".$field."' id='".$field."'>";
                        $rendered_text .= "<option value='jkl'>jkljkl</option>";
                        $rendered_text .= "</select>";

                        return $rendered_text;
                        break;

                    case 'multiselect';
                        include(COMMON_DIR . DS .'dropdown_options/'.$record[0]['extra1'].'.php');
                        $selected_val =  explode(',',$value);
                        $rendered_text = "<select multiple name='".$field."[]' id='".$field."'>";
                        foreach($options as $key => $val)
                            if(in_array($key,$selected_val))
                                $rendered_text .= "<option value='".$key."' selected='selected'>".$val."</option>";
                            else
                                $rendered_text .= "<option value='".$key."'>".$val."</option>";
                        $rendered_text .= "</select>";

                        return $rendered_text;
                        break;

                    case 'relation':
                        if($value != '') {
                            $select_handle = self::$DB->query("select name FROM ".$record[0]['extra1']." WHERE id='" . $value . "' and deleted=0");
                            if($relate_record = self::$DB->fetchAll($select_handle))
                                $relate_name=$relate_record[0]['name'];
                        }else $relate_name = '';
                        $rendered_text = "<input type='text' id='".$field."_name' name='".$field."_name' class='autofill' module='".$record[0]['extra1']."' value='".$relate_name."'/>
                                            <input type='hidden' id='".$field."' name='".$field."' value='".$value."'/>";
                        return $rendered_text;
                        break;
                }
                break;

            default:
                return $value;
        }

    }

    function renderDropDownOptions($dropdown)
    {
        $select_handle = self::$DB->query("select value,label from Dropdown_Values, Dropdown_Options  WHERE Dropdown_Options.deleted=0 AND name='{$dropdown[0]}' AND parent_id=Dropdown_Options.id");
        $results = self::$DB->fetchAll($select_handle);
        foreach($results as $record)
            $options[$record['value']]=$record['label'];

        echo json_encode($results);
    }

}
