<?php
//Actions Controller
class Mold_AC extends Foundry_Core_Service {

    function iActions($forModule,$myid) {
        $iActions = securityGear::getUserSecurity();
        return $iActions;
    }//END Function iActions

    function iActionsIDS($iActions) {
        $iActionsIDS=array();
        foreach($iActions as $iAction) {
            $iActionsIDS[] = $iAction[id];
        }
        return $iActionsIDS;
    }//END Function iActionsIDS

    function validateAccess($module,$action, $level)
    {
        $user_data = array('id'=>Mold_SAE::GA()['id'],'department' => Mold_SAE::GA()['default_department'], 'role' => Mold_SAE::GA()['role'], 'team' => Mold_SAE::GA()['default_team']);
        $iActions = securityGear::getUserSecurity($user_data, 'RAW');

        if(empty($iActions[$module]) || empty($iActions[$module][$action]))
            return 'No Access';

        if($iActions[$module][$action] == $level)
            return TRUE;
        else
            return FALSE;
    }

    function validateRecordAccess($module,$action, $record)
    {
        if(self::validateAccess($module, $action, 5) || self::validateAccess($module, $action, 5) == 'No Access') //NO ACCESS
        {
            return FALSE;
        }elseif (self::validateAccess($module, $action, 1)) { //GLOBAL ACCESS
            return TRUE;
        } elseif (self::validateAccess($module, $action, 2)) //DEPARTMENT ACCESS
        {
            if(empty($record) || in_array($record['department_id'],Mold_SAE::GA()['departments']))
                return TRUE;
            else
                return FALSE;
        }elseif (self::validateAccess($module, $action, 3))
        {
            if( empty($record) || in_array($record['team_id'],Mold_SAE::GA()['teams']))
                return TRUE;
            else
                return FALSE;
        }elseif (self::validateAccess($module, $action, 4))
        {
            if( empty($record) || $record['created_by'] == Mold_SAE::GA()['id'])
                return TRUE;
            else
                return FALSE;
        }else{
            return TRUE;
        }

    }

    function hasAccess($module,$action)
    {
        if(Mold_SAE::GA()['type'] == 3)
            return FALSE;
        else
            return TRUE;
    }


}//END Class Mold_SAE
?>