<?php
class Mold_Department extends Foundry_Core_Service {
    private $iActions=0;
    private $GA=null;
    static function __callStatic($name, $arguments) {
        global $iActions,$GA;
        $GA=mold_sae::GA();
        $iActions = mold_actions::extActions("DEPARTMENTS");
        return call_user_func_array("self::".substr($name, 1), $arguments);
    }//END generic Function __callStatic

    function get_departments() {
        //apc_clear_cache() ;
        if(apc_exists(MOON_NAME."DEPT-STRUCT")) {
            return apc_fetch(MOON_NAME."DEPT-STRUCT");
        }else {
            /*
		    $select_handle = self::$DB->query("select departments.*,(select name from moon_departments where department_id=departments.parent) as parent_name ,GROUP_CONCAT(depttoroles.eactions_id) eactions,GROUP_CONCAT(eactions.iactions) iactions from moon_departments departments left join moon_department_roles depttoroles on depttoroles.department_id=departments.department_id
left join moon_eactions eactions ON  eactions.id =depttoroles.eactions_id group by departments.department_id");
            */
            $select_handle = self::$DB->query("SELECT d.id did,d.name dname, d.parent dparent,
    t.id tid,t.name tname,t.departments_id tdid,t.parent tparent FROM Departments d left join Teams t on t.departments_id=d.id  order by dparent,tparent asc");
            $departments = self::$DB->fetchAll($select_handle);

            $tempdid=0;
            $odepts=[];
            $sub_depts=[];
            $team=[];
            $skippedTeams=[];
            $skippedDepts=[];

            foreach($departments as $department) {
                $did =$department['did'];

                if($tempdid != $did) {
                    $tempdid=$did;
                    $dparent =  $department['dparent'];
                    $dept['self']=['uid'=>'department-'.$did,'did'=>$did,'name'=>$department['dname'],'parent'=>$department['dparent']];
                    if($dparent==0) {
                        $odepts['dept-'.$did] =$dept;
                        //echo "<br>"."HERE MAIn DEPT"."<br>";
                    }else {
                       // echo "<br>"."HERE CHILD DEPT"."<br>";
                        $re=0;
                        if(gear3::multiArrMerge('dept-'.$did,$dept,'dept-'.$dparent,$odepts,true,$re) === 0) {
                            //echo "HERE 0";
                            $skippedDepts['dept-'.$dparent]['dept-'.$did]=$dept;
                           // print_r($skippedDepts);
                           // echo "HERE1";
                        }
                        if(array_key_exists('dept-'.$did,$skippedDepts)) {
                            $skippedDept=$skippedDepts['dept-'.$did];
                            foreach($skippedDept as $skpDept) {
                               //echo "(((((((((((((("."DEPT";
                                //print_r($skpDept);


                                $re=0;
                                $skpdid=$skpDept['self']['did'];
                                //echo "THIS DID = ".'dept-'.$skpdid;
                               // echo "THIS PARENT DID = ".'dept-'.$did;
                                //echo "<br>";
                               // echo ")))))))))))))))))))))))";
                               //gear3::multiArrMerge('dept-'.$skpdid,$skpDept,'dept-'.$did,$odepts,true,$re);
                                if(gear3::multiArrMerge('dept-'.$skpdid,$skpDept,'dept-'.$did,$odepts,true,$re)=== 0) {
                                    gear3::multiArrMerge('dept-'.$skpdid,$skpDept,'dept-'.$did,$skippedDepts,true,$re);
                                }
                            }//ForEach Skipped Teams
                            unset($skippedDept);
                        }
                    }//END ELSE FOR ADDING CHILD DEPARTMENTS

                }//END IF FOR ADDING DEPARMENT
                $ptparent = 0;

                if($department['tid']!=null) {
                    //echo "<br>"."HERE START TEAM"."<br>";
                    $tid = $department['tid'];
                    $tparent = $department['tparent'];
                    //echo "################".$tparent;
                    $team['self']=['uid'=>'team-'.$tid,'tid'=>$tid,'name'=>$department['tname'],'default_department'=>$department['tdid'],'parent'=>$tparent];

                    if($tparent==0) {
                         $re=0;
                       // echo "??????????????????????????????????????".'dept-'.$did."??????????";
                        if(gear3::multiArrMerge('team-'.$tid,$team,'dept-'.$did,$odepts,true,$re) === 0) {
                            gear3::multiArrMerge('team-'.$tid,$team,'dept-'.$did,$skippedDepts,true,$re);
                        }

                    }else {
                        $re=0;
                        if(gear3::multiArrMerge('team-'.$tid,$team,'team-'.$tparent, $odepts,true,$re) === 0) {
                            if(gear3::multiArrMerge('team-'.$tid,$team,'team-'.$tparent, $skippedDepts,true,$re) === 0) {
                                $skippedTeams['team-'.$tparent]['team-'.$tid]=$team;
                            }
                        }
                        if(array_key_exists('team-'.$tid,$skippedTeams)) {
                            $skippedTeam=$skippedTeams['team-'.$tid];
                            foreach($skippedTeam as $skpTeam) {
                            //echo "(((((((((((((("."TEAM";
                            //print_r($skpTeam);
                            //echo ")))))))))))))))))))))))";

                                $re=0;
                                $skptid=$skpTeam['self']['tid'];
                                if(gear3::multiArrMerge('team-'.$skptid,$skpTeam,'team-'.$tid, $odepts,true,$re)===0) {
                                    gear3::multiArrMerge('team-'.$skptid,$skpTeam,'team-'.$tid, $skippedDepts,true,$re);
                                }
                            }//ForEach Skipped Teams
                            unset($skippedTeam);
                        }

                    }
                }//Store Teams Inside a Department


            }//END FOREACH DEPARTMENT result
           // echo "=================================";
            //print_r($odepts);
            //echo "=================================";
            unset($departments);
            apc_store(MOON_NAME."DEPT-STRUCT",$odepts);
            unset($odepts);
            return apc_fetch(MOON_NAME."DEPT-STRUCT");
        }//END Else for departments not stored in cache
    }//END Function get_departments

    function departments_list() {
        global $iActions,$GA;

        $departments=self::get_departments();
        $departments_list=Array();

        $doMe=false;
        //Check whether the User has access to Listing Departments (iActions ID == 47)
        if(mold_ac::validateAccess(47,$iActions)) {
            $doMe=true;

            $userDepartments = mold_ac::validateUTODEPT($GA['id'],0,$iActions,$GA['department_id'],$GA['default_department']);

            $ONLY_MY_TEAM=false;
            //Check whether the User has access to View only their Team (iActions ID == 52)
            if(mold_ac::validateAccess(52,$iActions)) {

                foreach($userDepartments as $userDepartment) {
                    foreach($departments as $department) {
                        if($department['department_id']==$userDepartment['department_id'] && $department['type']==1 && $GA['department_id']==$department['department_id']) {
                            $departments_list[]=$department;
                            break;
                        }
                    }
                }
                $ONLY_MY_TEAM=true;
            }//END IF to check if the User has permission to view only their team

            $ONLY_MY_DEPT=false;
            //Check whether the User has access to View only their Department (and its child departments and teams) (iActions ID == 53)
            if(mold_ac::validateAccess(53,$iActions) && !$ONLY_MY_TEAM) {
                foreach($userDepartments as $userDepartment) {
                    foreach($departments as $department) {
                        if($department['department_id']==$userDepartment['department_id']) {
                            $departments_list[]=$department;
                            break;
                        }
                    }
                }
                $ONLY_MY_DEPT=true;
            }//END IF to check if the User has permission to view only their Departments

                /*
                 * BLOCK Actions IDS for Quote Search and VIEW
                 * 54 == departments_restrict_list_to_team
                 * 55 == departments_restrict_list_to_dept
                 * blockID denotes the iActionsID and the department type == 1 for Team 0 for Department
                 */

            if(!$ONLY_MY_TEAM) {
                $blockIDS="54**1,55**0";
                if(!$ONLY_MY_DEPT)
                    $departments_list=$departments;

                $blockDEPTS = mold_ac::validateDEPTTOU($GA['id'],$GA['department_id'],$GA['default_department'],$blockIDS);

                if(count($blockDEPTS)>0) {
                    foreach($blockDEPTS as $blockDEPT) {
                        foreach($departments_list as $dlKey => $department) {
                            if($department['department_id']==$blockDEPT['department_id']) {
                                unset($departments_list[$dlKey]);
                            }//END IF there is a match for BLOCK
                        }//FOR EACH Department List ot match BLOCK IDS
                    }//FOR EACH BLOCK IDS
                }//END IF there are block IDS
            }//END IF to check for Departments or Team based block (54 AND 55)
        }//END IF for access to listing departments
        if($doMe) {
            return $departments_list;
        }//END IF to check whether user has basic access to List Departments
    }//END Function List Departments
	
	function department_detail($department_id) {
		$select_handle = self::$DB->query("select * from moon_departments where department_id = ".$department_id."");
		if($department_detail = mysql_fetch_assoc($select_handle)) {
			Foundry_Memory::set('department_detail', $department_detail);
		}
	}//END Function department_detail

    function department_roles_list() {
        $departments = $this->get_departments();
        $geActions = mold_actions::geActions();
        print_r($geActions);
    }//END Function department roles
	
	function save_department($department_id, $name, $description) {
		$fields = array('name', 'description');
		$values = array($name, $description);
echo "****".$name."^^^^".$department_id."####".$description;
		if(trim($department_id) == 'false')  {
            echo $name;
			self::$DB->insert('moon_departments',$fields,$values);
		}	else	{
			$where = "department_id = ".$department_id;
			self::$DB->update('moon_departments',$fields,$values, $where);
		}

		//echo "CALLBACKS".UNIQUE_SEPERATOR."window.location='/departments'";
	}//END Function save_department
	
	function delete_department($department_id) {
		//delete user group
		self::$DB->query("delete from moon_departments where department_id = ".$department_id."");
		
		//remove row from record list
		echo "CALLBACKS".UNIQUE_SEPERATOR."$('#department_".$department_id."').remove()";
	}//END Function delete_department
	
	function search_department($keyword) {
		$keyword = mysql_real_escape_string($keyword);
		
		$select_handle = self::$DB->query("select department_id as id, name as label from moon_departments where name LIKE '".$keyword."%'");
		$departmentArr = self::$DB->fetchAll($select_handle);
		
		echo $RESP = json_encode($departmentArr);
	}//END Function search_department

    function checkBelongWS() {
        $myid=$_REQUEST['MYID'];
        $departments=$_REQUEST['departments'];
        print_r($departments);
        /*
        $keyword = mysql_real_escape_string($keyword);

        $select_handle = self::$DB->query("select department_id as id, name as label from moon_departments where name LIKE '".$keyword."%'");
        $departmentArr = self::$DB->fetchAll($select_handle);

        echo $RESP = json_encode($departmentArr);
        */
    }//END Function search_department
    /*
     * Retreive all the Child Departments or Teams for a given department, 2 params,
     * 1. Department ID aand 2. Toggle to check whether to include this department for which the child  is requested as a part of the output array
     *
     */
    function getChilds($departmentID,$incMe) {
        $departments=mold_department::get_departments();
        $cDEPTS=Array();
        $indepts=Array($departmentID);
        $fPrnt=false;
        $cdCallBack = function($value,$key) use($departments,&$fPrnt,&$cDEPTS,&$indepts,&$incMe,&$cdCallBack) {
            $did=$value['department_id'];
            $parent=$value['parent'];
            if(in_array($did,$indepts) && !$fPrnt && $incMe) {
                $cDEPTS[]=$value;
                $fPrnt=true;
            }
            if(!in_array($did,$indepts) && in_array($parent,$indepts)) {
                $indepts[]=$did;
                $cDEPTS[]=$value;
                array_walk($departments, $cdCallBack);
            }
        };//END CallBack Function cdCallBack (Child Departments (Teams) callback)
        array_walk($departments, $cdCallBack);

        return $cDEPTS;
    }//END Function getChilds Departments or TEAM


    /*
     *
     *
     */
    function checkDeptAccess($myid,$ldept,$ddept,$blockIDS) {
        $departments=mold_department::get_departments();
        $userDepartments=mold_user::get_user_departments($myid,0,$ldept,$ddept);
        $blockDEPTS=Array();
        $blockDIDS=Array();
        $indepts=array($ldept);

        //print_r($userDepartments);
        //ini_set('display_errors',1);
        //error_reporting(E_ALL);
        $dacCallBack = function($value,$key) use($departments,&$userDepartments,&$ldept,&$blockDEPTS,&$blockDIDS,&$dacCallBack,&$blockIDS) {
            $did=$value['department_id'];
            $cDepts=self::getChilds($did,true);
            $parent=$value['parent'];
            $iActions = $value['iactions'];
            /*
             * CHECK restrictions based on the BLOCK IDS requested .
             * each block ID  denotes the iActionsID and the department type == 1 for Team 0 for Department
             * for example 31**1,32**0 == first is quote_restrict_view_to_team so 31**1 restricts team and we can ignore departments and its child
             *  32**0 quote_restrict_view_to_department .. represent department so need to retreive all sub departments IDS and team IDS and match that with userDepartments to
             * check whether the USER have access to those departments , sub departments or sub teams.
             */
            $bIDS = explode(",",$blockIDS);
            foreach($bIDS as $bID) {
                $eachBID = explode("**",$bID);
                $blockID = $eachBID[0];
                $blockDTYPE = $eachBID[1];
                //  retreive all iActions for that department  or team from $departmens array and check whether BLOCKID requested is inside the iActions
                if(strpos($iActions,$blockID)!== false) {
                    if($blockDTYPE==0) {
                        foreach($cDepts as $cDept) {
                            $isaDeptMem=false;
                            foreach($userDepartments as $userDepartment) {
                                if($userDepartment['department_id']==$cDept['department_id']) {
                                    $isaDeptMem=true;
                                }
                            }//END Iteration userDeparments access for each Child Departments that has restrictions
                            if(!$isaDeptMem  && !in_array($cDept['department_id'],$blockDIDS)) {
                                $blockDEPTS[]=$cDept;
                                $blockDIDS[]=$cDept['department_id'];
                            }//Add the Department that has block and for which the user dont have access
                        }//END for to check whether user has access to that DEPARTMENT (DEPARTMENT that has restriction)
                    }//CHECK whether the BLOCK ID requested is for a DEPARTMENT
                    if($blockDTYPE==1) {
                        $isaTeamMem=false;
                        /*
                        foreach($userDepartments as $userDepartment) {
                            if($userDepartment['department_id']==$did)
                                $isaTeamMem=true;
                        }//END for to check whether user has access to that TEAM (TEAM that has restriction)
                        */
                        if($ldept==$did)
                            $isaTeamMem=true;
                        if(!$isaTeamMem && !in_array($did,$blockDIDS)) {
                            $blockDEPTS[]=$value;
                            $blockDIDS[]=$did;
                        }//END IF for user dont have access to that team

                    }//CHECK whether the BLOCK ID requested is for a TEAM


                }//END IF for a valid match (blockID in iActionsID)
            }//END to check whether the requested BLOCK ID is a part of this department's (team's) iActionIDS

        };//END callback Function dacCallBack

        array_walk($departments, $dacCallBack);
        /*
        echo "<#####@@@@@@@@@@@:<br>";
        print_r($blockDEPTS);
        echo "@!!!!!!!!!!!!!!!!!!!!<br>";
        */
        return $blockDEPTS;

    }//END Function checkDeptAccess

    function checkDeptAccessWS() {
        $myid=$_REQUEST['MYID'];
        $ldept=$_REQUEST['LDEPT'];
        $ddept=$_REQUEST['DDEPT'];
        $blockIDS=$_REQUEST['BLOCKIDS'];

        $blockDEPTS = self::checkDeptAccess($myid,$ldept,$ddept,$blockIDS);

        echo base64_encode(rawurlencode(json_encode($blockDEPTS)));
    }//END Function check Department Access WS
}//END Function Mold_department
?>