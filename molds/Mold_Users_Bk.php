<?php
/**
 * Created by PhpStorm.
 * User: 10046078
 * Date: 3/26/2015
 * Time: 3:37 PM
 */

class Mold_Users extends Foundry_Core_Service
{
    function Lists($request)
    {
        $module = ucfirst($request[0]);
        $action = ucfirst($request[1]);


        $data['module'] = $module;
        $data['action'] = $action;


        $data['search_txt']  = Mold_Mis::renderSearchString($_POST,$module);

        if (Mold_AC::validateAccess($module, $action, 1)) { //Global Access
            $data['where'] = '';
        } elseif (Mold_AC::validateAccess($module, $action, 2)) { // Department Level Access
            $data['where'] = " AND department_id IN (" . implode(",",Mold_SAE::GA()['departments']) . ")";
        } elseif (Mold_AC::validateAccess($module, $action, 3)) {  // Team Level Access
            $data['where'] = " AND team_id IN (" . implode(",",Mold_SAE::GA()['teams']) . ")";
        } elseif (Mold_AC::validateAccess($module, $action, 4)) {  // User Level Access
            $data['where'] = ' AND created_by=' . Mold_SAE::GA()['id'];
        }

        $where = '';

        if ($data['where'] != '') {
            $where = $data['where'];
        }

        if ($data['search_txt'] != "") {
            $search_txt = mysql_real_escape_string(str_replace(array('[', ']'), array('', ''), $data['search_txt']));
            $where .= ' AND ('. $data['search_txt'] .')';
        }

        //get base record for the module
        $select_handle = self::$DB->query("select * from " . $module . " WHERE deleted=0 " . $where);
        $records = self::$DB->fetchAll($select_handle);

        //GET Layout FOR Module
        $data['search'] = Mold_Mis::getSearchLayout($module);
        $data['lists'] = Mold_Mis::getListsLayout($module);
        $data['total_num'] = count($records);

        //get current page num
        $page_num = ($_GET['page_num']) ? $_GET['page_num'] : 1;
        $data['page_num'] = $page_num;

        //create pagination links
        $page_id = gear3::createPaging($records);

        $data['total_pages'] = gear3::totalPages($page_id, PAGE_SIZE);
        $data['records'] = gear3::sendPaging($page_id, PAGE_SIZE, $page_num);

        return $data;

    }
}