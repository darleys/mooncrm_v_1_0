<?php
class Mold_Actions extends Foundry_Core_Service {

	function extActions($forModule)  {
        $thisUser=mold_sae::GA();
        $dweller_id=$thisUser['id'];
        return self::retActions($forModule,$dweller_id);
    }//END Function extActions

    function retActions($forModule,$dweller_id) {
        $iActions=-1;
        $select_handle = self::$DB->query("select iactions.name,iactions.id from moon_user_roles roles,moon_eactions eactions,moon_modules modules,moon_iactions iactions where
user_id='".$dweller_id."'  and eactions.id=roles.eactions_id and modules.name='$forModule' and eactions.module_id=modules.module_id and
find_in_set(iactions.id,eactions.iactions) <> 0");
        $returnRows = mysql_num_rows($select_handle);
        if(!$returnRows>0) {
            $select_handle = self::$DB->query("select iactions.name,iactions.id from moon_user_to_department utodept, moon_department_roles droles,moon_eactions eactions,moon_modules modules,moon_iactions iactions where
	utodept.user_id='".$dweller_id."' and droles.department_id=utodept.department_id and eactions.id =droles.eactions_id and modules.name='$forModule' and eactions.module_id=modules.module_id and
	find_in_set(iactions.id,eactions.iactions) <> 0");
        }
        $iActions = Foundry_DB::fetchAll($select_handle);
        //$iActions=array_map('trim', $iActions);

        return $iActions;
    }

    function extActionsWS() {
        echo base64_encode(rawurlencode(json_encode(self::retActions($_REQUEST['MODULE_NAME'],$_REQUEST['MYID']))));
    }//END Function extActionsWS

    function geActions() {
        if(apc_exists(MOON_NAME."eActions")) {
            return apc_fetch(MOON_NAME."eActions");
        }else {
            $select_handle = self::$DB->query("select * from moon_eactions");
            $eActions = self::$DB->fetchAll($select_handle);
            apc_store(MOON_NAME."eActions",$eActions);
            return $eActions;
        }//END Else for eActions not stored in cache
    }//END to get and store all Global eActions

    function giActions() {
        if(apc_exists(MOON_NAME."iActions")) {
            return apc_fetch(MOON_NAME."iActions");
        }else {
            $select_handle = self::$DB->query("select * from moon_iActions");
            $iActions = self::$DB->fetchAll($select_handle);
            apc_store(MOON_NAME."iActions",$iActions);
            return $iActions;
        }//END Else for iActions not stored in cache
    }//END to get and store all Global iActions
}//END CLASS Mold_Actions
?>