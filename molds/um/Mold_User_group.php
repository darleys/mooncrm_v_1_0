<?php
class Mold_User_group extends Foundry_Core_Service {
	
	function get_user_groups() {
		$select_handle = self::$DB->query("select * from moon_groups");
		$user_groups = self::$DB->fetchAll($select_handle);
		Foundry_Memory::set('user_groups', $user_groups);
	}//END Function get_user_groups
	
	
	function search_user_groups($keyword) {
		$where = ( $keyword != "false" ) ? "where title like '%".mysql_real_escape_string($keyword)."%'" : "";
		$select_handle = self::$DB->query("select * from moon_groups ".$where);
		$user_groups = self::$DB->fetchAll($select_handle);
		
		$RESP = '';
		foreach( $user_groups as $resource )  {
			$RESP .= '<tr id="user_group_'.$resource['group_id'].'">';
			$RESP .= '<td>'.$resource['title'].'</td>';
			$RESP .= '<td>'.$resource['description'].'</td>';
			$RESP .= '<td>';
			$RESP .= '<a href="/user_group_form?id='.$resource['group_id'].'">Edit</a>';
			if ($resource['group_id'] > 2)
				$RESP .= ' | <a href="javascript:;" class="delete_user_group" rel="'.$resource['group_id'].'">Delete</a>';
			$RESP .= '</td>';
			$RESP .= '</tr>';
		}
		$RESP = mysql_real_escape_string($RESP);
		
		echo "CALLBACKS".UNIQUE_SEPERATOR."$('#user_group_list tbody').html('".$RESP."');";
		
	}//END Function search_user_groups
	
	
	function user_group_detail($group_id) {
		$select_handle = self::$DB->query("select * from moon_groups where group_id = ".$group_id."");
		if($group_detail = mysql_fetch_assoc($select_handle)) {
			Foundry_Memory::set('group_detail', $group_detail);
		}
		
		$select_handle = self::$DB->query("select * from moon_group_module_relation gmr JOIN moon_modules m ON m.module_id=gmr.module_id where group_id = ".$group_id."");
		$user_group_modules = self::$DB->fetchAll($select_handle);
		Foundry_Memory::set('user_group_modules', $user_group_modules);
	}//END Function user_group_detail
	
	
	function save_user_group($group_id, $title, $description, $module_ids, $module_permissions) {
		
		//save record in moon_groups table
			$fields = array('title', 'description');
			$values = array($title, $description);
			
			if( $group_id == "false" )  {
				self::$DB->insert('moon_groups', $fields, $values);
				$group_id = mysql_insert_id();
			}	else	{
				$where = "group_id = ".$group_id;
				self::$DB->update('moon_groups', $fields, $values, $where);
			}
		
		//save group modules in moon_group_module_relation
			//delete old records
			$where = "group_id = '".$group_id."'";
			self::$DB->delete('moon_group_module_relation', $where);
			
			//insert new modules in database
			$moduleIds = explode(",", $module_ids);
			$modulePermissions = explode(",", $module_permissions);
			
			foreach( $moduleIds as $i => $module_id )  {
				if( $module_id != "" )  {
					$fields1 = array('group_id', 'module_id', 'permission');
					$values1 = array($group_id, $module_id, $modulePermissions[$i]);
					self::$DB->insert('moon_group_module_relation', $fields1, $values1);
				}
			}
		
		echo "CALLBACKS".UNIQUE_SEPERATOR."window.location='/user_groups'";
	}//END Function save_user_group
	
	
	function delete_user_group($group_id) {
		//delete user group
		$where = "group_id = '".$group_id."'";
		self::$DB->delete("moon_groups", $where);
		
		//delete from moon_group_module_relation and moon_user_group_relation
		$where = "group_id = '".$group_id."'";
		self::$DB->delete("moon_group_module_relation", $where);
		self::$DB->delete("moon_user_group_relation", $where);
		
		//remove row from record list
		echo "CALLBACKS".UNIQUE_SEPERATOR."$('#user_group_".$group_id."').remove()";
	}//END Function delete_user_group
	
}//END Function Mold_User_group
?>