<?php
class Mold_User extends Foundry_Core_Service {
	function logIn($username, $password, $remember_me) {
//ini_set('display_errors',1);
//error_reporting(E_ALL);
		$username = mysql_real_escape_string($username);

		$select_handle = self::$DB->query("select * from Users where username='".$username."' AND mysecret='".md5($password)."'");

		$user = mysql_fetch_assoc($select_handle);
        $userDepts=self::get_user_departments(trim($user['default_department']), trim($user['default_team']), 1);
        $user['departments']=$userDepts['departments'];
        $user['teams']=$userDepts['teams'];
/*
        //Get Associated Departments
        $departments[0][] = $user['default_department'];
        $i=0;
        $flag=true;
        while($flag)
        {
            foreach($departments[$i] as $parent) {

                $select_handle = self::$DB->query("SELECT id from Departments where parent =".$parent);
                $records = self::$DB->fetchAll($select_handle);
                foreach ($records as $id) {
                    $departments[$i + 1][] = $id['id'];
                }
            }
            $i++;
            if(empty($departments[$i]))
                $flag = false;
        }

        foreach($departments as $ids) {
            foreach($ids as $id)
                $department_lists[] = $id;
        }
        $user['departments'] =$department_lists;

        //Get Associated Teams
        $teams[0][] = $user['default_team'];
        $i=0;
        $flag=true;
        while($flag)
        {
            foreach($teams[$i] as $parent) {

                $select_handle = self::$DB->query("SELECT id from Teams where parent =".$parent);
                $records = self::$DB->fetchAll($select_handle);
                foreach ($records as $id) {
                    $teams[$i + 1][] = $id['id'];
                }
            }
            $i++;
            if(empty($teams[$i]))
                $flag = false;
        }

        foreach($teams as $ids) {
            foreach($ids as $id)
                $team_lists[] = $id;
        }
        $user['teams'] =$team_lists;
        */


		if($user) {
			//set session values

            $user['status']="logged";
			Foundry_Memory::set('dweller', $user);
			Foundry_Memory::set('login_dweller_id', $user['id']);

            mold_sae::UserSeal($user);

			//set cookie if user have selected remember me
			if( $remember_me[0] == "YES" )  {
				setcookie('login_cookie', base64_encode($user['id']), time() + (3600 * 24));
			}	else	{
				setcookie('login_cookie', '', time() - 60);
			}
			echo "CALLBACKS".UNIQUE_SEPERATOR."window.location='dashboard';";
		}else {
			echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('Wrong Login Credentials')";
		}
	}//END Function logIn


    function getDepartments($id)
    {
        static $rows = array ();
        $select_handle = self::$DB->query("select id FROM departments WHERE parent_id=".$id);
        while ( $row = mysql_fetch_assoc($select_handle) ) {

            array_push ( $rows, $row ['id'] );

            self::getDepartments( $row ['id'] );
        }
        return $rows;
    }

    function getTeams($id)
    {
        static $rows = array ();
        $select_handle = self::$DB->query("select id FROM teams WHERE parent_id=".$id);
        while ( $row = mysql_fetch_assoc($select_handle) ) {

            array_push ( $rows, $row ['id'] );

            self::getTeams( $row ['id'] );
        }
        return $rows;
    }

	
	function forget_password($username)  {
		
		$username = mysql_real_escape_string($username);
		$select_handle = self::$DB->query("select * from users where username='".$username."'");
		$user = mysql_fetch_assoc($select_handle);
		
		if($user) {
			//add random key in database
			$reset_key = gear3::rand_string();
			self::$DB->update('moon_dwellers',array('reset_key'), array($reset_key), "id = ".$user['id']);
			
			//send mail to user
			$email = $user['myemail'];
			$reset_link = PROJECT_URL . "/reset_password?key=".$reset_key;
			$subject = "Forgot password request";
			
			$message = "Hello ".$user['myfname'].",<br/><br/>";
			$message .= "We have received your forgot password request. Below is the link to reset your password. <br/><br/>";
			$message .= "<a href='".$reset_link."'>Click here</a> to reset your password.<br/><br/>";
			$message .= "If you have not done this request than please just ignore this mail.<br/><br/>";
			$message .= "Thanks,<br/>".SENDER_NAME;
			gear3::send_mail($email, $subject, $message);
			
			echo "CALLBACKS".UNIQUE_SEPERATOR."$('#response_msg').html('Your reset password link has been sent to your email ".$email.". <br/>Please check your spam folder in case message not found in your inbox.')";
		}else {
			echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('This username does not exists in our system.')";
		}
	}//END Function forget_password
	
	
	function check_reset_key($reset_key)  
	{
		if( $reset_key != "" )
		{
			$reset_key = mysql_real_escape_string($reset_key);
			$select_handle = self::$DB->query("select * from moon_dwellers where reset_key='".$reset_key."'");
			$user = mysql_fetch_assoc($select_handle);
			
			if($user)  {
				return "";
			}
		}
		
		return "This is invalid link or it has been already used to reset password. <br/>Please login with your password.";
	}//END Function check_reset_key
	
	
	function reset_password($new_password, $reset_key)
	{
		$error = Mold_user::check_reset_key($reset_key);
		
		if( $error ) {
			echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('".$error."')";
		}	else	{
			//add new password and remove the key
			$fields = array('mysecret', 'reset_key');
			$values = array(md5($new_password), '');
			self::$DB->update('moon_dwellers', $fields, $values, "reset_key = '".$reset_key."'");
			
			echo "CALLBACKS".UNIQUE_SEPERATOR."$('#response_msg').html('Your password has been reset successfully. Please login with your new password.')";
		}
	}//END Function reset_password

    function checkLoginSession()  {
        //check if user is logined
        $UD=securityGear::processUS(securityGear::invokeCS());
        if( $UD['status']!="logged")  {
            echo '<script>';
            echo "window.location = '/login'";
            echo '</script>';
            exit();
        }
    }//END Function checkLoginSession

    function authUS() {
        $sealID= $_REQUEST['sealID'];
        //echo $sealID;
        //print_r(securityGear::processUS($sealID));
        //if(is_array(securityGear::processUS($sealID)))

        //print_r($sealUD);

        if($sealID)
            echo base64_encode(rawurlencode(json_encode(securityGear::processUS($sealID))));
        else
            echo base64_encode(rawurlencode(json_encode("false")));

    }

	function checkLoginSession1()  {
		//check if user is logined
		$gotoLogin = true;
		
		if( Foundry_Memory::isThere('dwellers') )
			$gotoLogin = false;
		
		if( isset($_COOKIE['login_cookie']) && $gotoLogin == true )
		{
			//get dweller information and set in session
			$id = base64_decode($_COOKIE['login_cookie']);
			$select_handle = self::$DB->query("select * from moon_dwellers where id='".$id."'");
			$user = mysql_fetch_assoc($select_handle);
			
			if( $user )  {
				//if cookie exists and valid user than set session values
				$gotoLogin = false;
				Foundry_Memory::set('dwellers', $user);
				Foundry_Memory::set('login_dweller_id', $user['id']);
			}
		}
		
		if( $gotoLogin )  {
			//because header is not working. I have used javascript here
			echo '<script>';
			echo "window.location = '/login'";
			echo '</script>';
			exit();
		}
	}//END Function checkLoginSession
	
	
	function logOut() {
		//session_destroy();
        securityGear::killSeal(securityGear::invokeCS());
        Foundry_Memory::dealloc('dwellers');
		Foundry_Memory::dealloc('login_dweller_id');
		
		//destroy cookie
		setcookie('login_cookie', '', time() - 60);
		
        echo "CALLBACKS".UNIQUE_SEPERATOR."window.location='/login'";
    }//END Function logOut
	
	
	function get_users() {
		$select_handle = self::$DB->query("select * from moon_dwellers");
		
		$users = self::$DB->fetchAll($select_handle);
		Foundry_Memory::set('users', $users);
	}//END Function get_users
	
	
	function search_users($keyword) {
		$keyword = mysql_real_escape_string($keyword);
		$where = ( $keyword != "false" ) ? "WHERE (myfname like '%".$keyword."%' or mylname like '%".$keyword."%' or myemail like '%".$keyword."%')" : "";
		
		$select_handle = self::$DB->query("select * from moon_dwellers ".$where);
		$users = self::$DB->fetchAll($select_handle);
		$total_users = count($users);
		
		$RESP = '';
		foreach( $users as $resource )  {
			$RESP .= '<tr id="user_'.$resource['id'].'">';
			$RESP .= '<td><input type="checkbox" class="checkbox_cls" value="'.$resource['id'].'" /></td>';
			$RESP .= '<td>'.$resource['myfname'].'</td>';
			$RESP .= '<td>'.$resource['mylname'].'</td>';
			$RESP .= '<td>'.$resource['myemail'].'</td>';
			$RESP .= '<td>';
			$RESP .= '<a href="/user_form?id='.$resource['id'].'">Edit</a> | ';
			$RESP .= '<a href="javascript:;" class="delete_user" rel="'.$resource['id'].'">Delete</a> | ';
			$RESP .= '<a href="javascript:;" class="export-user" rel="'.$resource['id'].'">Export</a>';
			$RESP .= '</td>';
			$RESP .= '</tr>';
		}
		$RESP = mysql_real_escape_string($RESP);
		
		echo "CALLBACKS".UNIQUE_SEPERATOR."$('#total_users').val(".$total_users.")".UNIQUE_SEPERATOR;
		echo "CALLBACKS".UNIQUE_SEPERATOR."$('#user_list tbody').html('".$RESP."');";
		
	}//END Function search_user_groups
	
	
	function search_user($keyword) {
		$keyword = mysql_real_escape_string($keyword);
		
		$select_handle = self::$DB->query("select id as id, CONCAT(myfname, ' ', mylname) as label from moon_dwellers where (myfname like '%".$keyword."%' or mylname like '%".$keyword."%')");
		$userArr = self::$DB->fetchAll($select_handle);
		
		echo $RESP = json_encode($userArr);
	}//END Function search_user for ajax auto complete
	
	
	function user_detail($id) {
		$sql = "select d.*, CONCAT(d1.myfname, ' ', d1.mylname) as report_to_name from moon_dwellers d left join moon_dwellers d1 on d.reports_to = d1.id where d.id = ".$id;
		$select_handle = self::$DB->query($sql);
		
		if($user_detail = mysql_fetch_assoc($select_handle)) {
			Foundry_Memory::set('id', $id);
			Foundry_Memory::set('user_detail', $user_detail);
		}
	}//END Function user_detail
	
	
	function save_user($id, $myfname, $mylname, $myemail, $myusername, $mypassword, $myphone, $myaddress1, $myaddress2, $mycity, $mystate, $myzip, $mycountry, $reports_to) {
		$fields = array('myfname', 'mylname', 'myemail', 'myusername', 'myphone', 'myaddress1', 'myaddress2', 'mycity', 'mystate', 'myzip', 'mycountry', 'reports_to');
		$values = array($myfname, $mylname, $myemail, $myusername, $myphone, $myaddress1, $myaddress2, $mycity, $mystate, $myzip, $mycountry, $reports_to);
		
		//if value not came than set it to blank
		foreach( $values as $i=>$val )  {
			if( $val == "false" )
				$values[$i] = "";
		}
		
		//if password is entered than update password
		if( $mypassword != "false" )  {
			$fields[] = "mysecret";
			$values[] = md5($mypassword);
		}
		
		if( $id == "false" )  {
			self::$DB->insert('moon_dwellers',$fields,$values);
			$id = mysql_insert_id();
		}	else	{
			$where = "id = ".$id."";
			self::$DB->update('moon_dwellers',$fields,$values, $where);
		}
		
		echo "CALLBACKS".UNIQUE_SEPERATOR."window.location='/users'";
	}//END Function save_user
	
	
	function delete_user($id) {
		//delete user group
		self::$DB->query("delete from moon_dwellers where id = ".$id);
		
		//remove row from record list
		echo "CALLBACKS".UNIQUE_SEPERATOR."$('#user_".$id."').remove()";
	}//END Function delete_user
	
	
	function get_eactions() {
		$select_handle = self::$DB->query("select * from moon_eactions");
		$actions = self::$DB->fetchAll($select_handle);
		Foundry_Memory::set('eactions', $actions);
	}//END get_actions
	
	function get_user_roles($user_id) {
		//get already added eactions
		$select_handle = self::$DB->query("select * from moon_user_roles where user_id = ".$user_id);
		$eactions = self::$DB->fetchAll($select_handle);
		
		foreach( $eactions as $eaction )  {
			echo "CALLBACKS".UNIQUE_SEPERATOR."$('#eactions').find('option[value=".$eaction['eactions_id']."]').attr('selected', 'selected');".UNIQUE_SEPERATOR;
		}
	}//END Function get_user_roles
	
	
	function save_user_roles($user_id, $eactions) {
		//delete old records
		$where = "user_id = '".$user_id."'";
		self::$DB->delete('moon_user_roles', $where);
		
		//save user roles eactions
		if( $eactions != "false" )  {
			$eactions = explode(",", $eactions);
			foreach( $eactions as $eactions_id )  {
				$fields = array('user_id', 'eactions_id');
				$values = array($user_id, $eactions_id);
				self::$DB->insert('moon_user_roles', $fields, $values);
			}
		}
		
		echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('User roles saved successfully.')";
	}//END Function save_user_roles
	
	
	function get_user_departmentsBACK($user_id) {
		//get actions
		$select_handle = self::$DB->query("select * from moon_departments");
		$departments = self::$DB->fetchAll($select_handle);
		
		//get user departments
		$select_handle = self::$DB->query("select * from moon_user_to_department where user_id = '".$user_id."'");
		$user_departments = self::$DB->fetchAll($select_handle);
		
		foreach( $user_departments as $resource )  {
			$RESP .= '<tr class="user_department">';
			$RESP .= '<td>';
			
			$RESP .= '<select class="department_id">';
			$RESP .= '<option value="">Select Department</option>';
            foreach( $departments as $resource1 )  {
				$selected = ( $resource1['department_id'] == $resource['department_id'] ) ? 'selected' : '';
				$RESP .= '<option value="'.$resource1['department_id'].'" '.$selected.'>'.$resource1['name'].'</option>';
			}
            $RESP .= '</select>';
			
			$RESP .= '</td>';
			$RESP .= '<td><a href="javascript:;" class="remove_department">Remove</a></td>';
			$RESP .= '</tr>';
		}
		$RESP = mysql_real_escape_string($RESP);
		
		echo "CALLBACKS".UNIQUE_SEPERATOR."$('.user_department').remove();".UNIQUE_SEPERATOR;
		echo "CALLBACKS".UNIQUE_SEPERATOR."$('#user_departments').prepend('".$RESP."');";
	}//END Function get_user_departments

    /*
     * ddept - default department
     * dteam - default team
    * rtype -return type 0=>ASSOC TYPE 1 , 1=>straight list of user linked to departments and teams.
    */
    function get_user_departments($ddept, $dteam, $rtype) {
        $departments=mold_department::get_departments();

        $userDepts=Array();
       // print_r($departments);

        /*
         *$foundDD is a boolean recurrent identifier which will execute the recurrent function $udCallBack first for all the departments list
         *  and once a match is found for default department,  $foundDD becomes true and then $udCallBack will iterate only through the subdepartments
         * and teams within the default department.
         */
        $foundDD=false;
        $addMe=false;
        $udCallBack = function($value,$key) use($departments,&$ddept,&$dteam,&$rtype,&$userDepts,&$foundDD,&$addMe,&$udCallBack) {
            if(is_array($value)) {
                if(!$foundDD) {
                    if(strpos($value['self']['uid'],'department-')!==FALSE && strpos($value['self']['uid'],'-'.trim($ddept)) !== FALSE) {
                        $foundDD=true;
                        $addMe=true;
                        //array_walk($value,$udCallBack);
                    }else {
                        array_walk($value,$udCallBack);
                    }
                }//Default Department not matched
                else {
                    if(strpos($value['self']['uid'],'department-')!==FALSE) {
                        $addMe=true;
                    } elseif(strpos($value['self']['uid'],'team-')!==FALSE) {
                        $addMe=true;
                    }else {

                    }
                }
                if($addMe) {
                    switch($rtype) {
                        case 0:
                            if(strpos($value['self']['uid'],'department-')!==FALSE) {

                                $userDepts['departments'][]=$value['self'];
                                array_walk($value,$udCallBack);
                            } elseif(strpos($value['self']['uid'],'team-')!==FALSE) {
                                $userDepts['teams'][]=$value['self'];
                                array_walk($value,$udCallBack);
                            }
                            break;
                        case 1:
                            if(strpos($value['self']['uid'],'department-')!==FALSE) {
                                $userDepts['departments'][]=$value['self']['did'];
                                array_walk($value,$udCallBack);
                            } elseif(strpos($value['self']['uid'],'team-')!==FALSE) {
                                $userDepts['teams'][]=$value['self']['tid'];
                                array_walk($value,$udCallBack);
                            }
                            break;
                    }//END SWITCH FOR RETURN TYPE
                    $addMe=false;
                }//Add the Department and Teams depending on the ressult type
            }//END IF to make sure that the department/team struct is an array
        };
        array_walk($departments,$udCallBack);

        //echo "+++++++++++++++++";
        //print_r($userDepts);
        //echo "+++++++++++++++++";
        return $userDepts;
    }//END Function get_user_departments
    /*
    * UDTYPE - is the type to denote whether to retrieve all CHILD DEPARTMENT(and TEAMS) (or)  retrieve all CHILD + parent DEPARTMENT(TEAMS) of the user linked department.
    * DTYPE - is to return all TEAMS or DEPARTMENTS , 1==team and 0 == departments&teams
    */
    function get_user_departmentsWS() {
        $ddept=$_REQUEST['DDEPT'];
        $dteam=$_REQUEST['DTEAM'];
        $rtype=$_REQUEST['RTYPE'];
        //echo $this->get_user_departments($ddept,$dteam,$rtype);
        echo base64_encode(rawurlencode(json_encode(  $this->get_user_departments($ddept,$dteam,$rtype) )));
    }//END Function get_user_departments



	
	
	function save_departments($user_id, $department_ids) {
		$departmentIDs = explode(",", $department_ids);
		$departmentIDs = array_unique($departmentIDs);
		
		//delete old records
		$where = "user_id = '".$user_id."'";
		self::$DB->delete('moon_user_to_department', $where);
		
		//save departments
		foreach( $departmentIDs as $i => $department_id )  {
			$fields1 = array('user_id', 'department_id');
			$values1 = array($user_id, $department_id);
			self::$DB->insert('moon_user_to_department', $fields1, $values1);
		}
		
		echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('User departments saved successfully.')";
	}//END Function save_departments
	
	function import_action($user)
	{
		$action = (string)$user->action;
		if( $action !== "" )  
		{
			//search user by email
			$myemail = mysql_real_escape_string($user->myemail);
			$select_handle = self::$DB->query("select * from moon_dwellers where myemail = '".$myemail."'");
			$userExists = self::$DB->fetch($select_handle);
			
			//if add/edit user than check that parent user exists
			if( $user->action == 1 || $user->action == 2 )  
			{
				$reportsToEmail = $user->reports_to;
				if( $reportsToEmail != "" && $reportsToEmail != "0" )
				{
					//search for parent user
					$select_handle = self::$DB->query("select * from moon_dwellers where myemail = '".mysql_real_escape_string($reportsToEmail)."'");
					$parentUser = self::$DB->fetch($select_handle);
					
					if( $parentUser )  {
						$reports_to = $parentUser['id'];
					}	else	{
						$user->response = 0;
						$user-> resp_msg = 'Reports to user doest not match.';
						return $user;
					}
				}	else	{
					$reports_to = 0;
				}
			}
			
			//fields to insert or update
			$fields = array('myusername', 'myemail', 'myfname', 'mylname', 'myphone', 'myaddress1', 'myaddress2', 'mycity', 'mystate', 'myzip', 'mycountry', 'reports_to');
			$values = array($user->myusername, $user->myemail, $user->myfname, $user->mylname, $user->myphone, $user->myaddress1, $user->myaddress2, $user->mycity, $user->mystate, $user->myzip, $user->mycountry, $reports_to);
			
			//create new user
			if( $user->action == 1 )  {
				if( $userExists )  {
					$user->response = 0;
					$user-> resp_msg = 'Email already exists';
				}	else	{
					//insert new record in database
					self::$DB->insert('moon_dwellers',$fields,$values, $where);
					
					$user->response = 1;
					$user-> resp_msg = 'null';
				}
			}
			
			//update user details
			if( $user->action == 2 )  {
				if( $userExists )  {
					//update in moon_dwellers table
					$where = "id = ".$userExists['id'];
					self::$DB->update('moon_dwellers',$fields,$values, $where);
					
					$user->response = 1;
					$user-> resp_msg = 'null';
				}	else	{
					$user->response = 0;
					$user-> resp_msg = 'Email doest not match';
				}
			}
			
			//delete user
			if( $user->action === 0 || $user->action === "0" )  {
				if( $userExists )  {
					//delete from database
					$where = "id = ".$userExists['id'];
					self::$DB->delete('moon_dwellers', $where);
					
					$user->response = 1;
					$user-> resp_msg = 'null';
				}	else	{
					$user->response = 0;
					$user-> resp_msg = 'Email doest not match';
				}
			}
		}	else	{
			$user->response = 0;
			$user->resp_msg = 'Data not processed because action not provided.';
		}
		return $user;
	}//END FUNCTION import_action
	
	function export_action($fileType, $id)  {
		//fetch dweller
		$sql = "SELECT d.myusername,d.myemail,d.myfname,d.mylname,d.myphone,d.myaddress1,d.myaddress2,d.mycity,d.mystate,d.myzip,d.mycountry,d1.myemail as reports_to ";
		$sql .= "FROM moon_dwellers d LEFT JOIN moon_dwellers d1 ON d.reports_to=d1.id where d.id = ".$id;
		$select_handle = self::$DB->query($sql);
		$users = self::$DB->fetchAll($select_handle);
		
		if( $fileType == "XML" )  {
			$data['users'] = $users;
			gear1::exportXML("user.xml", $data, 'UTF-8');
		}
		else if( $fileType == "CSV" )
		{
			$header = array("Username", "Email", "First Name", "Last Name", "Phone", "Address 1", "Address 2", "City", "State", "Zip", "Country", "Reports To");
			gear1::exportCSV("user.csv", $header, $users);
		}
	}//END Function export_action
	
	function export_all($fileType, $user_ids, $keyword, $zip_no)  {
		
		//make where condition for filtering data
		$whereStr = '';
		if( $user_ids != "" )  {
			$whereStr = 'Where d.id IN('.$user_ids.')';
		}	else if($keyword != "")	{
			$keyword = mysql_real_escape_string($keyword);
			$whereStr = "WHERE (d.myfname like '%".$keyword."%' or d.mylname like '%".$keyword."%' or d.myemail like '%".$keyword."%')";
		}
		
		//get users that need to be exported
		$sql = "SELECT d.id,d.myusername,d.myemail,d.myfname,d.mylname,d.myphone,d.myaddress1,d.myaddress2,d.mycity,d.mystate,d.myzip,d.mycountry,d1.myemail as reports_to FROM moon_dwellers d ";
		$sql .= "LEFT JOIN moon_dwellers d1 ON d.reports_to=d1.id ".$whereStr." order by d.id desc";
		
		$handle = self::$DB->query($sql);
		$all_users = self::$DB->fetchAll($handle);
		
		//get users for creating zip file
		$start_pos = ZIP_BUNCH_SIZE * ($zip_no - 1);
		$users = array_slice($all_users, $start_pos, ZIP_BUNCH_SIZE);
		
		//generate user files
		$files = array();
		foreach( $users as $user )
		{
			$id = $user['id'];
			unset($user['id']);
			
			if( $fileType == "XML" )
			{
				//generate xml from array
				$xmlStr = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Module><users>';
				$xmlStr = gear1::addXMLStringFromArray1($xmlStr, $user);
				$xmlStr .= '</users></Module>';
				
				$fileName = COMMON_DIR."tmp_dir".DS."user_".$id.".xml";
				gear1::generateXML($fileName, $xmlStr);
			}
			else if( $fileType == "CSV" )
			{
				$header = array("Username", "Email", "First Name", "Last Name", "Phone", "Address 1", "Address 2", "City", "State", "Zip", "Country", "Reports To");
				
				$fileName = COMMON_DIR."tmp_dir".DS."user_".$id.".csv";
				gear1::generateCSV($fileName, $header, array($user));
			}
			array_push($files, $fileName);
		}
		
		//create new zip file
		$zip_name = COMMON_DIR."tmp_dir".DS."users_".$zip_no.".zip";
		gear1::createZip($zip_name, $files, true);
		
		//download zip file
		gear1::downloadZip($zip_name, true);
	}//END Function export_all
}
?>