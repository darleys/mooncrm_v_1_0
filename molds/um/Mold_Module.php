<?php
class Mold_Module extends Foundry_Core_Service {
	
	function get_modules() {
		$select_handle = self::$DB->query("select * from moon_modules");
		$modules = self::$DB->fetchAll($select_handle);
		Foundry_Memory::set('modules', $modules);
	}//END Function get_modules
	
	function module_detail($module_id) {
		$select_handle = self::$DB->query("select * from moon_modules where module_id = ".$module_id."");
		if($module_detail = mysql_fetch_assoc($select_handle)) {
			Foundry_Memory::set('module_detail', $module_detail);
		}
	}//END Function module_detail
	
	function save_module($module_id, $name, $description) {
		$fields = array('name', 'description');
		$values = array($name, $description);
		
		if( $module_id == "false" )  {
			self::$DB->insert('moon_modules',$fields,$values);
		}	else	{
			$where = "module_id = ".$module_id;
			self::$DB->update('moon_modules',$fields,$values, $where);
		}
		
		echo "CALLBACKS".UNIQUE_SEPERATOR."window.location='/modules'";
	}//END Function save_module
	
	function delete_module($module_id) {
		//delete user group
		self::$DB->query("delete from moon_modules where module_id = ".$module_id."");
		
		//remove row from record list
		echo "CALLBACKS".UNIQUE_SEPERATOR."$('#module_".$module_id."').remove()";
	}//END Function delete_module
	
	function search_module($keyword) {
		$keyword = mysql_real_escape_string($keyword);
		
		$select_handle = self::$DB->query("select module_id as id, name as title as label from moon_modules where name LIKE '".$keyword."%'");
		$moduleArr = self::$DB->fetchAll($select_handle);
		
		echo $RESP = json_encode($moduleArr);
	}//END Function search_module
	
}//END Function Mold_module
?>