<?php
/**
 * Foundry
 *
 * LICENSE
 *
Copyright 2013 Virtuous Consulting Services

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 * @copyright  Copyright (c) 2013 Virtuous Consulting Services.  (http://www.virtuouscs.com)
 * @Owner     Darley Stephen (http://www.darleys.org)
 * @Author     Darley Stephen (http://www.darleys.org)
 */
require_once dirname(dirname(dirname(__FILE__))).DS.'external/swift/lib/swift_required.php';
class MoldMe extends Foundry_Core_Service {
    function getME($mymyid) {
        $select_handle =self::$DB->query("select * from mymy where mymyid='".$mymyid."'");
        return self::$DB->fetch($select_handle);
    }

    function checkavail($username,$OBOOL=false) {
        $select_handle =self::$DB->query("select * from mymy where myname='".$username."'");
        if($select_row = mysql_fetch_assoc($select_handle) || strlen($username) < 4 ||  empty($username) || $username == "false") {
            if(!$OBOOL)
                $RESP ="signin_username".UNIQUE_SEPERATOR."".UNIQUE_SEPERATOR."R".UNIQUE_SEPERATOR;
            $RESP .="CALLBACKS".UNIQUE_SEPERATOR;
            $RESP .="flashAauto('"."The username is not Available or not Valid"."')".UNIQUE_SEPERATOR;

            echo $RESP;
            return false;
        }else {
            if(!$OBOOL)
                echo "signin_username_err".UNIQUE_SEPERATOR."ITS available".UNIQUE_SEPERATOR."R".UNIQUE_SEPERATOR;
            return true;
        }
    }



    function signup($username,$password,$cpassword,$email,$location, $dobyyyy, $dobmm,$dobdd, $gender) {
        //echo $email;
        $domymy=true;

        $RESP="";

        $flashout="";
        $flashinc=1;
        if(!Foundry_Validation::validEmail($email)) { $domymy=false;$flashout .=$flashinc.". Please enter a valid email"."<br>";$flashinc++;	}
        if(!gear1::checkSecret($password))  { $domymy=false;$flashout .=$flashinc.". Please enter a valid password "."<br>";$flashinc++;	}
        if(!gear1::checkSecret($cpassword) || $password != $cpassword)  { $domymy=false;$flashout .=$flashinc.". specified passwords do not match"."<br>";$flashinc++;	}

        if(strlen($gender) > 1)  { $domymy=false;$flashout .=$flashinc.". Please enter a valid Gender"."<br>";$flashinc++;	}
        if(strlen($dobyyyy) > 4) { $domymy=false;$flashout .=$flashinc.". Please enter a valid Year"."<br>";$flashinc++;	}
        if(strlen($dobmm) > 2) { $domymy=false;$flashout .=$flashinc.". Please enter a valid Month"."<br>";$flashinc++;	}
        if(strlen($dobdd) > 2) { $domymy=false;$flashout .=$flashinc.". Please enter a valid Date"."<br>";$flashinc++;	}
        if(strlen($username) < 4 || !mymy::checkavail($username,true)) { $domymy=false;$flashout .=$flashinc.". The username is not Available or not Valid"."<br>";$flashinc++;	}



        $RESP .="CALLBACKS".UNIQUE_SEPERATOR;
        $RESP .="flashA('".$flashout."')".UNIQUE_SEPERATOR;
        if($domymy) {
            $select_handle =self::$DB->query("INSERT INTO mymy VALUES('".mymy::genMYMYID()."','".$username."','".md5($password)."','".$email."','".$location."','".date('Y-m-d',mktime(0, 0, 0, $dobmm, $dobdd, $dobyyyy))."','".$gender."')");
            self::logIn($username,$password);
            echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('Account Created')";
        }
        else
            echo $RESP;
    }

    function logIn_bk($username,$password) {

        $select_handle = self::$DB->query("select * from mymy where username='".$username."' AND mysecret='".md5($password)."'");
        if($select_row = mysql_fetch_assoc($select_handle)) {
            echo "CALLBACKS".UNIQUE_SEPERATOR."location.reload();";
        }else {
            echo "CALLBACKS".UNIQUE_SEPERATOR."flashAuto('Wrong Login Credentials')";
        }
    }//END Function logIn

    function forgot($email) {

        $select_handle = self::$DB->query("select * from mymy where myemail='".$email."'");

        if($select_row = mysql_fetch_assoc($select_handle)) {
            $new_secret=gear1::generatePassword();
            self::$DB->query("UPDATE mymy set mysecret='".md5($new_secret)."' where myname='".$select_row['myname']."' AND myemail='".$select_row['myemail']."'");
            //Foundry_Memory::set('mymy',$select_row);
            $transport = Swift_SmtpTransport::newInstance('smtp.zoho.com', 465,'ssl')
                ->setUsername('no-reply@irackit.com')
                ->setPassword('iam**strong');


            $mailer = Swift_Mailer::newInstance($transport);


            $message = Swift_Message::newInstance('Password Recovery')
                ->setFrom(array('no-reply@irackit.com' => 'IrackIt')) // can be $_POST['email'] etc...
                ->setTo(array($select_row['myemail'] => $select_row['myname'])) // your email / multiple supported.
                ->setBody("Hi ".$select_row['myname'].",<br>
&nbsp;&nbsp;&nbsp;Your username&nbsp;:  ".$select_row['myname']."<br>
&nbsp;&nbsp;&nbsp;Your password&nbsp;:  ".$new_secret."<br>
<br>
Note : Type the Password rather copying and pasting.<br>
<br>
                Regards,<br>
                IRackIt team<br>
http://irackit.com<br>
<br>
<p><i>This email is intended solely for the use of the intended recipient(s) and shall not attach any liability on the originator or IRackIt.If you are the addressee, the contents of this e-mail are intended for your use only and it shall not be forwarded to any third party, without first obtaining written authorization from the originator or IRackIt.It may contain information which is confidential and legally privileged and the same shall not be used or dealt with, by any third party in any manner whatsoever without the specific consent of IRackIt . If you are not the intended recipient or the person responsible for delivering the email to the intended recipient, please be advised that you have received this email in error and that any use, dissemination, forwarding, printing or copying of this email is strictly prohibited. If you have received this email in error, please notify the sender immediately by return e-mail and delete it from your system. Whilst IRackIt has made all efforts to exclude viruses, IRackIt expressly excludes all liability, to the fullest extent permissible by law,for viruses that may be transmitted via this e-mail and/or its attachments. IRackIt does not accept liability for any errors or omissions in the context of this message, which may arise as a result of Internet transmission. The recipient should check this email and any attachments for the presence of viruses. Any opinions contained in this message are those of the author and are not given or endorsed by IRackIt unless otherwise clearly stated to be so and the authority of the author to do so bind IRackIt has been duly verified.</i> </p>
",'text/html');



            if ($mailer->send($message)) {
                echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('Password Sent')";
            } else {
                echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('Something went wrong, Please Try again')";
            }

        }else {
            echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('Wrong Email Credentials')";
        }




    }//END Function forgot

    function changePass($username,$cpassword,$npassword,$ncpassword) {

        $select_handle = self::$DB->query("select * from mymy where myname='".$username."' AND mysecret='".md5($cpassword)."'");
        if($select_row = mysql_fetch_assoc($select_handle)) {
            //Foundry_Memory::set('mymy',$select_row);
            if($npassword === $ncpassword) {
                $update_handle = self::$DB->query("update mymy set mysecret='".md5($npassword)."' where myname='".$username."'");

                echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('Password Changed')";
            }else {
                echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('New Passwords doesn't match')";
            }
        }else {
            echo "CALLBACKS".UNIQUE_SEPERATOR."flashAauto('Please enter correct Current Password')";
        }
    }//END Function Change Password

    function logOut() {
        Foundry_Memory::dealloc('mymy');
        echo "CALLBACKS".UNIQUE_SEPERATOR."location.reload();";
    }//END Function logOut



    function genMYMYID() {
        /*
        $waste_str = round(microtime(true));
        $mypin='902010';
        $junk="";
        for($i=0;$i<strlen($mypin);$i++) {
            $part=substr($mypin,$i,1);
            $index=rand(1,strlen($waste_str));
            $first = substr($waste_str,0,$index);
            $second = substr($waste_str,$index);
            $waste_str=$first.$part.$second;
            $junk=$waste_str;
        }
        $mymyid = substr($junk,0,strlen($junk)-rand(1,10));
        */
        return uniqid();
    }



} //END Class MoldMe