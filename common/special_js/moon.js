function initMoon() {
    initOPCM();
    initMenu();
    initMisc();
    //initSearch();
    //initManageShow();
    //misc();
}
function initOPCM() {
    $(".moon-opcm-link").on("click",function(e) {
        //alert($(this).attr('name'));
        var url = $(this).attr('name');
        $.ajax({
            crossDomain:true,
            type: 'POST',
            url: url,
            dataType: "html"
        }).done(function( html ) {
            $( ".body-content" ).html( html );
        });
    });
}//END Function initOPC Manage
function initMenu() {
    $(".moon-menu-link").on("click",function(e) {
        //alert($(this).attr('name'));
        var url = $(this).attr('name');
        $.ajax({
            crossDomain:true,
            type: 'POST',
            url: url,
            dataType: "html"
        }).done(function( html ) {
            $( ".body-content" ).html( html );
        });
    });
}
var overlayCOM=0;
var endOverlay=0;
function initMisc(){
    /*
     $('.irackit-1-button').live('mousedown',function(e) {
     $(this).removeClass('irackit-1-button');
     $(this).addClass('irackit-1-buttonClk');
     });
     $('.irackit-1-buttonClk').live("mouseup",function(e) {
     $(this).removeClass('irackit-1-buttonClk');
     $(this).addClass('irackit-1-button');
     });

     $('.irackit-2-button').live('mousedown',function(e) {
     $(this).removeClass('irackit-2-button');
     $(this).addClass('irackit-2-buttonClk');
     });
     $('.irackit-2-buttonClk').live("mouseup",function(e) {
     $(this).removeClass('irackit-2-buttonClk');
     $(this).addClass('irackit-2-button');
     });
     */

}//END Function - initialize Media Library Structure Management

function initSearch() {
    $('.searchMedia ').click(function(e){
        if($('.search').val()!=''){
            $('#searchFC').submit();
        }
    });

    //handle search paging clicks
    $('.spage-num').on('click',function(e) {
        window.location= "/search?isalt="+$(this).attr('content')+"&skeys="+$('#skeys').html();
    });
}//END Function initSearch


$('.mytools-l').on('click',function(e) {
    $('.mytools').toggle();
});

function initManageShow() {
    $('.add-comment').on('click',function(e) {
        if($('.addComment').val().length > 10 && $('.addComment').val().length < 300){
            PROCESS_AJAX_REQ('request::processor',"::"+$('#resource-id').html()+",addComment",'managemedia:addComment','');
        }else {
            flashAauto('Please enter a valid comment')
        }
    });

    //handle comments
    $('.cpage-num').on('click',function(e) {
        $.ajax({
            type:"POST",
            url: "/fetchcomments@managemedia",
            data: { isalt: $(this).attr('content') }
        }).done(function(theResponse) {
            $('.comments').html(theResponse);
        });

    });

}//END Function initManageShow
/*
 *  START MANAGE MISC in SHOW
 */
function warnPrivate() {
    trans(0.8);
    $('.overlay').fadeToggle();
    $('.warn-private').fadeToggle();
    overlayCOM=$('.warn-private');
    $('#private-2-submit').click(function(e) {
        if($('.warn-private').attr('content') == 2) {
            $.ajax({
                type:"POST",
                url: "/checkprivate@managedelivery",
                data: { resourceid: $('#resource-id').html().trim(), privateType:base64_encode(2),privateData:base64_encode($('#private-password').val().trim()) }
            }).done(function(theResponse) {
                toDO(theResponse);
            });
        }
    });
    $('#private-3-submit').click(function(e) {
        if($('.warn-private').attr('content') == 3) {
            $.ajax({
                type:"POST",
                url: "/checkprivate@managedelivery",
                data: { resourceid: $('#resource-id').html().trim(), privateType:base64_encode(3),privateData:base64_encode($('#private-answer').val().trim()) }
            }).done(function(theResponse) {
                toDO(theResponse);
            });
        }
    });
}
/*
 *  END MANAGE MISC in SHOW
 */



function initManageUploads() {
    fileprog = new Array();
    _level="root";
    _level_status=false;
    var uploadFunc;
    var toolBox_target;

    $('#bupload').uploadify({
        'formData' : { 'mymyid' : $('#mymyid').html().trim() ,'myname' : $('#myname').html().trim()},
        'onDialogOpen()': function() {
            fileprog = new Array();
            _level_status=false;
        },
        'onDialogClose' : function(queueData) {

        },
        'buttonText':'Upload',
        'fileObjName' : 'myresource',
        'fileSizeLimit' : '3GB',
        'swf':THE_KEYS["MEDIA00_LINK"]+'/common/spmedia/bupload/uploadify.swf',
        'uploader' : THE_KEYS["MEDIA00_LINK"]+"/singleUpload@molds_manageuploads",
        //'uploader':'/bupload@manageuploads',
        dataType: "json",
        'successTimeout' : 5000,
        'onSelect' : onSelect,
        'onUploadProgress' : onUploadProgress,
        'onUploadSuccess' :onUploadSuccess,
        'onQueueComplete':function() {$('#bupload').uploadify('disable', false);}
    });
    $('.uploadify-queue').css('display','none');
    /*

     $('#supload').uploadify({
     'multi':false,
     'onDialogOpen': function() {
     fileprog = new Array();
     uploadFunc="single";
     toolBox_target=$('.toolBox-target').parent();
     },
     'onDialogClose' : function(queueData) {
     if(queueData.filesQueued>0)     {
     $('.shownotify').css('display','block');
     $('#supload').uploadify('disable', true);
     }
     },
     'buttonText':'',
     'fileObjName' : 'ml-supload-res',
     'fileSizeLimit' : '100MB',
     'swf':'/common/spmedia/bupload/uploadify.swf',
     'uploader' : "/savve_ml.supload::pillars",
     //'uploader':'/bupload@manageuploads',
     dataType: "json",
     'successTimeout' : 5000,
     'onUploadProgress' : onUploadProgress,
     'onUploadSuccess' :onUploadSuccess,
     'onQueueComplete':function() {$('#supload').uploadify('disable', false);}

     });

     */

    function onSelect(file){
        filename = file.name.trim();
        findex = fileprog.indexOf(filename);
        if(findex < 0){
            var nindex = fileprog.push(filename);
            var fid = $('.prog-each-temp').clone();
            fid.attr('id',"prog"+(nindex-1));
            fid.children('.prog-file').html(filename);
            fid.removeClass('prog-each-temp');
            fid.addClass('prog-each');
            fid.appendTo($('.main-content'));
            $('.main-content').append("<br>");
            fid.show();
        }
    }


    function onUploadProgress(file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal) {
        filename = file.name.trim();
        findex = fileprog.indexOf(filename);
        percentUP = (bytesUploaded/bytesTotal)*100;
        if(percentUP>90) {
            $("#prog"+findex).children('.prog-tools').children('.prog-cancel').remove();
            percentUP = 100;
            showStoring="";
            if($("#prog"+findex).children('.prog-file').html().indexOf("(Storing Media)") < 0)
                showStoring = $("#prog"+findex).children('.prog-file').html()+"  (Storing Media)";
            else
                showStoring = $("#prog"+findex).children('.prog-file').html();
            console.log(showStoring);
            $("#prog"+findex).children('.prog-file').html(showStoring);
        }
        $("#prog"+findex).children('.prog-back').children('.prog-status').css("width",(percentUP)+"%");
        //  $('.notify').css('display','none');

        //alert(file.name + " " + findex + " "+((bytesUploaded/bytesTotal)*100));
    }//END Function onUploadProgress



    function onUploadSuccess(file, theResponse, response) {
        console.log("******************");
        console.log(file.name);
        console.log(theResponse);

        fname = file.name.trim();
        var rindex = fileprog.indexOf(fname);
        //UPmsg = "The Media is Uploaded , will be published within 2 hours after review <br><br>"+THE_KEYS["MY_LINK"]+theResponse.trim()
        $("#prog"+rindex).children('.prog-file').html(theResponse);
        $("#prog"+rindex).children('.prog-file').css("margin-top","3%");
        $("#prog"+rindex).children('.prog-back').remove();
        $("#prog"+rindex).children('.prog-tools').remove();
        $("#prog"+rindex).children('.prog-misc').remove();
        setTimeout(function(){
            $("#prog"+rindex).hide('slow', function(){ $("#prog"+rindex).remove();});
        },30000);



    }//End Function onUploadSuccess

}//END Function initManageUploads

function initManageRack() {
    privateType=0;

    function showRack(rackId) {
        for(inc=1;inc<=4;inc++) {
            $('.racktype-'+inc).hide();
        }
        $('.racktype-'+rackId).show();
    }
    $('.rack-l').click(function(e) {
        showRack($(this).attr('content'));

    });
    clearActiveL = function(resourceID) {
        $(".activate-rack").each(function() {
            if($(this).attr('content')==resourceID) {
                $(this).remove();
            }
        });
    }



    ceditObj=0;

    $('.edit-rack').click(function(e) {
        trans(0.5);
        //$('body').append($());
        $('.overlay').fadeToggle();
        $('.rdata').fadeToggle();
        overlayCOM=$('.rdata');
        $('#rdata-title').val(base64_decode($(this).parent().children('.ordata-title').attr('content')));
        $('#rdata-desc').val(base64_decode($(this).parent().children('.ordata-desc').attr('content')));
        $('#rdata-tags').val(base64_decode($(this).parent().children('.ordata-tags').attr('content')));
        $("#rdata-rid").html($(this).attr('content'));
        ceditObj=$(this);
    });

    $('#rdata-save').on('click',function(e) {
        $.ajax({
            type:"POST",
            url: "save_rdata",
            data: {resourceid: $('#rdata-rid').html().trim(), rtitle:base64_encode($('#rdata-title').val().trim()),
                rdesc:base64_encode($('#rdata-desc').val().trim()), rtags:base64_encode($('#rdata-tags').val().trim()),
                privateType:privateType,privateData:base64_encode(compilePrivateData())}
        }).done(function(theResponse) {
            if(theResponse==1) {
                ceditObj.parent().children('.ordata-title').attr('content',base64_encode($('#rdata-title').val().trim()));
                ceditObj.parent().children('.ordata-desc').attr('content',base64_encode($('#rdata-desc').val().trim()));
                ceditObj.parent().children('.ordata-tags').attr('content',base64_encode($('#rdata-tags').val().trim()));
            }
            $('.overlay').fadeToggle();
            $('.rdata').fadeToggle();
            //location.reload();
        });
    });

    function compilePrivateData() {
        privateData="";
        if($('.private-type-s').val() ==2) {
            privateData = $('#private-type-2').children('div').children('.private-data-s').val();
        }
        if($('.private-type-s').val() ==3) {
            //return( $('#private-type-2').children('div').children('#private-data-1').val());

            $('#private-type-3').children('div').each(function(e) {
                privateData+=$(this).children('.private-data-s').val()+',';
            });
        }
        return privateData;
    }//END Function compilePrivateData

    $('.rdata-settings-l').on('click',function(e) {
        $('.rdata-setting').hide();
        $('#'+$(this).attr('id')+"config").show();
    });

    $('.activate-rack').on('click',function(e) {
        title = base64_decode($(this).parent().children('.ordata-title').attr('content').trim());
        desc = base64_decode($(this).parent().children('.ordata-desc').attr('content').trim());
        tags = base64_decode($(this).parent().children('.ordata-tags').attr('content').trim());
        if(title.length >=4 && desc.length >=10 && tags.length >=5 ) {
            $.ajax({
                type:"POST",
                url: "activatenow",
                data: {resourceid: $(this).attr('content').trim()}
            }).done(function(theResponse) {
                toDO(theResponse);
            });
        }//END IF TO CHECK VALID Title, Desc and Tags
        else {
            alert("Please enter valid Title,Description & Tags for this Media");
        }
    });



    /*
     * Deal with Response Settings Start
     */

    $('#rdata-private').on('click',function(e) {
        if($(this).attr('checked')) {
            $('.private-type').css('display','inline');
            privateType = $('.private-type-s').val();
            $('.private-data').hide();
            $('#private-type-'+privateType).show();
        }else {
            $('.private-type').css('display','none');
            $('.private-data').hide();
            privateType=0;
        }
    });

    $('.private-type-s').on('change',function(e) {
        privateType = $('.private-type-s').val();
        $('.private-data').hide();
        $('#private-type-'+privateType).show();

    });

    /*
     * Deal with Response Settings End
     */

}//END Function initManageRack


function misc() {
    $('.advertise-l').click(function(e) {
        trans(0.8);
        $('.overlay').fadeToggle();
        $('.advertise').fadeToggle();
        overlayCOM=$('.advertise');
    });

    $('.contactus-l').click(function(e) {
        trans(0.8);
        $('.overlay').fadeToggle();
        $('.contactus').fadeToggle();
        overlayCOM=$('.contactus');
    });
}