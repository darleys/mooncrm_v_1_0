$(document).ready(function(e) {

	//show tabs in add/edit form
	$( "#tabs" ).tabs();
	
	//open next tab and make last green
	$(".next-tab-btn").click(function() {
		$(".ui-tabs-active").next().find('a').css('color', 'green').trigger('click');
	});
	
	//delete lead
	$(".delete_lead").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var id = $(this).attr('rel');
			$("#id").val(id);
			
			//delete user group from database
			PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id','Mold_Lead:_delete_lead','');
		}
	});
	
	//submit search form
	$("#search-leads").click(function() {
		submitFormToCRMCAjax("leads", "#search_form" );
	});
	
	//pagination link requests
	$(".leads_pagination a").click(function() {
		submitFormToCRMCAjax($(this).attr('href'), "#search_form" );
		return false;
	});
	
	//clear search
	$("#reset-search").click(function() {
		$("#search_txt").val('');
		submitFormToCRMCAjax("leads", "#search_form" );
	});
	
	///open export popup
	$(".export_popup").click(function() {
		var type = $(this).attr('rel');
		export_all(type);
		return false;
	});

	//save lead detail
	$("#save_lead").click(function() {
		var first_name = $.trim( $("#first_name").val() );
		var last_name = $.trim( $("#last_name").val() );

		if( first_name == "" )  {
			flashAauto('Please enter first name.');
			return false;
		}

		if( last_name == "" )  {
			flashAauto('Please enter last name.');
			return false;
		}

		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id,first_name,last_name,phone,mobile,addressline1,addressline2,city,state,country','Mold_Lead:_save_lead','');
	});
});



function export_all(type)
{
	var export_ids = '', search_keyword = '';
	if( type == "selected" )  {
		//checkbox selected export
		var export_count = $(".checkbox_cls:checked").size();
		
		//get checked ids to export
		var exportIDs = new Array();
		$(".checkbox_cls:checked").each(function() {
			exportIDs.push($(this).val());
		});
		export_ids = exportIDs.join(",");
	}	else	{
		//all record exports - includes search filter
		var export_count = $("#total_num").val();
		search_keyword = $("#search_txt").val();
	}
	
	if( export_count > 0 )  {
		$("#export_ids").val(export_ids);
		$("#search_keyword").val(search_keyword);
		
		//open dialog popup
		$("#export_dialog").dialog({ modal: true, width: 450 });
	}	else	{
		alert("There is no leads to export.");
	}
}//END Function export_all