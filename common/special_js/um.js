var addActionRow;
$(document).ready(function(e) {
	userLogin();
	forget_password();
	reset_password();
	
	//Go to register page
	$("#register_link").click(function() {
		document.location = "register";
	});
	
	//logout user
	$("#logout_link").click(function() {
		PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','','Mold_User:logOut','');
	});
});

function userLogin() {
	$('#signin_link').click(function(e){
		//check if username and password entered
		var username = $.trim( $("#username").val() );
		var password = $.trim( $("#password").val() );
		
		if( username == "" )  {
			flashAauto('Please enter username.');
			return false;
		}
		
		if( password == "" )  {
			flashAauto('Please enter password.');
			return false;
		}
		
		//check user in database
		PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','username,password','Mold_User:login','');	//,remember_me
	});
}//END Function userLogin

function forget_password() {
	$('#forget_link').click(function(e){
		//check if username entered
		var username = $.trim( $("#username").val() );
		
		if( username == "" )  {
			flashAauto('Please enter username.');
			return false;
		}
		
		//check username in database
		PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','username','Mold_User:forget_password','');
	});
}//END Function forget_password

function reset_password() {
	$('#reset_password_link').click(function(e){
		//check if password and retype password is okay
		var new_password = $.trim( $("#new_password").val() );
		var repassword = $.trim( $("#repassword").val() );
		
		if( new_password == "" )  {
			flashAauto('Please enter New password.');
			return false;
		}
		
		if( repassword == "" )  {
			flashAauto('Please enter Re-type password.');
			return false;
		}
		
		if( new_password != repassword )  {
			flashAauto('New password and re-type password must match.');
			return false;
		}
		
		//check username in database
		PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','new_password,reset_key','Mold_User:reset_password','');
	});
}//END Function reset_password