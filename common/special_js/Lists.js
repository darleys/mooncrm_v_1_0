$(document).ready(function(e) {
    //delete

    $(".delete_record").click(function() {
        if( confirm('Are you sure to delete this record?') )
        {
            var id = $(this).attr('rel');
            $("#id").val(id);

            //delete account from database
            PROCESS_AJAX_URL_REQ($(this).attr('rel'),'', $( ".body-content" ));

            //submitFormToCRMCAjax($(this).attr('rel'));
        }
    });

    $("#view_record").click(function() {
            PROCESS_AJAX_URL_REQ($(this).attr('href'),'', $( ".body-content" ));
    });
    $("#edit_record").click(function() {
        alert("^^^^");
        PROCESS_AJAX_URL_REQ($(this).attr('href'),'', $( ".body-content" ));
        return false;
    });



    //submit search form
    $("#search").click(function() {
        PROCESS_AJAX_URL_REQ(module, $("#search_form").serialize(), $( ".body-content" ));
        //submitFormToOPCAjax(module, "#search_form" );
    });
    $("#crmc_search").click(function() {
        PROCESS_AJAX_URL_REQ(module, $("#search_form").serialize(), $( ".body-content" ));
        //submitFormToCRMCAjax(module, "#search_form" );
    });

    //pagination link requests
    $(".crmc_pagination a").click(function() {
        PROCESS_AJAX_URL_REQ($(this).attr('href'), $("#search_form").serialize(), $( ".body-content" ));
        //submitFormToCRMCAjax($(this).attr('href'));
        return false;
    });

    //pagination link requests
    $(".opc_pagination a").click(function() {
        PROCESS_AJAX_URL_REQ($(this).attr('href'), '', $( ".body-content" ));
        return false;
    });

    //clear search
    $("#reset-search").click(function() {
        $("#search_txt").val('');
        PROCESS_AJAX_URL_REQ(module, $("#search_form").serialize(), $( ".body-content" ));
    });
    $("#crmc-reset-search").click(function() {
        $("#search_txt").val('');
        PROCESS_AJAX_URL_REQ(module, $("#search_form").serialize(), $( ".body-content" ));
        //submitFormToCRMCAjax(module, "#search_form" );
    });

    ///open export popup
    $(".export_popup").click(function() {
        var type = $(this).attr('rel');
        export_all(type);
        return false;
    });
});


function export_all(type)
{
    var export_ids = '', search_keyword = '';
    if( type == "selected" )  {
        //checkbox selected export
        var export_count = $(".checkbox_cls:checked").size();

        //get checked ids to export
        var exportIDs = new Array();
        $(".checkbox_cls:checked").each(function() {
            exportIDs.push($(this).val());
        });
        export_ids = exportIDs.join(",");
    }	else	{
        //all record exports - includes search filter
        var export_count = $("#total_num").val();
        search_keyword = $("#search_txt").val();
    }

    if( export_count > 0 )  {
        $("#export_ids").val(export_ids);
        $("#search_keyword").val(search_keyword);

        //open dialog popup
        $("#export_dialog").dialog({ modal: true, width: 450 });
    }	else	{
        alert("There is no " + module + " to export.");
    }
}//END Function export_all