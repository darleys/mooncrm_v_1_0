$(document).ready(function(){
	validatePassword = function(inputId,errorId,errorMsg) {
		if(!checkPassword($('#'+inputId).val())) {
			if(errorMsg!="") $('#'+errorId).html(errorMsg); else $('#'+errorId).html('Please enter a valid Password');
			$('#'+errorId).html("wrong password"); 
			$('#'+inputId).focus();
			$('#'+inputId).val('');
		}else {
			$('#'+errorId).html(''); 
		}
	}
	
	checkPassword = function(password) {		
		if(alphanumeric(password) && password.length >6 && password.length < 16 && password.match('[A-Z]') && password.match('[a-z]')  && password.match('[0-9]')) { 
				return true;	
			}
			else {
				return false; 
			}
		
	}
	
	checkSame = function(inputId,confirmId,errorId,errorMsg) {
		if($('#'+inputId).val() != $('#'+confirmId).val()) {
			$('#'+errorId).html(errorMsg);
			$('#'+confirmId).val('');
			$('#'+confirmId).focus();
		}else {
			$('#'+errorId).html(''); 
		}
	}
	
	checkEmail = function(inputId,errorId,errorMsg) {
		if(!$('#'+inputId).val().match(/^[a-z]+\@[a-z]+\.[a-z]+$/)) {			
			if(errorMsg!="") $('#'+errorId).html(errorMsg); else $('#'+errorId).html('must be a valid email');
			$('#'+inputId).val('');
		}else {
			$('#'+errorId).html('');
		}
			
	}
	
	alphanumeric = function(alphane) {
		var numaric = alphane;
		for(var j=0; j<numaric.length; j++)
			{
			  var alphaa = numaric.charAt(j);
			  var hh = alphaa.charCodeAt(0);
			  if((hh > 47 && hh<58) || (hh > 64 && hh<91) || (hh > 96 && hh<123))
			  {
			  }
			else	{
				return false;
			  }
			}
	 return true;
	}	
	
	
});