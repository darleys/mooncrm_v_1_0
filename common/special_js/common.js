$(document).ready(function(e) {
    $(function() {
        initMoon();
    });
    $(document).on('focus',".date_picker", function(){
        $(this).datepicker();
    });
    $(document).on('focus',".datetime_picker", function(){
        $(this).datetimepicker();
    });

    //on link click - load data via ajax
    $( document ).on( "click", '.crmc_req', function() {
        var cmrc_url = $( this ).attr("href");
        PROCESS_CRMC_REQ(CRMC_KEYS['LINK'] + cmrc_url,$( ".body-content" ));
        return false;
    });

    $( document ).on( "click", '.opc_req', function() {
        var opc_url = $( this ).attr("href");
        PROCESS_OPC_REQ(opc_url,$( ".body-content" ));
        return false;
    });

    //import xml/csv popup
    $( document ).on( "click", '.show_popup', function() {
        var popupId = $(this).attr('href');
        $(popupId).dialog({ modal: true, width: 450 });
        return false;
    });

    //check selected file is xml/csv
    $( document ).on( "change", '#importFile', function() {
        var file_detail = pathToFile($(this).val());
        var file_ext = $(".fileType:checked").val();

        var allowed_extensions = [file_ext];
        if( allowed_extensions.indexOf(file_detail.extension) > -1 )  {
            //file is valid
        }	else	{
            //file is not valid - clear file input
            var control = $(this);
            control.replaceWith( control = control.clone( true ) );
            alert("Please select "+file_ext+" file.");
        }
    });

    //xml/csv import submit form
    $( document ).on( "click", '#import_submit', function() {
        var importFile = $("#importFile").val();
        if( importFile == "" )  {
            alert("Please select file to import.");
            return false;
        }

        submitFormToCRMC("#import_form");
        $('#import_dialog').dialog('close');
    });

    //xml/csv export submit form
    $( document ).on( "click", '#single_export_submit', function() {
        submitFormToCRMC("#single_export_form");
        $('#single_export_dialog').dialog('close');
    });

    //xml/csv export submit form
    $( document ).on( "click", '#export_submit', function() {
        submitFormToCRMC("#export_form");
        $('#export_dialog').dialog('close');
    });

    //radio button set value to hidden input id when radio button used
    $( document ).on( "click", 'input[type=radio]', function() {
        var name = $(this).attr('name');
        var value = $(this).val();
        $("#"+name).val(value);
    });

    //input number only in input fields
    $( document ).on( "keypress", '.numberOnly', function(e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
            //display error message
            $(this).siblings(".numberMsg").html("Digits Only").show().fadeOut(3000);
            return false;
        }
    });

    //input +/-/number only for phone fields
    $( document ).on( "keypress", '.phoneOnly', function(e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 && e.which != 45) {
            //display error message
            $(this).siblings(".numberMsg").html("Digits/+/- Characters Only").show().fadeOut(2000);
            return false;
        }
    });

    //uncheck/check select all
    $(document).on("click", '.checkbox_cls', function() {
        if( $(".checkbox_cls").size() == $(".checkbox_cls:checked").size() )  {
            var checkAll = true;
        }	else	{
            var checkAll = false;
        }

        $('.check_all').each(function() {
            this.checked = checkAll;
        });
    });
});

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}//END Function validateEmail

//check/uncheck all checkbox script
function checkAll(obj)  {
    var checkedAttr = obj.checked;
    $('.checkbox_cls').each(function() {
        this.checked = checkedAttr;
    });
}//END Function checkAll

function submitFormToCRMC( formID ) {
    var form_action = $(formID).attr('action');
    $(formID).attr('action', CRMC_KEYS["LINK"] + form_action);
    $(formID).submit();
    $(formID).attr('action', form_action);
}

function submitFormToCRMCAjax( url, formID ) {
    var crmc_url = CRMC_KEYS["LINK"] + url;
    PROCESS_CRMC_POST_REQ(crmc_url, $(formID).serialize(), $( ".body-content" ))
}

function submitFormToOPC( formID ) {
    var form_action = $(formID).attr('action');
    $(formID).attr('action', OPC_KEYS["LINK"] + form_action);
    $(formID).submit();
    $(formID).attr('action', form_action);
}

function submitFormToOPCAjax( url, formID ) {
    var opc_url = url;
    PROCESS_CRMC_POST_REQ(opc_url, $(formID).serialize(), $( ".body-content" ))
}

function pathToFile(str)
{
    var nOffset = Math.max(0, Math.max(str.lastIndexOf('\\'), str.lastIndexOf('/')));
    var eOffset = str.lastIndexOf('.');
    if(eOffset < 0)    {
        eOffset = str.length;
    }

    var name = str.substring(nOffset > 0 ? nOffset + 1 : nOffset, eOffset);
    var extension = str.substring(eOffset > 0 ? eOffset + 1 : eOffset, str.length);
    return {isDirectory: eOffset == str.length,
        path: str.substring(0, nOffset),
        name: name,
        extension: extension,
        file_name : name+"."+extension};
}