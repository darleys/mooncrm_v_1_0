$(document).ready(function(e) {
	charge_card();
});

function charge_card()  {
	//check card and amount detail
	$("#charge_card").click(function() {
		var card_name = $.trim( $("#card_name").val() );
		var card_number = $.trim( $("#card_number").val() );
		var exp_month = $.trim( $("#exp_month").val() );
		var exp_year = $.trim( $("#exp_year").val() );
		var cvc = $.trim( $("#cvc").val() );
		var amount = $.trim( $("#amount").val() );
		
		if( card_name == "" )  {
			flashAauto('Please enter Card name.');
			return false;
		}
		
		if( card_number == "" )  {
			flashAauto('Please enter Card number.');
			return false;
		}
		
		if( exp_month == "" )  {
			flashAauto('Please enter Expiry month.');
			return false;
		}
		
		if( exp_year == "" )  {
			flashAauto('Please enter Expiry year.');
			return false;
		}
		
		if( cvc == "" )  {
			flashAauto('Please enter cvc number back on your card.');
			return false;
		}
		
		if( amount == "" )  {
			flashAauto('Please enter Amount.');
			return false;
		}
		
		PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','card_name,card_number,exp_month,exp_year,cvc,amount','Mold_Payment:charge_card','');
	});
}//END Function save_contact