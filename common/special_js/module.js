$(document).ready(function(e) {
	save_module();
	
	//delete module
	$(".delete_module").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var module_id = $(this).attr('rel');
			$("#module_id").val(module_id);
			
			//delete user group from database
			PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','module_id','Mold_Module:delete_module','');
		}
	});
	
});


function save_module()  {
	//save user group detail
	$("#save_module").click(function() {
		//check if name and description entered
		var name = $.trim( $("#name").val() );
		var description = $.trim( $("#description").val() );
		
		if( name == "" )  {
			flashAauto('Please enter name.');
			return false;
		}
		
		if( description == "" )  {
			flashAauto('Please enter description.');
			return false;
		}
		
		PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','module_id,name,description','Mold_Module:save_module','');
	});
}//END Function save_module