$(document).ready(function(e) {
	save_department();
	
	//delete department
	$(".delete_department").click(function() {
		var department_id = $(this).attr('rel');
		$("#department_id").val(department_id);
		
		//delete user group from database
	    PROCESS_AJAX_REQ('/request'+THE_KEYS["MASQUERADE"]+'processor','department_id','Mold_Department:delete_department','');
	});
	
});


function save_department()  {
	//save department detail
	$("#save_department").click(function() {
		var name = $.trim( $("#name").val() );
		var description = $.trim( $("#description").val() );
		
		if( name == "" )  {
			flashAauto('Please enter name.');
			return false;
		}
		
		if( description == "" )  {
			flashAauto('Please enter description.');
			return false;
		}
		
		PROCESS_AJAX_REQ('/request'+THE_KEYS["MASQUERADE"]+'processor','department_id,name,description','Mold_Department:save_department','');
	});
}//END Function save_department