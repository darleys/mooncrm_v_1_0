PROCESS_CRMC_REQ = function(url,postApplyID) {
$.ajax({
    crossDomain:true,
    type: 'POST',
    url: url,
    dataType: "html"
}).done(function( html ) {
    postApplyID.html(html);
});
}//END Function Process_CRMC_REQ

PROCESS_CRMC_POST_REQ = function(url, data, postApplyID) {
    console.log(data);
    $.ajax({
        crossDomain:true,
        type: 'POST',
        url: url,
        data : data,
        dataType: "html"
    }).done(function( html ) {
        postApplyID.html(html);
    });
}//END Function PROCESS_CRMC_POST_REQ