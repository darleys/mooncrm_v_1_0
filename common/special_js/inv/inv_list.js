$(document).ready(function(e) {
	
	//add search field
	$("#search_fields").change(function() {
		var label = $(this).find('option:selected').html();
		var name = $(this).val();
		$("#quote_search_fields").append('<tr><td>'+label+' : </td> <td><input type="text" id="'+name+'" value="" /> <a href="javascript:;" class="remove-search-field"><i class="fa fa-times"></i></a></td></tr>');
	});
	
	//remove search field
	$("#quote_search_fields").on("click", '.remove-search-field', function() {
		$(this).parents("tr:first").remove();
	});
	
	//search for accounts based in searched keywords
	$("#search-quotes").click(function() {
		$("#page_num").val('1');
		search_quotes();
	});
	
	//reset search
	$("#reset-search").click(function() {
		$("#quote_search_fields").find("input").each(function() {
			$(this).val('');
		});
		
		$("#page_num").val('1');
		search_quotes();
	});
	
	//change to new page
	$("#quote_data").on("click", '#pagination a:not(.active)', function() {
		var page_num = $(this).attr('rel');
		$("#page_num").val(page_num);
		
		search_quotes();
		return false;
	});
	
	//change page size
	$("#page_size").change(function() {
		$("#page_num").val('1');
		search_quotes();
	});
	
	//show fields based on value is selected
	$("#show_fields option").click(function() {
		$(".all-account-fields").css('display', 'none');
		$("#show_fields option:selected").each(function() {
			var colName = $(this).val();
			$(".data-"+colName).css('display', 'table-cell');
		});
	});
	
	//remove field from table view
	$("#quote_data").on("click", '.remove-account-field', function() {
		var colName = $(this).attr('rel');
		$(".data-"+colName).css('display', 'none');
		$("#show_fields option[value="+colName+"]").removeAttr('selected');
	});

    $(document).on("click","#export_tq",function() {
        trans(0.5,$("#single_export_dialog"));
    });
	
	//export quote xml
	$("#quote_data").on("click", '.export-quote', function() {
		var quote_id = $(this).attr('rel');
		$("#export_id").val(quote_id);
        trans(0.5,$("#single_export_dialog"));
		//$("#single_export_dialog").dialog({ modal: true, width: 450 });
		return false;
	});
	
	///open export popup
	$(".export_popup").click(function() {
		var type = $(this).attr('rel');
		export_all_quotes(type);
		return false;
	});
	
	//Download zip file
	$("#export_all_form").on("click", '.download_zip_file', function() {
		var zip_no = $(this).attr('rel');
		$("#zip_no").val(zip_no);
		
		submitFormToCRMC("#export_all_form");
		$('#export_dialog').dialog('close');
		return false;
	});
	
	//Generate pdf
	$("#quote_data").on("click", '.generate-pdf', function() {
		var quote_id = $(this).attr('rel');
		$("#quoteID").val(quote_id);
        //$("#quote_pdf_dialog").fadeToggle();
        ///$("#quote_pdf_dialog").css('z-index','2000');
        trans(0.5,$('#quote_pdf_dialog'));

		//$("#quote_pdf_dialog").dialog({ modal: true, width: 350 });
	});
	
	//submit pdf generate form
	$("#quote_pdf_submit").click(function() {
		submitFormToCRMC("#quote_pdf_form");
        itrans();
		//$('#quote_pdf_dialog').dialog('close');
	});
	
	//Search quotes for getting initial data
	search_quotes();
});

function search_quotes()  {
	var searchDataArr = {};
	$("#quote_search_fields").find("input").each(function() {
		var id = $(this).attr('id');
		var value = $(this).val();
		searchDataArr[id] = value;
	});
	
	var searchDataJson = JSON.stringify(searchDataArr);
	$("#searchDataJson").val(searchDataJson);

	PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','searchDataJson,page_size,page_num','Mold_Quotes:_search_quotes','');
}//END Function search_quotes

function export_all_quotes(type)  {
	var export_quote_ids = '', searchDataJson = '';
	var zip_bunch_size = $("#zip_bunch_size").val();
	
	if( type == "selected" )  {
		//checkbox selected export
		var export_quote_count = $(".checkbox_cls:checked").size();
		
		//get checked quotes to export
		var quote_ids = new Array();
		$(".checkbox_cls:checked").each(function() {
			quote_ids.push($(this).val());
		});
		export_quote_ids = quote_ids.join(",");
	}	else	{
		//all record exports - includes search filter
		var export_quote_count = $("#total_quotes").val();
		
		//get current search parameters and add to input
		var searchDataArr = {};
		$("#quote_search_fields").find("input").each(function() {
			var id = $(this).attr('id');
			var value = $(this).val();
			searchDataArr[id] = value;
		});
		
		searchDataJson = JSON.stringify(searchDataArr);
	}
	
	if( export_quote_count > 0 )  {
		//if some quotes to export than show the zip files to export
		var total_zips = Math.ceil(export_quote_count/zip_bunch_size);
		
		//add zip file name and download link
		var zip_files = '<table border="1" cellspacing="0" cellpadding="5" style="min-width:220px;">';
		for(var i=1; i <= total_zips; i++)  {
			zip_files += '<tr><td>quotes_'+i+'.zip</td><td><a href="#" class="download_zip_file" rel="'+i+'">Download</a></td>';
		}
		zip_files += '</table>';
		$('#zip_files').html(zip_files);
		
		//set search fields and selected quote ids
		$("#export_quote_ids").val(export_quote_ids);
		$("#export_search_data").val(searchDataJson);
		
		//open dialog popup
		$("#export_dialog").dialog({ modal: true, width: 450 });
	}	else	{
		alert("There is no quotes to export.");
	}
}//END Function export_all_quotes