$(document).ready(function(e) {
	//add new product row in product list
	$("#add_product").click(function() {
		add_product();
	});
	
	//on product quantity change - calculate new total
	$("#product_list").on( "change", 'input.quantity, input.discount', function() {
		calculate_total();
	});
	
	//on remove product - calculate new total
	$("#product_list").on( "click", 'a.remove_product', function() {
		$(this).parents("tr").remove();
        if($('.product_search').length <=0)
            lineNum=0;
		calculate_total();
		return false;
	});
	
	//auto suggest account name
	$("#name").autocomplete({
		source : CRMC_KEYS["LINK"] + 'ajax_search_account',
		minLength: 2,
		select: function( event, ui ) {
			select_account(ui.item.id);
		}
	});
	
	//add search field
	$("#search_fields").change(function() {
		var label = $(this).find('option:selected').html();
		var name = $(this).val();
		$("#quote_search_fields").append('<tr><td>'+label+' : </td> <td><input type="text" id="'+name+'" value="" /> <a href="javascript:;" class="remove-search-field"><i class="fa fa-times"></i></a></td></tr>');
	});
	
	//remove search field
	$("#quote_search_fields").on("click", '.remove-search-field', function() {
		$(this).parents("tr:first").remove();
	});
	
	//search for accounts based in searched keywords
	$("#search-accounts").click(function() {
		$("#page_num").val('1');
		search_accounts();
	});
	
	//change to new page
	$("#customer_data").on("click", '#pagination a:not(.active)', function() {
		var page_num = $(this).attr('rel');
		$("#page_num").val(page_num);
		
		search_accounts();
		return false;
	});
	
	//get customer detail with ajax and open popup
	$("#customer_data,#customer_name").on("click", '.customer-detail', function() {
		var account_id = $(this).attr('rel');
		$("#account_id").val(account_id);
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','account_id','Mold_Quotes:_account_detail','');
	});
	
	//show fields based on value is selected
	$("#show_fields option").click(function() {
		$(".all-account-fields").css('display', 'none');
		$("#show_fields option:selected").each(function() {
			var colName = $(this).val();
			var colName = $(this).val();
			$(".data-"+colName).css('display', 'table-cell');
		});
	});
	
	//remove field from table view
	$("#customer_data").on("click", '.remove-account-field', function() {
		var colName = $(this).attr('rel');
		$(".data-"+colName).css('display', 'none');
		$("#show_fields option[value="+colName+"]").removeAttr('selected');
	});
	
	//when account is selected - goto step2 for adding products
	$("#customer_data").on("click", '.select-customer', function() {
		var account_id = $(this).attr('rel');
		select_account(account_id);
	});
	
	//open contract popup
	$("#product_list").on( "click", '.conrtact_popup', function() {
		var contract_id = $(this).attr('rel');
		var contractDetail = contracts[contract_id];
		
		$("#contractDetailPopup .code").html(contractDetail.ccnt_id);
		$("#contractDetailPopup .name").html(contractDetail.ccnt_name);
		$("#contractDetailPopup .desc").html(contractDetail.ccnt_desc);
		$("#contractDetailPopup .discount").html(contractDetail.discount);
		$("#contractDetailPopup .start_date").html(contractDetail.start_date);
		$("#contractDetailPopup .end_date").html(contractDetail.end_date);
		
		$("#contractDetailPopup").dialog({ modal: true, width: 600 });
	});
	
	//toggle price format
	$("#toggle_format").click(function() {
		toggle_price('true');
	});
	
	//save new quote
	$("#confirm_quote").click(function() {
		save_quote('create_quote');
	});
	
	//save edit quote
	$("#confirm_edit_quote").click(function() {
		save_quote('edit_quote');
	});
	
	//for edit quote page bind auto complete product search
	bind_product_autocomplete("#product_list input.product_search");
	calculate_total();
});

function search_accounts()  {
	var searchDataArr = {};
	$("#quote_search_fields").find("input").each(function() {
		var id = $(this).attr('id');
		var value = $(this).val();
		searchDataArr[id] = value;
	});
	
	var searchDataJson = JSON.stringify(searchDataArr);
	$("#searchDataJson").val(searchDataJson);
	
	PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','searchDataJson,page_num','Mold_Quotes:_search_accounts','');
}//END Function search_accounts

//add new product in product list
var lineNum=0;
var contracts = new Array();
function add_product()
{
	//add new product in table
	var prodcut_str = '<tr>';
	prodcut_str += '<td><input type="text" class="product_search" id="product_'+lineNum+'" data-provide="typeahead" /><input type="hidden" id="product_id_'+lineNum+'" class="product_id" />';
	prodcut_str += '<input type="hidden" class="unit_price" id="product_unitp_'+lineNum+'" /><input type="hidden" class="contract_price" id="product_conp_'+lineNum+'"/>';
	prodcut_str += '<input type="hidden" class="contract_applied" id="product_conapp_'+lineNum+'" /><input type="hidden" class="contract_code" id="product_concode_'+lineNum+'" /></td>';
	prodcut_str += '<td class="td_unit_price toggle_price" id="product_unitpshow_'+lineNum+'">$0</td>';
	prodcut_str += '<td class="td_contract_price" id="product_conpshow_'+lineNum+'">$0</td>';
	prodcut_str += '<td><input type="text" class="quantity" id="product_qty_'+lineNum+'" value="1" style="width:40px;" /></td>';
	prodcut_str += '<td><input type="text" class="discount" id="product_dis_'+lineNum+'" value="0" style="width:40px;" />%</td>';
	prodcut_str += '<td class="td_total_price" id="product_ttp_'+lineNum+'">$0</td>';
	prodcut_str += '<td><a href="javascript:;" class="remove_product">Remove</a></td>';
	prodcut_str += '</tr>';
    lineNum++;
	$("#product_list").append(prodcut_str);
	
	bind_product_autocomplete("#product_list tr:last input.product_search");
}//END Function add_product

function bind_product_autocomplete(obj)
{

    var productNames = new Bloodhound({
        name: 'Products',
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        remote: {
            url : CRMC_KEYS["LINK"] + 'ajax_search_product?term=%QUERY',

            filter: function (pnames) {
               // console.log(pnames);
                // Map the remote source JSON array to a JavaScript array
                return $.map(pnames, function (pnames) {
                 //   console.log("@@@@@"+pnames.label)
                    return {
                        value: pnames.label,
                        id: pnames.id,
                        unitp:pnames.price
                    };
                });

            }
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });
    productNames.initialize();
	//bind autocomplete script to product input
	$(obj).typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
        displayKey: 'value',
        source: productNames.ttAdapter()

	}).on('typeahead:selected', productSelected)
      .on('typeahead:autocompleted', productSelected);
}//END Function bind_product_autocomplete

function productSelected(e, datum) {
    //current ROW (LINE ITEM NUM)
    $prlineNum = jQuery.trim(e.target.id);
    $prlineNum=$prlineNum.slice(8);
    //selected product ID
    $selprID=jQuery.trim(datum["id"]);
    //selected product Unit Price
    $selprUnitP=jQuery.trim(datum["unitp"]);
    //var tdObj = $(this).parent("td");
    //var trObj = $(this).parents("tr");

    //find contract price for selected product
    $.ajax({
        url : CRMC_KEYS["LINK"] + 'ajax_product_contract_price',
        data : { product_id : $selprID, account_id : $("#account_id").val()  },
        type : 'POST',
        success : function(contractData) {
            var contractData = $.parseJSON(contractData);
            contracts[contractData.id] = contractData;

            //assigning value to hidden inputs and show unit price in view
            $("#product_id_"+$prlineNum).val($selprID);
            $("#product_unitp_"+$prlineNum).val($selprUnitP);
            $("#product_unitpshow_"+$prlineNum).html($selprUnitP);
            //tdObj.find(".product_id").val(ui.item.id);
            //trObj.find(".td_unit_price").html("$"+ui.item.price);
            //tdObj.find(".unit_price").val(ui.item.price);

            $("#product_conapp_"+$prlineNum).val(contractData.ccnt_name);

            $("#product_concode_"+$prlineNum).val(contractData.ccnt_id);
            $("#product_conpshow_"+$prlineNum).html("<span class='toggle_price'>$"+contractData.price+"</span>");
            $("#product_conp_"+$prlineNum).val(contractData.price);
            if( contractData.price > 0 )  {
                $("#product_conpshow_"+$prlineNum).append('&nbsp;[<a href="javascript:;" rel="'+contractData.id+'" class="conrtact_popup">'+contractData.ccnt_id+'</a>]');
            }

            //this function will calculate row total and grand total
            calculate_total();
        }
    });
}

function calculate_total()
{
	var grand_total = 0;
	
	//foreach all products
	$("#product_list tr").each(function() {
		
		//fetch price and quantity of product
		var price = jQuery.trim($(this).find(".unit_price").val());
		var contract_price = jQuery.trim($(this).find(".contract_price").val());
		if( contract_price > 0 )
			price = contract_price;
		
		var discountPrice = price - (price * jQuery.trim($(this).find(".discount").val()))/100;
		var quantity = parseInt(jQuery.trim($(this).find(".quantity").val()));
		
		//calculate total price for product
		var total = price * quantity;
		var discountTotal = parseFloat((discountPrice * quantity).toFixed(2));
		$(this).find(".td_total_price").html("<span class='toggle_price'>$"+discountTotal+"</span>");
		if( total != discountTotal ) {
			total = parseFloat(total.toFixed(2));
			$(this).find(".td_total_price").append("<br/>(<span class='toggle_price'>$"+total+"</span> without discount)");
		}
		
		//add product price in grand total
		grand_total = grand_total + discountTotal;
	});
	
	grand_total = (grand_total).toFixed(2);
	$("#grand_total").html("$"+grand_total);
	
	toggle_price('false');
}//END Function calculate_total

function save_quote(type)
{
	if( $("#quote_name").val() == "" )  {
		flashAauto('Please enter Quote name.');
		return false;
	}
	
	//get all products and price
	var quote_products = new Array();
	$(".product_id").each(function() {
		var trObj = $(this).parents("tr:first");
		var product_id = $(this).val();
		
		//get required values and push in array
		if( product_id != "" )  {
			var temp = new Object();
			temp.product_id = product_id;
			temp.quantity = $(trObj).find(".quantity").val();
			temp.discount_percentage = $(trObj).find(".discount").val();
			temp.contract_price = $(trObj).find(".contract_price").val();
			temp.contract_applied = $(trObj).find(".contract_applied").val();
			temp.contract_code = $(trObj).find(".contract_code").val();
			quote_products.push(temp);
		}
	});
	
	if( quote_products.length == 0 )  {
		flashAauto('Please select at least one product.');
		return false;
	}
	
	var quote_products_json = JSON.stringify(quote_products);
	$("#quote_products").val(quote_products_json);
	
	if( type == "create_quote" ) {
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','account_id,quote_name,quote_department,quote_products','Mold_Quotes:_save_quote','');
	}	else if( type == "edit_quote" ) {
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id,account_id,quote_name,quote_department,quote_products','Mold_Quotes:_edit_quote','');
	}
}//END function save_quote

function select_account(account_id) {
	if( $("#quote_name").val() == "" )  {
		alert("Please enter quote name.");
		return false;
	}
	
	$("#account_id").val(account_id);
	add_product();
	
	//hide customer detail step and show second step
	$("#first_step").hide();
	$("#second_step").show();
    //alert($('.tab-container').tabs('length'));
    //$('.nav-tabs > li.active').removeClass('active');
    //$('.nav-tabs' > $('#new_quote_products')).parent().addClass('active');
    //$('#new_quote_header').removeClass('active in');
    //$('#new_quote_products').addClass('active in');
    //$('.nav-tabs').tabs( "option", "active", 1 );
    $('#new_quote').find("#lnew_quote_products").tab('show')
}//END function select_account

function toggle_price(change)  {
	//get format to convert string
	var format = $('#number_format').val();
	if( change == "true" )  {
		format = ( format == "number" ) ? "money" : "number";
		$('#number_format').val(format);
	}
	
	//get all toggle prices
	$(".toggle_price").each(function() {
		var price = $(this).html().replace("$", "");
		
		if( format == "money" )  {
			price = parseFloat(price);
			price = '$' + price.toFixed(2).replace(/./g, function(c, i, a) {
							return i && c !== "." && !((a.length - i) % 3) ? ',' + c : c;
						});
		}	else	{
			price = '$'+price.replace(/,/g, '');
		}
		$(this).html(price);
    });
}//END Function toggle_price