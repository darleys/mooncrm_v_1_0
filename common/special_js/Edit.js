$(document).ready(function(e) {

    //show tabs in add/edit form
    $( "#tabs" ).tabs();

    //open next tab and make last green
    $(".next-tab-btn").click(function() {
        $(".ui-tabs-active").next().find('a').css('color', 'green').trigger('click');
    });

    $("#save").click(function() {
        submitFormToCRMCAjax(module + '/Save', "#save_form" );
    });

    $("#opcsave").click(function() {
        submitFormToOPCAjax(module + '/Save', "#save_form" );
    });

    $("#opcpost").click(function() {
        submitFormToOPCAjax(url, "#save_form" );
    });
;

    $("input[type=checkbox]").click(function() {
       if($(this).is(':checked'))
            $(this).val(1);
        else
            $(this).val(0);
    });


        var presidents = [
            "George Washington",
            "John Adams",
            "Thomas Jefferson",
            "James Madison"
        ];

    $(".autofill").autocomplete({
        source:presidents,
        minLength: 3,
        select: function (event, ui) {
            var label = ui.item.label;
            var value = ui.item.value;
            alert($(this).attr('module'));
        }
    });



});


function form_post(id)
{
    submitFormToOPCAjax(module + '/Save', "#save_form_" + id );
}


function renderExtraField(type)
{
    if($('#type').val() == 'text')
    {
        $('#default_th').html('Default Text');
        $('#default_td').html('<input type="text" id="default_value" name="default_value">');
        $('#default_tr').show();
    }

    if($('#type').val() == 'relation')
        $('.extra1_module').show();

    if($('#type').val() == 'dropdown' || $('#type').val() == 'multiselect')
        $('.extra1_ddlist').show();

    if($('#type').val() != 'relation')
        $('.extra1_module').hide();

    if($('#type').val() != 'text')
        $('#default_tr').hide();

    if($('#type').val() != 'dropdown' && $('#type').val() != 'multiselect')
        $('.extra1_ddlist').hide();
}


