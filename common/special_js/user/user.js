$(document).ready(function(e) {
	save_user();
	save_user_roles();
	save_user_departments();
	
	//search for users
	$("#search-users").click(function() {
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','search_txt','Mold_User:_search_users','');
	});
	
	//reset search
	$("#reset_search").click(function() {
		$("#search_txt").val('');
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','search_txt','Mold_User:_search_users','');
	});
	
	//delete user
	$(".delete_user").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var id = $(this).attr('rel');
			$("#id").val(id);
			
			//delete user from database
			PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id','Mold_User:_delete_user','');
		}
	});
	
    //export user xml
	$("#user_list").on("click", '.export-user', function() {
		var user_id = $(this).attr('rel');
		$("#export_id").val(user_id);
		//$("#single_export_dialog").dialog({ modal: true, width: 450 });
		return false;
	});
	
	///open export popup
	$(".export_popup").click(function() {
		var type = $(this).attr('rel');
		export_all_users(type);
		return false;
	});
	
	//Download zip file
	$("#export_all_form").on("click", '.download_zip_file', function() {
		var zip_no = $(this).attr('rel');
		$("#zip_no").val(zip_no);
		
		$("#export_all_form").submit();
		return false;
	});
	
	//search for users
	$(".search_user").autocomplete({
		source : CRMC_KEYS['LINK']+'ajax_search_user',
		minLength: 2,
		select: function( event, ui ) {
			var callback = $(this).attr('data-callback');
			
			//assign module actions to user
			if( callback == "assign_roles" )  {
				$(this).siblings("#user_id").val(ui.item.id);
				get_user_eactions();
			}
			
			//department management user auto complete
			if( callback == "assign_department" )  {
				$(this).siblings("#user_id").val(ui.item.id);
				get_user_departments();
			}
		},
		change: function( event, ui ) {
			var callback = $(this).attr('data-callback');
			var user_id = ( ui.item != null ) ? ui.item.id : '';
			
			//user form report to auto complete
			if( callback == "user_form" )  {
				$(this).siblings("#reports_to").val(user_id);
			}
		}
	});



    var accountNames = new Bloodhound({
        name: 'Products',
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('label'),
        remote: {
            url : CRMC_KEYS["LINK"] + 'ajax_search_account?term=%QUERY',

            filter: function (anames) {
                // console.log(pnames);
                // Map the remote source JSON array to a JavaScript array
                return $.map(anames, function (anames) {
                    //   console.log("@@@@@"+pnames.label)
                    return {
                        value: anames.label,
                        id: anames.id

                    };
                });

            }

        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    accountNames.initialize();
    //bind autocomplete script to product input
    $("#search_txt").typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            displayKey: 'value',
            source: accountNames.ttAdapter()

        }).on('typeahead:selected', accountSelected)
        .on('typeahead:autocompleted', accountSelected);


    function accountSelected(e, datum) {
        //selected account Value
        $accountName=jQuery.trim(datum["value"]);
        $("#search_txt").val($accountName);
        submitFormToCRMCAjax("accounts", "#search_form" );
        return false;

    }

	
	//Add department to user
		addDepartmentRow = "<tr>"+$("#user_departments tr:first").html()+"</tr>";
		
		//Add department row
		$("#add_department").click(function() {
			$("#user_departments .save_btns").before(addDepartmentRow);
		});
		
		//Remove department row
		$("#user_departments").on( "click", 'a.remove_department', function() {
			$(this).parents("tr:first").remove();
		});
});

function export_all_users(type)  {
	
	var export_user_ids = '', search_keyword = '';
	var zip_bunch_size = $("#zip_bunch_size").val();
	
	if( type == "selected" )  {
		//checkbox selected export
		var export_user_count = $(".checkbox_cls:checked").size();
		
		//get checked users to export
		var user_ids = new Array();
		$(".checkbox_cls:checked").each(function() {
			user_ids.push($(this).val());
		});
		export_user_ids = user_ids.join(",");
	}	else	{
		//all record exports - includes search filter
		var export_user_count = $("#total_users").val();
		search_keyword = $("#search_txt").val();
	}
	
	if( export_user_count > 0 )  {
		//if some users to export than show the zip files to export
		/*var total_zips = Math.ceil(export_user_count/zip_bunch_size);
		
		//add zip file name and download link
		var zip_files = '<table border="1" cellspacing="0" cellpadding="5" style="min-width:220px;">';
		for(var i=1; i <= total_zips; i++)  {
			zip_files += '<tr><td>users_'+i+'.zip</td><td><a href="#" class="download_zip_file" rel="'+i+'">Download</a></td>';
		}
		zip_files += '</table>';
		$('#zip_files').html(zip_files);*/
		
		$("#export_user_ids").val(export_user_ids);
		$("#search_keyword").val(search_keyword);
		
		//open dialog popup
		$("#export_dialog").dialog({ modal: true, width: 450 });
	}	else	{
		alert("There is no users to export.");
	}
}//END Function export_all_users

function save_user()  {
	//save user detail
	$("#save_user").click(function() {
		//check if username and password entered
		var id = $("#id").val();
		var reports_to = $.trim( $("#reports_to").val() );
		var myfname = $.trim( $("#myfname").val() );
		var mylname = $.trim( $("#mylname").val() );
		var myemail = $.trim( $("#myemail").val() );
		var myusername = $.trim( $("#myusername").val() );
		var mypassword = $.trim( $("#mypassword").val() );
		
		/*if( reports_to == "" )  {
			flashAauto('Please select reports to.');
			return false;
		}*/
		
		if( myfname == "" )  {
			flashAauto('Please enter first name.');
			return false;
		}
		
		if( mylname == "" )  {
			flashAauto('Please enter last name.');
			return false;
		}
		
		if( !validateEmail(myemail) )  {
			flashAauto('Please enter valid email address.');
			return false;
		}
		
		if( myusername == "" )  {
			flashAauto('Please enter username.');
			return false;
		}
		
		if( mypassword == "" && id == "" )  {
			flashAauto('Please enter password.');
			return false;
		}
		
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id,myfname,mylname,myemail,myusername,mypassword,myphone,myaddress1,myaddress2,mycity,mystate,myzip,mycountry,reports_to','Mold_User:_save_user','');
	});
}//END Function save_user

function save_user_roles()  {
	$("#save_user_roles").click(function() {
		//check if user is selected
		var user_id = $.trim( $("#user_id").val() );
		if( user_id == "" )  {
			flashAauto('Please select user.');
			return false;
		}
		
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','user_id,eactions','Mold_User:_save_user_roles','');
	});
}//END Function save_user_modules

function save_user_departments()  {
	$("#save_departments").click(function() {
		//check if user is selected
		var user_id = $.trim( $("#user_id").val() );
		if( user_id == "" )  {
			flashAauto('Please select user.');
			return false;
		}
		
		//get all department ids
		var departmentIDs = new Array();
		$("#user_departments .department_id").each(function(index) {
			if( $(this).val() != "" )
				departmentIDs[index] = $(this).val();
        });
		$("#department_ids").val( departmentIDs.join(',') );
		
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','user_id,department_ids','Mold_User:_save_departments','');
	});
}//END Function save_user_departments

function get_user_eactions() {
	var user_id = $.trim( $("#user_id").val() );
	
	$("#eactions").find("option:selected").removeAttr('selected');
	if( user_id != "" )  {
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','user_id','Mold_User:_get_user_roles','');
	}
}//END Function get_user_modules

function get_user_departments()  {
	var user_id = $.trim( $("#user_id").val() );
	
	if( user_id != "" )  {
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','user_id','Mold_User:_get_user_departments','');
	}	else	{
		$("#user_departments").html('');
	}
}//END get_user_departments