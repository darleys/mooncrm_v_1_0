/**
 * Created by 10046078 on 6/3/2015.
 */

function getUserAccess(id)
{
    submitFormToOPCAjax('/workshop/Access/GetAccess/Users/' + $(id).val(), "" );
}

function getAccess(id)
{
    submitFormToOPCAjax('/workshop/Access/GetAccess/' + $('#accessType').val() +'/' + $('#accessList').val(), "#get_Access" );
}