$(document).ready(function(e) {
	save_account();
	save_contact();
	
	//hide and show account contact list
	$(".account_row td.openContacts").click(function() {
		var trObj = $(this).parent();
		
		//hide all contact list
		$(".contact_list_row").hide();
		
		if( !trObj.hasClass('open') )  {
			//hide all contact row list
			$(".account_row").removeClass('open');
			
			//add open class and show contact list
			var idCls = trObj.attr('id');
			$("."+idCls).show('slow');
			trObj.addClass('open');
		}	else	{
			//remove open class to open contact list again
			$(".account_row").removeClass('open')
		}
	});
	
	//show tabs in add/edit form
	$( "#tabs" ).tabs();
	
	//open next tab and make last green
	$(".next-tab-btn").click(function() {
		$(".ui-tabs-active").next().find('a').css('color', 'green').trigger('click');
	});
	
	//delete account
	$(".delete_account").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var id = $(this).attr('rel');
			$("#id").val(id);
			
			//delete account from database
			PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id','Mold_Account:_delete_account','');
		}
	});
	
	//load contact form
	$(".contact_list_row").on( "click", 'a.contact_form', function() {
		//contact_form_data
		var contact_id = $(this).attr('rel');
		var account_id = $(this).parents("tr.contact_list_row:first").find(".account-id").val();
		
		$.ajax({
			url : CRMC_KEYS['LINK']+'account_contact_form',
			data : { contact_id : contact_id, account_id : account_id  },
			type : 'POST',
			success : function(form_html) {
				$(".contact_form_data").html('');
				$(".account_"+account_id+" .contact_form_data").html(form_html);
			}
		});
	});
	
	//cancel contact form
	$(".contact_list_row").on('click', 'a.cancel-contact-form', function() {
		$(".contact_form_data").html('');
	});
	
	//delete contact
	$(".contact_list_row").on('click', 'a.delete-contact', function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var contact_id = $(this).attr('rel');
			$("#delete_contact_id").val(contact_id);
			
			PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','delete_contact_id','Mold_Account:_delete_contact','');
			$(this).parents("tr:first").remove();
		}
	});
	
	///open export popup
	$(".export_popup").click(function() {
		var type = $(this).attr('rel');
		export_all_accounts(type);
		return false;
	});

    /*
	//auto suggest account name
	$("#search_txt").autocomplete({
		source : CRMC_KEYS['LINK']+'ajax_search_account',
		minLength: 2,
		select: function( event, ui ) {
			$("#search_txt").val("["+ui.item.value+"]");
			//$("#search_form").submit();
			return false;
		}
	});
     */



        var accountNames = new Bloodhound({
            name: 'Products',
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('label'),
            remote: {
                url : CRMC_KEYS["LINK"] + 'ajax_search_account?term=%QUERY',

                filter: function (anames) {
                    // console.log(pnames);
                    // Map the remote source JSON array to a JavaScript array
                    return $.map(anames, function (anames) {
                        //   console.log("@@@@@"+pnames.label)
                        return {
                            value: anames.label,
                            id: anames.id

                        };
                    });

                }

            },
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });

            accountNames.initialize();
        //bind autocomplete script to product input
    $("#search_txt").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            },
            {
                displayKey: 'value',
                source: accountNames.ttAdapter()

            }).on('typeahead:selected', accountSelected)
            .on('typeahead:autocompleted', accountSelected);


    function accountSelected(e, datum) {
        //selected account Value
        $accountName=jQuery.trim(datum["value"]);
        $("#search_txt").val($accountName);
        submitFormToCRMCAjax("accounts", "#search_form" );
        return false;

    }

	//submit search form
	$("#search-accounts").click(function() {
		submitFormToCRMCAjax("accounts", "#search_form" );
	});

	//pagination link requests
	$(".accounts_pagination a").click(function() {
		submitFormToCRMCAjax($(this).attr('href'), "#search_form" );
		return false;
	});
	
	//clear search
	$("#reset-search").click(function() {
		$("#search_txt").val('');
		submitFormToCRMCAjax("accounts", "#search_form" );
	});
});

function save_account()  {
	//save account detail
	$("#save_account").click(function() {
		var name = $.trim( $("#name").val() );
		if( name == "" )  {
			flashAauto('Please enter name.');
			return false;
		}
		
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id,name,description,account_type,industry,annual_revenue,phone_fax,billing_address_street,billing_address_city,billing_address_state,billing_address_country,billing_address_postalcode,shipping_address_street,shipping_address_city,shipping_address_state,shipping_address_country,shipping_address_postalcode,phone_office,phone_alternate,rating,website,ownership,status','Mold_Account:_save_account','');
	});
}//END Function save_account

function save_contact()  {
	//save contact detail
	$(".contact_list_row").on( "click", '.save-contact', function() {
		var first_name = $.trim( $("#first_name").val() );
		var last_name = $.trim( $("#last_name").val() );
		var email = $.trim( $("#email").val() );
		
		if( first_name == "" )  {
			flashMessage('.contact_form_err', 'Please enter First name.');
			return false;
		}
		
		if( last_name == "" )  {
			flashMessage('.contact_form_err', 'Please enter Last name.');
			return false;
		}
		
		if( !validateEmail(email) )  {
			flashMessage('.contact_form_err', 'Please enter valid Email address.');
			return false;
		}
		
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','contact_id,account_id,first_name,last_name,title,email,primary_contact,addressline1,addressline2,city,state,country,postal_code','Mold_Account:_save_contact','');
	});
}//END Function save_contact

function export_all_accounts(type)  
{
	var export_ids = '', search_keyword = '';
	if( type == "selected" )  {
		//checkbox selected export
		var export_count = $(".checkbox_cls:checked").size();
		
		//get checked ids to export
		var exportIDs = new Array();
		$(".checkbox_cls:checked").each(function() {
			exportIDs.push($(this).val());
		});
		export_ids = exportIDs.join(",");
	}	else	{
		//all record exports - includes search filter
		var export_count = $("#total_accounts").val();
		search_keyword = $("#search_txt").val();
	}
	
	if( export_count > 0 )  {
		$("#export_ids").val(export_ids);
		$("#search_keyword").val(search_keyword);
		
		//open dialog popup
		$("#export_dialog").dialog({ modal: true, width: 450 });
	}	else	{
		alert("There is no accounts to export.");
	}
}//END Function export_all_accounts