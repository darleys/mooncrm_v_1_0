$(document).ready(function(e) {
    $("#cnt_id").change(function() {
		cmrc_url = "contract?type="+$(this).val();
		PROCESS_CRMC_REQ(CRMC_KEYS['LINK'] + cmrc_url,$( ".body-content" ));
	});
	
	//delete contract
	$(".delete_contract").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var contract_id = $(this).attr('rel');
			$("#id").val(contract_id);
			
			//delete contract from database
			PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id','Mold_CMEngine:_delete_contract','');
			$("#contract_"+contract_id).remove();
			$(".contract_detail_"+contract_id).remove();
		}
	});
	
	//hide and show contract detail
	$(".contract_row").click(function() {
		//hide all contract detail
		$(".contract_detail_row").hide();
		
		if( !$(this).hasClass('open') )  {
			$(".contract_row").removeClass('open')
			
			//add open class and show contract detail
			var idCls = $(this).attr('id');
			$("."+idCls).show('slow');
			$(this).addClass('open');
		}	else	{
			//remove open class to open detail again
			$(".contract_row").removeClass('open')
		}
	});
});