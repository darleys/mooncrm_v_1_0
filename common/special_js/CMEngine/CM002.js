$(document).ready(function(e) {
	save_contract();
	
	//load datepicker - start and end date selection
	$( "#start_date" ).datepicker({
		dateFormat : 'yy-mm-dd',
		onClose: function( selectedDate ) {
		  $( "#end_date" ).datepicker( "option", "minDate", selectedDate );
		}
    });
	
    $( "#end_date" ).datepicker({
		dateFormat : 'yy-mm-dd',
		onClose: function( selectedDate ) {
		  $( "#start_date" ).datepicker( "option", "maxDate", selectedDate );
		}
    });
});

function save_contract()  {
	$("#save_contract").click(function() {
		
		if( $("#ccnt_name").val() == "" )  {
			flashAauto('Please enter Contract Name.');
			return false;
		}
		
		if( $("#ccnt_id").val() == "" )  {
			flashAauto('Please enter Contract Code.');
			return false;
		}
		
		if( $("#ccnt_desc").val() == "" )  {
			flashAauto('Please enter Contract description.');
			return false;
		}
		
		if( $("#product_name").val() == "" )  {
			flashAauto('Please enter Product name.');
			return false;
		}
		
		if( $("#match_criteria").val() == "" )  {
			flashAauto('Please select Name match criteria.');
			return false;
		}
		
		if( $("#contract_price").val() == "" )  {
			flashAauto('Please enter Discount % or Actual Price.');
			return false;
		}
		
		if( $("#start_date").val() == "" )  {
			flashAauto('Please select start date.');
			return false;
		}
		
		if( $("#end_date").val() == "" )  {
			flashAauto('Please select End date.');
			return false;
		}
		
		//save to database
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id,ccnt_name,ccnt_id,ccnt_desc,cnt_id,product_name,match_criteria,contract_price,start_date,end_date','Mold_CM002:_save_contract','');
	});
}