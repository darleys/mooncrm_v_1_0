$(document).ready(function(e) {
	save_contract();
	bind_product_autocomplete(".product");
	
	//load datepicker - start and end date selection
	$( "#start_date" ).datepicker({
		dateFormat : 'yy-mm-dd',
		onClose: function( selectedDate ) {
		  $( "#end_date" ).datepicker( "option", "minDate", selectedDate );
		}
    });
	
    $( "#end_date" ).datepicker({
		dateFormat : 'yy-mm-dd',
		onClose: function( selectedDate ) {
		  $( "#start_date" ).datepicker( "option", "maxDate", selectedDate );
		}
    });
	
	//add contract product
    $("#add-contract-product").click(function() {
		add_product();
		return false;
	});
	
	//remove contract product
	$("#product_list").on("click", '.remove-contract-product', function() {
		$(this).parents("div:first").remove();
		return false;
	});
});

//add new product in product list
function add_product()
{
	//add new product in table
	var prodcut_str = '<div style="margin-top:5px;"><input type="text" class="product" /><input type="hidden" class="product_id" /> &nbsp; <a href="javasript:;" class="remove-contract-product">Remove</a></div>';
	$("#product_list").append(prodcut_str);
	bind_product_autocomplete("#product_list .product:last");
}

function bind_product_autocomplete(obj)  {
	//bind autocomplete script to product input
	$(obj).autocomplete({
		source : CRMC_KEYS['LINK']+'ajax_search_product',
		minLength: 2,
		change: function( event, ui ) {
			var product_id = ( ui.item != null ) ? ui.item.id : '';
			$(this).siblings(".product_id").val(product_id);
		}
	});
}//END function bind_product_autocomplete

function save_contract()  {
	$("#save_contract").click(function() {
		
		//get all product ids
		var products = new Array();
		$(".product_id").each(function() {
			var product_id = $(this).val();
			if( product_id != "" )
				products.push(product_id);
        });
		
		if( $("#ccnt_name").val() == "" )  {
			flashAauto('Please enter Contract Name.');
			return false;
		}
		
		if( $("#ccnt_id").val() == "" )  {
			flashAauto('Please enter Contract Code.');
			return false;
		}
		
		if( $("#ccnt_desc").val() == "" )  {
			flashAauto('Please enter Contract description.');
			return false;
		}
		
		//check validation
		if( products.length == 0 )  {
			flashAauto('Please select at least one product.');
			return false;
		}
		
		if( $("#contract_price").val() == "" )  {
			flashAauto('Please enter Discount % or Actual Price.');
			return false;
		}
		
		if( $("#start_date").val() == "" )  {
			flashAauto('Please select start date.');
			return false;
		}
		
		if( $("#end_date").val() == "" )  {
			flashAauto('Please select End date.');
			return false;
		}
		
		//save to database
		$("#product_ids").val(products.join(","));
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id,ccnt_name,ccnt_id,ccnt_desc,cnt_id,product_ids,contract_price,start_date,end_date','Mold_CM001:_save_contract','');
	});
}