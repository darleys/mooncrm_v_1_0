$(document).ready(function(e) {
	save_contract();
	bind_product_autocomplete(".product_category");
	
	//load datepicker - start and end date selection
	$( "#start_date" ).datepicker({
		dateFormat : 'yy-mm-dd',
		onClose: function( selectedDate ) {
		  $( "#end_date" ).datepicker( "option", "minDate", selectedDate );
		}
    });
	
    $( "#end_date" ).datepicker({
		dateFormat : 'yy-mm-dd',
		onClose: function( selectedDate ) {
		  $( "#start_date" ).datepicker( "option", "maxDate", selectedDate );
		}
    });
	
	//add contract product_category
    $("#add-product-category").click(function() {
		add_product();
		return false;
	});
	
	//remove contract product_category
	$("#product_category_list").on("click", '.remove-product-category', function() {
		$(this).parents("div:first").remove();
		return false;
	});
});

//add new product_category in product_category list
function add_product()
{
	//add new product_category in table
	var prodcut_str = '<div style="margin-top:5px;"><input type="text" class="product_category" /><input type="hidden" class="product_category_id" /> &nbsp; <a href="javasript:;" class="remove-product-category">Remove</a></div>';
	$("#product_category_list").append(prodcut_str);
	bind_product_autocomplete("#product_category_list .product_category:last");
}

function bind_product_autocomplete(obj)  {
	//bind autocomplete script to product_category input
	$(obj).autocomplete({
		source : CRMC_KEYS['LINK']+'ajax_search_product_category',
		minLength: 2,
		change: function( event, ui ) {
			var product_category_id = ( ui.item != null ) ? ui.item.id : '';
			$(this).siblings(".product_category_id").val(product_category_id);
		}
	});
}//END function bind_product_autocomplete

function save_contract()  {
	$("#save_contract").click(function() {
		
		//get all product_category ids
		var product_categories = new Array();
		$(".product_category_id").each(function() {
			var product_category_id = $(this).val();
			if( product_category_id != "" )
				product_categories.push(product_category_id);
        });
		
		if( $("#ccnt_name").val() == "" )  {
			flashAauto('Please enter Contract Name.');
			return false;
		}
		
		if( $("#ccnt_id").val() == "" )  {
			flashAauto('Please enter Contract Code.');
			return false;
		}
		
		if( $("#ccnt_desc").val() == "" )  {
			flashAauto('Please enter Contract description.');
			return false;
		}
		
		//check validation
		if( product_categories.length == 0 )  {
			flashAauto('Please select at least one product category.');
			return false;
		}
		
		if( $("#contract_price").val() == "" )  {
			flashAauto('Please enter Discount % or Actual Price.');
			return false;
		}
		
		if( $("#start_date").val() == "" )  {
			flashAauto('Please select start date.');
			return false;
		}
		
		if( $("#end_date").val() == "" )  {
			flashAauto('Please select End date.');
			return false;
		}
		
		//save to database
		$("#product_category_ids").val(product_categories.join(","));
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id,ccnt_name,ccnt_id,ccnt_desc,cnt_id,product_category_ids,contract_price,start_date,end_date','Mold_CM001:_save_contract','');
	});
}