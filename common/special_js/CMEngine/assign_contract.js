$(document).ready(function(e) {
	
	//get accounts associated with contract
	$("#contract_id").change(function() {
		var contract_id = $.trim( $("#contract_id").val() );
		
		$("#account_list").html('');
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','contract_id','Mold_CMEngine:_get_contract_accounts','');
	});
	
	//auto suggest account name
	$("#search_account").autocomplete({
		source: function (request, response) {
			$.get(CRMC_KEYS['LINK']+"ajax_search_account?ccnt_id="+$("#contract_id").val(), request, response, 'json');
		},
		minLength: 2,
		change: function( event, ui ) {
			var account_id = ( ui.item != null ) ? ui.item.id : '';
			$("#account_id").val(account_id);
			return false;
		}
	});
	
	//add account to contract
	$("#add_account").click(function() {
		var account_id = $("#account_id").val();
		var contract_id = $.trim( $("#contract_id").val() );
		
		if( contract_id != "" )  {
			if( account_id != "" )  {
				PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','contract_id,account_id','Mold_CMEngine:_add_contract_account','');
			}	else	{
				alert("Please select account.");
			}
		}	else	{
			alert("Please select contract.");
		}
	});
});

function save_end_date(inputID)
{
	var auto_id = $("#"+inputID).attr('rel');
	var contract_end_date = $("#"+inputID).val();
	
	$("#auto_id").val(auto_id);
	$("#contract_end_date").val(contract_end_date);
	
	PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','auto_id,contract_end_date','Mold_CMEngine:_save_end_date','');
}