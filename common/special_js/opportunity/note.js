$(document).ready(function(e) {
	save_note();
	
	//load note form
	$(".note_list_row").on( "click", 'a.note_form', function() {
		//note_form_data
		var note_id = $(this).attr('rel');
		var entity_id = $(this).parents("tr.note_list_row:first").find(".entity-id").val();
		var entity = $(this).parents("tr.note_list_row:first").find(".entity").val();
		
		$.ajax({
			url : 'note_form',
			data : { note_id : note_id, entity_id : entity_id, entity : entity  },
			type : 'POST',
			success : function(form_html) {
				$(".opportunity_"+entity_id+" .note_form_data").html(form_html);
			}
		});
	});
	
	//cancel note form
	$(".note_list_row").on('click', 'a.cancel-note-form', function() {
		$(".note_form_data").html('');
	});
	
	//delete note
	$(".note_list_row").on('click', 'a.delete-note', function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var note_id = $(this).attr('rel');
			$("#delete_note_id").val(note_id);
			
			PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','delete_note_id','Mold_Note:delete_note','');
			$(this).parents("tr:first").remove();
		}
	});
	
	//check attached file
	$(".note_list_row").on('change', '.notes_attach', function() {
		var file_detail = pathToFile($(this).val());
		var extension = file_detail.extension.toLowerCase();
		
		var allowed_extensions = ["txt", "pdf", "doc", "docx", "rtf", "xls", "xlsx", "ppt", "pptx", "jpg", "jpeg", "png", "gif"];
		if( allowed_extensions.indexOf(extension) > -1 )  {
			//file is valid
		}	else	{
			//file is not valid - clear file input
			var control = $(this);
			control.replaceWith( control = control.clone( true ) );
			alert("Please select valid file.");
		}
	});
	
	//add more attachment files in notes
	$(".note_list_row").on('click', '.add-note-attach', function() {
		var tdObj = $(this).parents("td:first");
		tdObj.append('<div class="margintop"><input type="file" name="notes_attach[]" class="notes_attach" /></div>');
	});
	
	//remove note file from database
	$(".note_list_row").on('click', '.remove-note-file', function() {
		$("#deletefilename").val($(this).attr('rel'));
		PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','note_id,deletefilename','Mold_Note:remove_note_file','');
		
		if( $(this).parent("div:first").hasClass("margintop") )  {
			$(this).parent("div:first").remove();
		}	else	{
			$(this).parent("div:first").html('<input type="file" name="notes_attach[]" class="notes_attach" /><a href="javascript:;" class="add-note-attach" style="margin-left:25px;">Add File</a>');
		}
	});
	
	//load note attachment in popup
	$(".note_list_row").on('click', '.load-note-file', function() {
		var filename = $(this).attr('title');
		var type = $(this).attr('rel');
		
		$("#dialog").html('<iframe src="note_attachment?file='+filename+'&type='+type+'" width="700" height="460" style="border:0px;"></iframe>');
		$( "#dialog" ).dialog({ title: filename, modal: true, width: 700, height: 500 });
	});
});

function save_note()  {
	//save note detail
	$(".note_list_row").on( "click", '.save-note', function() {
		var notes = $.trim( $("#notes").val() );
		
		if( notes == "" )  {
			flashAauto('Please enter notes.');
			return false;
		}
		
		//show preloader overlay
		$(".overlay").css('display', 'block');
		
		//used ajax because PROCESS_AJAX_REQ function can not handle file
		var formData = new FormData($("#note_file_form")[0]);
		$.ajax({
			url: "save_note",
			type: 'POST',
			data: formData,
			success: function (data) {
				$('.note_form_data').html('');
				eval(data);
				$(".overlay").css('display', 'none');
			},
			cache: false,
			contentType: false,
			processData: false
		});
		
		return false;
	});
}//END Function save_note