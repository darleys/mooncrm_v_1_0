$(document).ready(function(e) {
	save_product();
	
	//delete product
	$(".delete_product").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var product_id = $(this).attr('rel');
			$("#product_id").val(product_id);
			
			//delete from database
			PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','product_id','Mold_Product:delete_product','');
		}
	});
	
	//auto suggest product name
	$("#search_txt").autocomplete({
		source : CRMC_KEYS['LINK']+'ajax_search_product',
		minLength: 2,
		select: function( event, ui ) {
			$("#search_txt").val("["+ui.item.value+"]");
			//$("#search_form").submit();
			return false;
		}
	});
	
	//submit search form
	$("#search-products").click(function() {
		submitFormToCRMCAjax("products", "#search_form" );
	});
	
	//pagination link requests
	$(".products_pagination a").click(function() {
		submitFormToCRMCAjax($(this).attr('href'), "#search_form" );
		return false;
	});
	
	//clear search
	$("#reset-search").click(function() {
		$("#search_txt").val('');
		submitFormToCRMCAjax("products", "#search_form" );
	});
	
	///open export popup
	$(".export_popup").click(function() {
		var type = $(this).attr('rel');
		export_all(type);
		return false;
	});
});


function save_product()  {
	//save product detail
	$("#save_product").click(function() {
		var name = $.trim( $("#name").val() );
		var description = $.trim( $("#description").val() );
		var price = $.trim( $("#price").val() );
		var currency_code = $.trim( $("#currency_code").val() );
		
		if( name == "" )  {
			flashAauto('Please enter Product name.');
			return false;
		}
		
		if( description == "" )  {
			flashAauto('Please enter Product description.');
			return false;
		}
		
		if( price == "" )  {
			flashAauto('Please enter Price.');
			return false;
		}
		
		if( currency_code == "" )  {
			flashAauto('Please enter Currency code.');
			return false;
		}
		
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','product_id,name,description,currency_code,price,floor_price,target_price,categories','Mold_Product:_save_product','');
	});
}//END Function save_product

function export_all(type)  
{
	var export_ids = '', search_keyword = '';
	if( type == "selected" )  {
		//checkbox selected export
		var export_count = $(".checkbox_cls:checked").size();
		
		//get checked ids to export
		var exportIDs = new Array();
		$(".checkbox_cls:checked").each(function() {
			exportIDs.push($(this).val());
		});
		export_ids = exportIDs.join(",");
	}	else	{
		//all record exports - includes search filter
		var export_count = $("#total_num").val();
		search_keyword = $("#search_txt").val();
	}
	
	if( export_count > 0 )  {
		$("#export_ids").val(export_ids);
		$("#search_keyword").val(search_keyword);
		
		//open dialog popup
		$("#export_dialog").dialog({ modal: true, width: 450 });
	}	else	{
		alert("There is no products to export.");
	}
}//END Function export_all