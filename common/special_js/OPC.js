PROCESS_OPC_REQ = function(url,postApplyID) {
$.ajax({
    crossDomain:true,
    type: 'POST',
    url: url,
    dataType: "html"
}).done(function( html ) {
    postApplyID.html(html);
});
}//END Function Process_OPC_REQ

PROCESS_OPC_POST_REQ = function(url, data, postApplyID) {
    $.ajax({
        crossDomain:true,
        type: 'POST',
        url: url,
        data : data,
        dataType: "html"
    }).done(function( html ) {
        postApplyID.html(html);
    });
}//END Function PROCESS_OPC_POST_REQ