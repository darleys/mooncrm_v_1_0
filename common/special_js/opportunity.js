$(document).ready(function(e) {
	save_opportunity();
	
	//load datepicker
	$( "#expected_date_closure" ).datepicker({ dateFormat : 'yy-mm-dd' });
	
	//delete Opportunity
	$(".delete_opportunity").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var id = $(this).attr('rel');
			$("#id").val(id);
			
			//delete user group from database
			PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id','Mold_Opportunity:_delete_opportunity','');
		}
	});
	
	//hide and show note list
	$(".opportunity_row td.openNotes").click(function() {
		var trObj = $(this).parent();
		
		//hide all note list
		$(".note_list_row").hide();
		
		if( !trObj.hasClass('open') )  {
			$(".opportunity_row").removeClass('open')
			
			//add open class and show note list
			var idCls = trObj.attr('id');
			$("."+idCls).show('slow');
			trObj.addClass('open');
		}	else	{
			//remove open class to open note list again
			$(".opportunity_row").removeClass('open')
		}
	});
	
	//submit search form
	$("#search-opportunities").click(function() {
		$("#search_form").submit();
	});
	
	//clear search
	$("#reset-search").click(function() {
		$("#search_txt").val('');
		$("#search_form").submit();
	});
	
	///open export popup
	$(".export_popup").click(function() {
		var type = $(this).attr('rel');
		export_all(type);
		return false;
	});
});

function save_opportunity()  {
	//save Opportunity detail
	$("#save_opportunity").click(function() {
		var name = $.trim( $("#name").val() );
		var value = $.trim( $("#value").val() );
		
		if( name == "" )  {
			flashAauto('Please enter name.');
			return false;
		}
		
		if( value == "" )  {
			flashAauto('Please enter value.');
			return false;
		}
		
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id,name,value,description,expected_date_closure,status,probability','Mold_Opportunity:_save_opportunity','');
	});
}//END Function save_opportunity

function export_all(type)  
{
	var export_ids = '', search_keyword = '';
	if( type == "selected" )  {
		//checkbox selected export
		var export_count = $(".checkbox_cls:checked").size();
		
		//get checked ids to export
		var exportIDs = new Array();
		$(".checkbox_cls:checked").each(function() {
			exportIDs.push($(this).val());
		});
		export_ids = exportIDs.join(",");
	}	else	{
		//all record exports - includes search filter
		var export_count = $("#total_num").val();
		search_keyword = $("#search_txt").val();
	}
	
	if( export_count > 0 )  {
		$("#export_ids").val(export_ids);
		$("#search_keyword").val(search_keyword);
		
		//open dialog popup
		$("#export_dialog").dialog({ modal: true, width: 450 });
	}	else	{
		alert("There is no opportunities to export.");
	}
}//END Function export_all