
$(function() {




    $("#datepicker2").datepicker({
        numberOfMonths: 1,
        showOn: 'button',
        buttonText: '&lt;i class="fa fa-calendar"&gt;&lt;/i&gt;',
        prevText: '&lt;i class="fa fa-chevron-left"&gt;&lt;/i&gt;',
        nextText: '&lt;i class="fa fa-chevron-right"&gt;&lt;/i&gt;',
        showButtonPanel: true
    });

    $(".inline-calender").datepicker({
        numberOfMonths: 1,
        minDate: -20,
        maxDate: "+1M -10D",
        prevText: '&lt;i class="fa fa-chevron-left"&gt;&lt;/i&gt;',
        nextText: '&lt;i class="fa fa-chevron-right"&gt;&lt;/i&gt;'
    });

});

