// toolBox plugin
jQuery.fn.toolBox = function(_options){
    var _options = jQuery.extend({
        targetElements: 'li',
        toolBox: '.junk',
        activeClass:'toolBox-active',
        body:'#main-body',
        tevent:'hover',
        diffElse: 5,
        diffTop: -20,
        diffLeft: 0,
        onMouseLeft:0,
        onMouseTop:0
    },_options);
    return this.each(function(){
        var holder = $(this);
        var targetElements = holder.find(_options.targetElements);
        var toolBox = $(_options.toolBox);
        var body = 	_options.body;
        var tevent =_options.tevent;
        $(body).append(toolBox);
        toolBox.css("position","absolute");
        var activeClass = _options.activeClass;
        var _diffTop = _options.diffTop;
        var _diffLeft = _options.diffLeft;
        var _diffElse = _options.diffElse;
        var onMouseLeft = _options.onMouseLeft;
        var onMouseTop = _options.onMouseTop;
        var timer;

        targetElements.each(function(){
            //prevTarget.removeClass(activeClass);
            var cur = $(this);



            var onEvent = function(e){
                cur.addClass(activeClass);
                toolBox.css("display","block");
                var offsetLeft = cur.offset().left;
                var offsetTop = cur.offset().top;
                var holderWidth = cur.outerWidth();
                if($(window).width() < offsetLeft + holderWidth + toolBox.outerWidth()){
                    holderWidth = -toolBox.outerWidth() + _diffElse;
                }
                prevTarget=$(this);
                clearTimeout(timer);

                if(onMouseLeft || onMouseTop) {
                    toolBox.css("left",e.pageX+parseInt(onMouseLeft));
                    toolBox.css("top",e.pageY+parseInt(onMouseTop));
                }else
                    toolBox.css({left:offsetLeft + holderWidth + _diffLeft,top:offsetTop+_diffTop});
            };
            var outEvent = function(e){
                timer = setTimeout(function(){
                    targetElements.removeClass(activeClass);
                    toolBox.css({left:-99999,top:-99999});
                },100);
            };

            //START Check for Mouse Event set in the Option
            if(tevent == "hover") {
                cur.hover(onEvent,outEvent);
            }
            if(tevent == "right") {
                cur.bind('contextmenu', function(){ return false });
                cur.mousedown(function(e) {
                    if(e.button == 2) {
                        $(onEvent(e));
                    }
                });
                cur.hover("",outEvent);
            }
            //END Check for Mouse Event set in the Option


        });

        toolBox.hover(function(){
            clearTimeout(timer);
        },function(){
            if ($.browser.msie && $.browser.version == 6) {
                $('select').css({
                    visibility: 'visible'
                });
            }
            targetElements.removeClass(activeClass);
            toolBox.css({left:-99999,top:-99999});

        });

    });
}
//END ToolBox Plugin
$(document).ready(function(e) {
var overlayCOM=0;
var overlayCOM_disp=0;
var dialogMask="<div class='dialogMask'></div>";
var endOverlay=0;
    $('.overlay').click(function(e) {
        itrans();
    });
    function callOver(){
        endOverlay();
    }
});

//START CODE FOR trans alpha
function trans(alpha,poverlayCOM) {
    overlayCOM=poverlayCOM;
    overlayCOM.addClass("place-center wht-bg col-md-2");
    //alert(overlayCOM.parent().index(overlayCOM));
    //
    //$('.dialogMask').replaceWith(overlayCOM);
    //overlayCOM.replaceWith(dialogMask);
    //overlaC
    $('.dialogMask').insertAfter(overlayCOM);
    //overlayCOM_clone = overlayCOM.clone();
    overlayCOM.appendTo(".overlayCOM");
    overlayCOM_disp=overlayCOM.css("display");
    overlayCOM.css("display","block");
    $('.trans').css({
        '-ms-filter': "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)",
        'filter' : "alpha(opacity="+(alpha*100)+")",
        '-moz-opacity': alpha,
        '-khtml-opacity': alpha,
        'opacity': alpha
    });
    $('.overlay').fadeToggle();
    $('.overlayCOM').fadeToggle();
}//END Function trans

function itrans() {
    $('.overlay').fadeToggle();
    overlayCOM.removeClass("place-center wht-bg col-md-2");
    overlayCOM.insertAfter($('.dialogMask'));
    $('.overlayCOM').append($('.dialogMask'));
    $('.overlayCOM').children().not('.dialogMask').remove();
    overlayCOM.css("display",overlayCOM_disp);
    $('.overlayCOM').fadeToggle();
}//END Function trans
//END CODE FOR trans alpha


//Start  Dialog Box plugin
jQuery.fn.dialogB = function(_options){
    var _options = jQuery.extend({
        body:'body',
        tevent:'click',
        position : 'center',
        dmsg:'',
        trueBTxt :'OK',
        falseBTxt :'Cancel',
        onSuccess : '',
        onFailure : ''
    },_options);

        var holder = $(this);
        var body = 	$(_options.body);
        var tevent = 	_options.tevent;
        var dmsg= 	_options.dmsg;
        var trueBTxt = 	_options.trueBTxt;
        var falseBTxt = 	_options.falseBTxt;
        var onSuccess = 	_options.onSuccess;
        var onFailure = 	_options.onFailure;
        var position = _options.position;
            if(tevent=='click') {
                $(this).click(function(){
                    createDialog();
                });
            }
            if(tevent=='show') {
                createDialog();
            }
            function createDialog() {
                var tdialog=$("<div class='ppp' style='background: #f3ab00'>" +
                    "<p>"+dmsg+"</p>"+
                    "<button id='dialogB0' >" +trueBTxt+
                    "</button><button id='dialogB1' >"+falseBTxt+"</button></div>");
                tdialog.appendTo(body);
                tdialog.css({
                    position:"absolute",
                    display:"none",
                    top: '50%',
                    left: '50%',
                    zindex:'5000'
                });
                tdialog.show();
            }
            $(document).on('click','#dialogB0',function() {
                onSuccess();
                $(this).parent().remove();
            });
            $(document).on('click','#dialogB1',function() {
                onFailure();
                $(this).parent().remove();
            });


}
//End  Dialog Box plugin