//show edit popup and save data into template
(function($){$.fn.saveEdit=function(options){
    var settings = options;
	var dialogBoxID = "#saveEditDialog";
	
    $(this).bind(settings.event,function(e){
        theTarget = $(this);
		
		//hide all inputs and show only relative input
		$("#saveEditDialog .edit_inputs").css('display', 'none');
		$(settings.field).parent().css('display', 'block');
		
		//get value from html and set in input
		var data = $.trim($(this).html());
		if(settings.type == "textarea")  {
			data = data.replace(/(?:<br>|<br\/>|<br \/>)/g, '\n');
		}
		$(settings.field).val(data);
		
		//if input field is image than set recommended size
		if(settings.type == "image")  {
			$("#img_width").val($(this).width());
			$("#img_height").val($(this).height());
			$("#recommended_image_size").html("Recommended Size : " + $(this).width() + " x " + $(this).height());
		}
		
		//open popup for update data
		$(dialogBoxID).dialog({ modal: true, width: 350  });
		
		//close update data popup
        $(".close-edit").off().on('click',function(e) {
            $( dialogBoxID ).dialog( "close" );
        });
		
		//update data in html and do necessary action here
        $("#update-data").off().on('click',function(e) {
			if(settings.type == "image")  {
				//check for image filetype
				var file_detail = pathToFile($("#imageField").val());
				var allowed_extensions = ["jpg", "jpeg", "png", "gif"];
				
				var extension = file_detail.extension.toLowerCase();
				if( allowed_extensions.indexOf(extension) < 0 )  {
					alert("Please select valid file.");
					return false;
				}
				
				//upload image file
				var formData = new FormData($("#template_edit_form")[0]);
				$.ajax({
					url: CRMC_KEYS['LINK']+"upload_image",
					type: 'POST',
					data: formData,
					success: function (response) {
						response = $.parseJSON(response);
						if( response.error )  {
							alert(response.error);
						}	else	{
							var src_path = CRMC_KEYS['LINK'] + response.file_path;
							$(theTarget).attr('src', src_path).attr('rel', response.file_path);
						}
					},
					cache: false,
					contentType: false,
					processData: false
				});
			}	else	{
				//input and text area
				var newContent = $(settings.field).val();
				if(settings.type == "textarea")  {
					newContent = newContent.replace(/(?:\r\n|\r|\n)/g, '<br />');
				}
				$(theTarget).html(newContent);
			}
			
			//close popup
			$( dialogBoxID ).dialog( "close" );
        });
    });
};})(jQuery);

$(document).ready(function() {
	//delete template
	$(".delete_template").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var id = $(this).attr('rel');
			$("#id").val(id);
			
			//delete user group from database
			PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id','Mold_TemplateEngine:_delete_template','');
		}
	});
	
	//select template
	$("#templateType").change(function() {
		var cmrc_url = "create_template?type=" + $(this).val();
        PROCESS_CRMC_REQ(CRMC_KEYS['LINK'] + cmrc_url,$( ".body-content" ));
	});
	
	//generate pdf
	$("#generate_pdf").click(function () {
		$("#pdfHTML").val($("#template").html());
		$("#generate_pdf_form").submit();
	});
	
	//edit input field
	$('.inputText').saveEdit({
		event:'dblclick',
		type : 'input',
		field : '#inputField'
	});
	
	//edit textarea field
	$('.inputTextarea').saveEdit({
		event:'dblclick',
		type : 'textarea',
		field : '#textareaField'
	});
	
	//edit image field
	$('.inputImage').saveEdit({
		event:'dblclick',
		type : 'image',
		field : '#imageField'
	});
	
	//show edit box when clicked on edit_icon button
	$(".edit_icon").click(function() {
		$(this).siblings(".inputText, .inputTextarea, .inputImage").trigger('dblclick');
	});
	
	//save template into database
	$("#save-template").click(function() {
		//check if name is entered
		var templateName = $("#templateName").val();
		if( templateName == "" )  {
			flashAauto('Please enter Template Name.');
			return false;
		}
		
		//add template html in textarea value
		$("#pdfHTML").val($("#template").html());
		
		//get all image paths used in html to copy images
		var imagePaths = new Array();
		$(".inputImage").each(function() {
			var imagePath = $(this).attr('src');
            imagePaths.push(imagePath);
        });
		$("#copyImagePaths").val( JSON.stringify(imagePaths) );
		
		//save template
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','templateName,pdfHTML,templateType,copyImagePaths','Mold_TemplateEngine:_save_template','');
	});
	
	//update template data
	$("#edit-template").click(function() {
		//check if name is entered
		var templateName = $("#templateName").val();
		if( templateName == "" )  {
			flashAauto('Please enter Template Name.');
			return false;
		}
		
		//add template html in textarea value
		$("#pdfHTML").val($("#template").html());
		
		//get all image paths used in html to copy images
		var imagePaths = new Array();
		$(".inputImage").each(function() {
			var imagePath = $(this).attr('src');
            imagePaths.push(imagePath);
        });
		$("#copyImagePaths").val( JSON.stringify(imagePaths) );
		
		//save template
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','id,templateName,pdfHTML,copyImagePaths','Mold_TemplateEngine:_edit_template','');
	});
	
	change_img_path_to_crmc();
});

function change_img_path_to_crmc()  {
	//make all image path to crmc server
	$(".inputImage").each(function() {
		var src = $(this).attr('src');
		$(this).attr('rel', src);
		
		var new_src = CRMC_KEYS['LINK'] + src;
		$(this).attr('src', new_src);
	});
}