$(document).ready(function(e) {
	save_user_group();
	
	//delete user group
	$(".delete_user_group").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var group_id = $(this).attr('rel');
			$("#group_id").val(group_id);
			
			//delete user group from database
			PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','group_id','Mold_User_group:delete_user_group','');
		}
	});
	
	//add new product row in product list
	$("#add_module").click(function() {
		add_module();
	});
	
	//Remove module
	$("#module_list").on( "click", 'a.remove_module', function() {
		$(this).parents("tr:first").remove();
	});
	
	//bind auto complete and add 1 new module
	bind_auto_complete("#module_list input.module_search");
	add_module();
	
	//search for user group
	$("#search_keyword_group").on('keyup', function() {
		PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','search_keyword_group','Mold_User_group:search_user_groups','');
	});
});


function save_user_group()  {
	//save user group detail
	$("#save_user_group").click(function() {
		//check if username and password entered
		var title = $.trim( $("#title").val() );
		var description = $.trim( $("#description").val() );
		
		if( title == "" )  {
			flashAauto('Please enter title.');
			return false;
		}
		
		if( description == "" )  {
			flashAauto('Please enter description.');
			return false;
		}
		
		//get all module's id
		var moduleIDs = new Array();
		$("#module_list .module_id").each(function(index) {
			moduleIDs[index] = $(this).val();
        });
		$("#module_ids").val( moduleIDs.join(',') );
		
		//get all module's permission
		var modulePermissions = new Array();
		$("#module_list .module_permission").each(function(index) {
			modulePermissions[index] = $(this).val();
        });
		$("#module_permissions").val( modulePermissions.join(',') );
		
		PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','group_id,title,description,module_ids,module_permissions','Mold_User_group:save_user_group','');
	});
}//END Function save_user_group


//add new module for add to group
function add_module()
{
	//add new module in table
	var module_str = '<tr>';
	module_str += '<td><input type="text" class="module_search" /><input type="hidden" class="module_id" value="" /></td>';
	module_str += '<td><select class="module_permission"><option value="ADMIN">ADMIN</option><option value="WRITE">WRITE</option><option value="READ">READ</option></select></td>';
	module_str += '<td><a href="javascript:;" class="remove_module">Remove</a></td>';
	module_str += '</tr>';
	$("#module_list").append(module_str);
	
	bind_auto_complete("#module_list tr:last input.module_search");
}

function bind_auto_complete( element )
{
	//bind autocomplete script to product input
	$(element).autocomplete({
		source : 'ajax_search_module',
		minLength: 2,
		select: function( event, ui ) {
			//assigning value to hidden input module_id
			var tdObj = $(this).parent("td");
			tdObj.find(".module_id").val(ui.item.id);
		}
	});
}