$(document).ready(function(e) {
	save_contact();
	
	//delete contact
	$(".delete_contact").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var contact_id = $(this).attr('rel');
			$("#id").val(contact_id);
			
			//delete from database
			PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','id','Mold_Contact:delete_contact','');
		}
	});
	
	//auto suggest for account
	$("#search_account").autocomplete({
		source: 'ajax_search_account',
		minLength: 2,
		change: function( event, ui ) {
			var account_id = ( ui.item != null ) ? ui.item.id : '';
			$("#account_id").val(account_id);
			return false;
		}
	});
	
	//submit search form
	$("#search-contacts").click(function() {
		$("#search_form").submit();
	});
	
	//clear search
	$("#reset-search").click(function() {
		$("#search_txt").val('');
		$("#search_form").submit();
	});
	
	///open export popup
	$(".export_popup").click(function() {
		var type = $(this).attr('rel');
		export_all(type);
		return false;
	});
});

function save_contact()  {
	//save contact detail
	$("#save_contact").click(function() {
		var account_id = $.trim( $("#account_id").val() );
		var first_name = $.trim( $("#first_name").val() );
		var last_name = $.trim( $("#last_name").val() );
		var email = $.trim( $("#email").val() );
		
		if( account_id == "" )  {
			flashAauto('Please select Account.');
			return false;
		}
		
		if( first_name == "" )  {
			flashAauto('Please enter First name.');
			return false;
		}
		
		if( last_name == "" )  {
			flashAauto('Please enter Last name.');
			return false;
		}
		
		if( !validateEmail(email) )  {
			flashAauto('Please enter valid Email address.');
			return false;
		}
		
		PROCESS_AJAX_REQ('request'+THE_KEYS["MASQUERADE"]+'processor','id,account_id,first_name,last_name,title,email,primary_contact,addressline1,addressline2,city,state,country,postal_code','Mold_Contact:save_contact','');
	});
}//END Function save_contact

function export_all(type)
{
	var export_ids = '', search_keyword = '';
	if( type == "selected" )  {
		//checkbox selected export
		var export_count = $(".checkbox_cls:checked").size();
		
		//get checked ids to export
		var exportIDs = new Array();
		$(".checkbox_cls:checked").each(function() {
			exportIDs.push($(this).val());
		});
		export_ids = exportIDs.join(",");
	}	else	{
		//all record exports - includes search filter
		var export_count = $("#total_num").val();
		search_keyword = $("#search_txt").val();
	}
	
	if( export_count > 0 )  {
		$("#export_ids").val(export_ids);
		$("#search_keyword").val(search_keyword);
		
		//open dialog popup
		$("#export_dialog").dialog({ modal: true, width: 450 });
	}	else	{
		alert("There is no contacts to export.");
	}
}//END Function export_all