$(document).ready(function(e) {
	save_product_category();
	
	//delete product_category
	$(".delete_product_category").click(function() {
		if( confirm('Are you sure to delete this record?') )
		{
			var product_category_id = $(this).attr('rel');
			$("#product_category_id").val(product_category_id);
			
			//delete from database
			PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','product_category_id','Mold_Product_Category:_delete_product_category','');
		}
	});
	
	//search for users
	$(".search_category").autocomplete({
		source : CRMC_KEYS['LINK']+'ajax_search_product_category',
		minLength: 2,
		select: function( event, ui ) {
			var callback = $(this).attr('data-callback');
			
			//user form report to auto complete
			if( callback == "product_category_form" )  {
				$(this).siblings("#parent_category").val(ui.item.id);
			}
		}
	});
	
	//auto suggest product category name
	$("#search_txt").autocomplete({
		source : CRMC_KEYS['LINK']+'ajax_search_product_category',
		minLength: 2,
		select: function( event, ui ) {
			$("#search_txt").val("["+ui.item.value+"]");
			//$("#search_form").submit();
			return false;
		}
	});
	
	//submit search form
	$("#search-product-categories").click(function() {
		submitFormToCRMCAjax("product_categories", "#search_form" );
	});
	
	//pagination link requests
	$(".product_categories_pagination a").click(function() {
		submitFormToCRMCAjax($(this).attr('href'), "#search_form" );
		return false;
	});
	
	//clear search
	$("#reset-search").click(function() {
		$("#search_txt").val('');
		submitFormToCRMCAjax("product_categories", "#search_form" );
	});
	
	///open export popup
	$(".export_popup").click(function() {
		var type = $(this).attr('rel');
		export_all(type);
		return false;
	});
});

function save_product_category()  {
	//save product_category detail
	$("#save_product_category").click(function() {
		var category_name = $.trim( $("#category_name").val() );
		var category_desc = $.trim( $("#category_desc").val() );
		
		if( category_name == "" )  {
			flashAauto('Please enter Category name.');
			return false;
		}
		
		if( category_desc == "" )  {
			flashAauto('Please enter Category description.');
			return false;
		}
		
		PROCESS_AJAX_REQ(CRMC_KEYS['LINK']+'request'+THE_KEYS["MASQUERADE"]+'processor','product_category_id,category_name,category_desc,parent_category','Mold_Product_Category:_save_product_category','');
	});
}//END Function save_product_category

function export_all(type)
{
	var export_ids = '', search_keyword = '';
	if( type == "selected" )  {
		//checkbox selected export
		var export_count = $(".checkbox_cls:checked").size();
		
		//get checked ids to export
		var exportIDs = new Array();
		$(".checkbox_cls:checked").each(function() {
			exportIDs.push($(this).val());
		});
		export_ids = exportIDs.join(",");
	}	else	{
		//all record exports - includes search filter
		var export_count = $("#total_num").val();
		search_keyword = $("#search_txt").val();
	}
	
	if( export_count > 0 )  {
		$("#export_ids").val(export_ids);
		$("#search_keyword").val(search_keyword);
		
		//open dialog popup
		$("#export_dialog").dialog({ modal: true, width: 450 });
	}	else	{
		alert("There is no product categories to export.");
	}
}//END Function export_all