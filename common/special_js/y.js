$(document).ready(function(){
	flashA = function(conts) {
		$('body').append("<div id='flashA' class='flashA' onClick='closethis(\"flashA\")'>"+conts+"</div>");
	}
	
	flashAauto = function(conts) {
		$('body').append("<div id='flashA' class='flashA' onClick='closethis(\"flashA\")'>"+conts+"</div>");
        setTimeout(function(e) {
            $("#flashA").slideUp("slow",function() {
                $('#flashA').remove();
            });
        },2500);
	}
	
	$("#flashA").click(function () {
		$("#flashA").slideUp("slow",function() {
            $('body').remove("#flashA");
        });
	});
	
	closethis = function(id,timer) {		
		$("div#"+id).slideUp("slow",function() {
            $('body').remove("#flashA");
        });
	}
	
	var flashATimeOut;
	flashMessage = function(selector, msg) {
		$(selector).append("<div class='flashA  alert alert-danger' onClick='closethis1(this)'>"+msg+"</div>");
		
		clearTimeout(flashATimeOut);
		flashATimeOut = setTimeout(function(e) {
            $(selector+' .flashA').slideUp("slow",function() {
                $(selector+' .flashA').remove();
            });
        },2500);
	}
	
	closethis1 = function(obj) {
		$(obj).slideUp("slow",function() {
			$(obj).remove();
        });
	}
});