<?php
/**
 * Foundry
 *
 * LICENSE
 *
Copyright 2013 Virtuous Consulting Services

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 * @copyright  Copyright (c) 2013 Virtuous Consulting Services.  (http://www.virtuouscs.com)
 * @Owner     Darley Stephen (http://www.darleys.org)
 * @Author     Darley Stephen (http://www.darleys.org)
 */
class Department extends Foundry_Local_Broker{
    
	function department_list() {
        mold_sae::GA();
		//Mold_user::checkLoginSession();
		mold_department::get_departments();
		
        include_once CORE_PATH . DS . 'casts/header.php';
		$this->display (TEMPLATES_DIR.DS.'common/menu.tpl');
        $this->display (TEMPLATES_DIR.DS.'department/department_list.tpl' );
		
		Foundry_Memory::dealloc('departments');
    }//END department_list

    function manage_departments() {
        $iActions = mold_actions::extActions("DEPARTMENTS");
        //Check whether the User has access to Managing Departments (iActions ID == 46)
        if(mold_ac::validateAccess(46,$iActions)) {
            $data['iActions']=$iActions;
            $data['departments_list'] =mold_department::_departments_list();
            $this->assign($data);
            $this->display (TEMPLATES_DIR.DS.'department/department_list.tpl' );
        }//END IF for iActions
    }//END Functon manage departments

    function manage_department_roles() {
        $iActions = mold_actions::extActions("DEPARTMENTS");
        if(mold_ac::validateAccess(49,$iActions)) {
            $data['iActions']=$iActions;
            $this->assign($data);
            $this->display (TEMPLATES_DIR.DS.'department/department_list.tpl' );
        }//END IF for iActions
    }//END Functon manage department roles
	
	function department_form()
    {
		$department_id = $_GET['id'];
		Foundry_Memory::set('department_id', $department_id);

		//get department detail
		if( $department_id != "" )  {
			Mold_department::department_detail($department_id);
		}

        $this->display (TEMPLATES_DIR.DS.'department/department_form.tpl' );

		//deallocate variables
		Foundry_Memory::dealloc('department_id');
		Foundry_Memory::dealloc('department_detail');
    }//END department_form


	
	function search_department()  {
		$keyword = $_REQUEST['term'];
		Mold_department::search_department($keyword);
	}//END search_department
	
}//END Department
