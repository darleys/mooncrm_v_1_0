<?php
/**
 * Foundry
 *
 * LICENSE
 *
Copyright 2010 Virtuous Consulting Services

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 * @copyright  Copyright (c) 2013 Virtuous Consulting Services.  (http://www.virtuouscs.com)
 * @Owner     Darley Stephen (http://www.darleys.org)
 * @Author     Darley Stephen (http://www.darleys.org)
 */
//$header =  base_loader::loadBaseCss();
//$header .=  base_loader::loadSpecialCss();
//$header .=  base_loader::loadBaseJs();
//$header .=  base_loader::loadSpecialJs();
Optimizer::psLNKs(array("bootstrap/1","moon_dash","style-responsive"),"CSS",0);
Optimizer::psLNKs(array("font-awesome/1"),"CSS",1);
Optimizer::psLNKs(array("jquery"),"JS",0);
Optimizer::psLNKs(array("bootstrap/1","jquery_ui","keys","gears1","dealUI","common","moon","x","y","z"),"JS",1);
$this->assign("header",$header);