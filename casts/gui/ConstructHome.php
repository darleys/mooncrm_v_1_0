<?php
class ConstructHome extends Foundry_Local_Broker{
    public function doALL($head,$divHeaderCont,$divMainCont) {
        $this->doHead($head);
        $this->doDivHeaderCont($divHeaderCont);
        $this->doDivMainCont($divMainCont);

        //return $topHTML;
    }//END Function do ALL

    function doHead($head) {

$sOut = <<<HTML
            <html>
            <head>
HTML;

        if(count($head) != 0) {
            foreach($head as $headTagsName => $headTagsValue) {
                switch($headTagsName) {
                    case "title" :
                        $sOut.=unserialize($headTagsValue);
                        break;
                    case "meta" :
                            $sOut.=unserialize($headTagsValue);
                        break;
                    case "link" :
                        $sOut.=unserialize($headTagsValue);
                        break;
                    case "script" :
                        $sOut.=unserialize($headTagsValue);
                        break;
                }//END Switch for head tags

            }//END FOR for each head tags
$sOut .= headers::irackHead1();
$sOut .= <<<HTML
            <script language='javascript'>
                $(function() {
                    initIrackIt();
                });
            </script>
            </head>
HTML;
echo $sOut;
        }else {

$sOut = <<<HTML
            <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>
            <head>
                <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
                <title>IRackIt - Rack your stuff!</title>
                <meta name="description" content="IRackIt is the only place where you can upload & share Videos, Photos, Documents, Music, Images & Audio . It provides a facility to view any media type according to your choice which are categorized as Rack Types. There are five Racks - General (all), Document, Image, Audio & Video">
                <?php echo headers::irackHead1();?>
            <script language='javascript'>
                $(function() {
                    initIrackIt();
                });
            </script>
            </head>
HTML;
echo $sOut;

        }//END ELSE FOR EMPTY HEAD
    }//END do Head

    function doDivHeaderCont($divHeaderCont) {
        if(count($divHeaderCont) != 0) {

        }else {
$sOut = <<<HTML
        <body>
        <!-- START header-content -->
        <div class='header-content'>
HTML;
echo $sOut;

            if(Foundry_Memory::isThere('mymy')){
                $mymy = Foundry_Memory::get('mymy');
            }

            if(!Foundry_Memory::isThere('myrack')) {
                 $_SESSION['myrack'][0]=0;
            }
            $activeRacks = Foundry_Memory::get('myrack');
            $racks = gear1::getRacks();
            $i =0;
            echo "<a href='".MY_URL."' class='logo'><img src='images/logo.png'></a>
                <ul class='rackTypes'>";
            foreach($racks as $rack){
                if(in_array($i,$activeRacks))
                    $class = "active";
                else
                    $class = 'inactive';

                echo html::a('rack_'.$rack->rackid,'javascript:void(0)',array("class"=>$class,"onclick"=>"PROCESS_AJAX_REQ('/setMYRCK(".$rack->rackid.")@managerack','','','')"),"<li>  ".$rack->rack_desc." Rack"." |</li>");


                $i++;
            }
            echo "</ul>";
            echo "<div class='searchC'>";
            echo form::me("searchFC","/search","get","",'');
            echo "<div class='searchTC'>";
            echo "<input type='text' class='search irackit-1-tbox' id='search-text' name='search-text' value='".$_REQUEST['search-text']."'>";
            echo "</div>";
            echo "<div class='searchBC'>";
            echo "<div type='submit' value='Search' class='searchMedia searchB irackit-1-button'>Search</div>";
            echo "</div>";
            echo form::me_;
            echo "</div>";
            echo "<div class='gearsC'>
                <span class='gears signup-link'>Sign Up |</span>
                ";
            if($mymy['myname']) {
                echo "<span class='mytools-l gears'>[".substr($mymy['myname'],0,20)."]</span>  | ";
                echo html::a('rack_'.$rack->rackid,'javascript:void(0)',array("class"=>"gears","onclick"=>"PROCESS_AJAX_REQ('/request::processor','','mymy:logOut','')"),"logout");
                echo "<div class='mytools irackit-3-panel'>
                    <div class='organize-l'><a href='http://irackit.com/organize?rackid=1'>Organize</a></div>
                    <div class='racknow-l'><a href='http://irackit.com/racknow'>Racknow</a></div>
                </div>";
            }else {
                echo "<span class='gears signin-link'>Upload | </span>";
                echo "<span class='gears signin-link'>Sign In | </span>";
            }
            echo "</div>";


$sOut = <<<HTML
            <!-- END header-content -->
            <!-- START main-content -->

        </div>

HTML;
            echo $sOut;
        }//EMD ELSE FOR EMPTY DIV HEADER CONT
    }//END doDIVHeaderCont

    function doDivMainCont($divMainCont) {
        if(count($divMainCont) != 0) {

        }else {
$sOut = <<<HTML
<div class="main-content">
HTML;
echo $sOut;
        }//END ELSE FOR EMPTY DIV MAIN CONT
    }//END function doDIVMainCont

}
