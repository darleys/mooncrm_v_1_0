<?php
/**
 * Created by PhpStorm.
 * User: STEPHD
 * Date: 5/16/14
 * Time: 2:40 PM
 */

class Headers extends Foundry_Local_Broker {
    function nullHeader() {
        base_loader::loadBaseJs();
        base_loader::loadSpecialJs();
    }//END Function Null Header
    function Type1() {
        $rVal=Foundry_Memory::get("header_type_1");
        if( count($rVal)<=0) {
            $rVal.=base_loader::loadBaseCss();
            $rVal.=base_loader::loadSpecialCss();
            $rVal.=base_loader::loadBaseJs();
            $rVal.=base_loader::loadSpecialJs();
            Foundry_Memory::set("header_type_1",$rVal);
        }
        return $rVal;
        //headers::headerA();
    }//END Function for Header type 1
}//End Class for formating the Headers
