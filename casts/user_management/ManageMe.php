<?php
/**
 * Foundry
 *
 * LICENSE
 *
Copyright 2013 Virtuous Consulting Services

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 * @copyright  Copyright (c) 2013 Virtuous Consulting Services.  (http://www.virtuouscs.com)
 * @Owner     Darley Stephen (http://www.darleys.org)
 * @Author     Darley Stephen (http://www.darleys.org)
 */
class ManageMe extends Foundry_Local_Broker{
    function first_entry() {
        include_once CORE_PATH . DS . 'casts/header.php';
        //$this->display (TEMPLATES_DIR.DS.'first_entry.tpl');
        $this->display (TEMPLATES_DIR.DS.'mymy.tpl' );
    }

	function login() {
        include_once CORE_PATH . DS . 'casts/header.php';
        //echjo
        $this->display (TEMPLATES_DIR.DS.'login.tpl');
    }

    function logout() {
        Mold_User::logOut();
    }

    function logme($request) {
        json_decode(rawurldecode(base64_decode(Foundry_Curl::doCurl("/fetchImages@mold_images"))),true);
    }
	
    function funcCall($funcName) {
        $this->{$funcName}();
    }
	
    function getImages() {
        //$imageRES = json_decode(rawurldecode(base64_decode(Foundry_Curl::doCurl(PROCESS_IMAGES_LINK."/fetchImages@mold_images"))),true);

        mold_images::fetchImages();
        include_once CORE_PATH . DS . 'casts/header.php';
        $this->display (TEMPLATES_DIR.DS.'first.tpl');
        //include_once CORE_PATH.DS.'casts/footer.php';
    }//END Function getImages
	
	/* Maulik code start */
	function loginCheck() {
        echo "Yyy";
        echo base64_decode($_REQUEST['REQP']);
        echo $_REQUEST['username'];
        echo $_REQUEST['password'];
        echo "www";
		$username = mysql_escape_string($_POST['username']);
		$password = $_POST['password']; //do not escape password as it is going to be md5
		
		mold_user::logIn($username,$password);
	}
	
	function dashboard()	{
        mold_sae::GA();
        Modules::module_menu();
	}//END Function Dashboard

    function administration(){
        mold_sae::GA();
        Modules::administration_menu();
    }//END Function Dashboard

    function workshop(){
        mold_sae::GA();
        Modules::workshop_menu();
    }//END Function Dashboard





}//END ManageMe
