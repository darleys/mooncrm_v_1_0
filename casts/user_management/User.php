<?php
/**
 * Foundry
 *
 * LICENSE
 *
Copyright 2013 Virtuous Consulting Services

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 * @copyright  Copyright (c) 2013 Virtuous Consulting Services.  (http://www.virtuouscs.com)
 * @Owner     Darley Stephen (http://www.darleys.org)
 * @Author     Darley Stephen (http://www.darleys.org)
 */
class User extends Foundry_Local_Broker{
    
	function user_list() {

		Mold_user::get_users($type);
		
        include_once CORE_PATH . DS . 'casts/header.php';
		$this->display (TEMPLATES_DIR.DS.'common/menu.tpl');
        $this->display (TEMPLATES_DIR.DS.'user_list.tpl' );
		
		Foundry_Memory::dealloc('users');
    }//END user_list
	
	function user_form() {

		
		//get user group detail
		$myid = $_GET['id'];
		if( $myid != "" )  {
			Mold_user::user_detail($myid);
		}
		
        include_once CORE_PATH . DS . 'casts/header.php';
		$this->display (TEMPLATES_DIR.DS.'common/menu.tpl');
        $this->display (TEMPLATES_DIR.DS.'user_form.tpl' );
		
		//deallocate variables
		Foundry_Memory::dealloc('id');
		Foundry_Memory::dealloc('user_detail');
    }//END user_form
	
	function assign_roles()  {

		Mold_user::get_eactions();
		
        include_once CORE_PATH . DS . 'casts/header.php';
		$this->display (TEMPLATES_DIR.DS.'common/menu.tpl');
        $this->display (TEMPLATES_DIR.DS.'assign_roles.tpl' );
		
		//deallocate variables
		Foundry_Memory::dealloc('eactions');
	}//END assign_roles
	
	function assign_department()  {

		Mold_Department::get_departments();
		
        include_once CORE_PATH . DS . 'casts/header.php';
		$this->display (TEMPLATES_DIR.DS.'common/menu.tpl');
        $this->display (TEMPLATES_DIR.DS.'department/assign_department.tpl' );
		
		//deallocate variables
		Foundry_Memory::dealloc('departments');
	}//END assign_department
	
	function search_user() {
		$keyword = $_REQUEST['term'];
		Mold_User::search_user($keyword);
	}//END search_user
	
}//END User