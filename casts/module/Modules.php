<?php
/**
 * Foundry
 *
 * LICENSE
 *
Copyright 2013 Virtuous Consulting Services

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 * @copyright  Copyright (c) 2013 Virtuous Consulting Services.  (http://www.virtuouscs.com)
 * @Owner     Darley Stephen (http://www.darleys.org)
 * @Author     Darley Stephen (http://www.darleys.org)
 */
class Modules extends Foundry_Local_Broker{
    public function __call($name, $arguments) {
        mold_sae::GA();
        echo "Calling object method '$name' " . implode(', ', $arguments). "\n";
        echo "^^^";
        //echo Foundry_Curl::doCurl(PROCESS_CRMC_LINK."showMeWS@Quotes");
       //echo base64_decode(Foundry_Curl::doCurl(PROCESS_CRMC_LINK."showMeWS@Quotes"));
        //echo "@!!!";


    }
    function module_menu() {

        $user_data = array('id'=>Mold_SAE::GA()['id'],'department' => Mold_SAE::GA()['default_department'], 'role' => Mold_SAE::GA()['default_role'], 'team' => Mold_SAE::GA()['default_team']);
        $iActions = securityGear::getUserSecurity($user_data,'RAW');

        $menuList= Array();
        $iInc=0;

        foreach($iActions as $module => $access) {
            if($iActions[$module]['admin'] == 0) {
                if (!empty($iActions[$module]['Lists']) && $iActions[$module]['Lists'] != 5) {
                    $menuList[$iInc]['link'] = $module;
                    $select_handle = self::$DB->query("SELECT label FROM modules WHERE name ='{$module}'");
                    $module_labels = self::$DB->fetchAll($select_handle);
                    $menuList[$iInc]['title'] = $module_labels[0]['label'];
                    $iInc++;
                }
            }
        }


        include_once CORE_PATH . DS . 'casts/header.php';
        $this->assign("menuList",$menuList);
        $override_dashbody=false;
        if($_REQUEST[0]!=null) {
            $override_dashbody=true;

        }
        $this->assign("override_dashbody",$override_dashbody);

        $this->display (TEMPLATES_DIR.DS.'common/inc_core.tpl');
        $this->display (TEMPLATES_DIR.DS.'common/inc_footer.tpl');
    }//END Fucntion showModulesMenu

    function administration_menu() {

        if(Mold_SAE::GA()['type'] == 3)
        {
            $select_handle = self::$DB->query("SELECT label,name FROM modules WHERE is_admin = 1");
            $modules = self::$DB->fetchAll($select_handle);
            $iInc = 0;
            foreach($modules as $module)
            {
                $menuList[$iInc]['link'] = PROCESS_OPC_LINK . '/' . $module['name'] . '/Lists';
                $menuList[$iInc]['title'] = $module['label'];
                $iInc++;
            }

            include_once CORE_PATH . DS . 'casts/header.php';
            $this->assign("menuList", $menuList);
            $this->display(TEMPLATES_DIR . DS . 'common/inc_core.tpl');
            $this->display(TEMPLATES_DIR . DS . 'common/inc_footer.tpl');
        }
    }//END Fucntion showModulesMenu


    function workshop_menu() {

        if(Mold_SAE::GA()['type'] == 3) {
            $menus = Array('Manage Modules','Manage Fields', 'Manage Layouts','Dropdown Manager','Access');

            $submenus['Manage Modules'] = array('Alter'=>'Alter','Add New'=>'Add New','Relate Modules' => 'Relate Modules');
            $submenus['Access'] = array('Users'=>'User Level','Roles'=>'Role Level','Teams' => 'Team Level','Departments' => 'Department Level');

            $select_handle = self::$DB->query("SELECT label,name FROM modules WHERE is_admin = 0");
            $modules = self::$DB->fetchAll($select_handle);
            foreach($modules as $module)
            {
                $submenus['Manage Fields'][$module['name']] = $module['label'];
                $submenus['Manage Layouts'][$module['name']] = $module['label'];
            }


            $iInc = 0;
            $menuList = Array();

            foreach ($menus as $menu) {
                $menuList[$iInc]['link'] = PROCESS_OPC_LINK . '/workshop/' . str_replace(' ','_',$menu);
                $menuList[$iInc]['title'] = $menu;
                $i=0;
                foreach($submenus[$menu] as $submenu => $label) {
                    $menuList[$iInc]['submenus'][$i]['link'] = PROCESS_OPC_LINK . '/workshop/' . str_replace(' ','_',$menu).'/'. str_replace(' ','_',$submenu);
                    $menuList[$iInc]['submenus'][$i]['title'] = $label;
                    $i++;
                }
                $iInc++;
            }
        }else $menuList = Array();
       // echo '<pre>';
        //print_r($menuList);
        //exit;

        include_once CORE_PATH . DS . 'casts/header.php';
        $this->assign("menuList",$menuList);
        $override_dashbody=false;
        if($_REQUEST[0]!=null) {
            $override_dashbody=true;

        }
        $this->assign("override_dashbody",$override_dashbody);

        $this->display (TEMPLATES_DIR.DS.'common/inc_core.tpl');
        $this->display (TEMPLATES_DIR.DS.'common/inc_footer.tpl');
    }//END Fucntion showModulesMenu




    function opc_manage()	{
        mold_sae::GA();
        $iActions = Mold_Actions::extActions("OPC-MANAGE");
        $opcm_menus=parse_ini_file (dirname(__FILE__).DS."opc-manage.moon");
        $opcmList= Array();
        $iInc=0;
        foreach($iActions as $iAction) {
            if($opcm_menus[trim($iAction[name])]) {
                $iVal= explode(":#:",$opcm_menus[trim($iAction[name])]);
                $opcmList[$iInc]['link']=  $iVal[0];
                $opcmList[$iInc]['title']=  $iVal[1];
                $iInc++;
            }
        }
        include_once CORE_PATH . DS . 'casts/header.php';
        $this->assign("opcmList",$opcmList);
        $this->display (TEMPLATES_DIR.DS.'common/inc_opcm_core.tpl');
        $this->display (TEMPLATES_DIR.DS.'common/inc_footer.tpl');
    }//END Function Dashboard


    function getModule($moduleName) {
       echo $moduleName;
    }
    function module_list() {
		Mold_module::get_modules();
        include_once CORE_PATH . DS . 'casts/header.php';
		$this->display (TEMPLATES_DIR.DS.'common/menu.tpl');
        $this->display (TEMPLATES_DIR.DS.'module/module_list.tpl' );
		
		Foundry_Memory::dealloc('modules');
    }//END module_list
	
	function module_form() {
		Mold_user::checkLoginSession();
		
		$module_id = $_GET['id'];
		Foundry_Memory::set('module_id', $module_id);
		
		//get module detail
		if( $module_id != "" )  {
			Mold_module::module_detail($module_id);
		}
		
        include_once CORE_PATH . DS . 'casts/header.php';
		$this->display (TEMPLATES_DIR.DS.'common/menu.tpl');
        $this->display (TEMPLATES_DIR.DS.'module/module_form.tpl' );
		
		//deallocate variables
		Foundry_Memory::dealloc('module_id');
		Foundry_Memory::dealloc('module_detail');
    }//END module_form
	
	function search_module()  {
		$keyword = $_REQUEST['term'];
		Mold_module::search_module($keyword);
	}//END search_module


}//END Module
