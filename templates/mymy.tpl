<div style="" class="mymy">

        <div class='mymy-login'>
        {if $mymy==false}
            <table>
                <tr>
                    <td>Username  </td>
                    <td>
                        <input type="text" onblur="" maxlength="50" value="" id="signin_username" class='irackit-1-tbox'>
                    </td>
                </tr>
                <tr>
                    <td>Password  </td>
                    <td>
                        <input type="password" onblur="" maxlength="50" value="" id="signin_secret" class='irackit-1-tbox'>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align='center'>
                        <div  id="process_signin" class='irackit-2-button'>SignIn</div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <br>
            <br>
            <table>
                <tr>
                    <td colspan="2" align="center">Forgot Password?</td>
                </tr>
                <tr>
                    <td>Account Email</td>
                    <td>
                        <input type="text" onblur="" maxlength="50" value="" id="signin_email" class='irackit-1-tbox'>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align='center'>
                        <div  id="process_forgot" class='irackit-2-button'>Submit</div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table>
                <tr>
                    <td colspan="2" align="center">Change Password</td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td>
                        <input type="text" onblur="" maxlength="50" value="" id="changePass_username" class='irackit-1-tbox'>
                    </td>
                </tr>
                <tr>
                    <td>Current Password  </td>
                    <td>
                        <input type="password" onblur="" maxlength="50" value="" id="changePass_c_secret" class='irackit-1-tbox'>
                    </td>
                </tr>
                <tr>
                    <td>New Password  </td>
                    <td>
                        <input type="password" onblur="" maxlength="50" value="" id="changePass_n_secret" class='irackit-1-tbox'>
                    </td>
                </tr>
                <tr>
                    <td>New Password (again)  </td>
                    <td>
                        <input type="password" onblur="" maxlength="50" value="" id="changePass_nc_secret" class='irackit-1-tbox'>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align='center'>
                        <div  id="process_changePass" class='irackit-2-button'>Change</div>
                    </td>
                </tr>
            </table>



        {else}
            <span class='login-status'>Logged In as {$mymy['myname']}</span>
        {/if}
        </div>

    <div class="mymy-signup">
        <table>
            <tbody>
            <tr>
                <td>Username</td>
                <td><input type="text" onblur="PROCESS_AJAX_REQ('/request::processor',
						'signup_username','mymy:checkavail','')" maxlength="50" value="" class='irackit-1-tbox' id="signup_username">
                </td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" onblur="PROCESS_AJAX_REQ('/request::processor','signup_secret','gear2:validateSecret','')"
                           maxlength="50" value="" class='irackit-1-tbox' id="signup_secret">
                </td>
            </tr>
            <tr>
                <td>ReEnter</td>
                <td><input type="password" onblur="PROCESS_AJAX_REQ('/request::processor',
						'signup_secret,signup_csecret','gear2:validateCSecret','')" maxlength="50" value="" class='irackit-1-tbox' id="signup_csecret">
                </td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="text" onblur="PROCESS_AJAX_REQ('/request::processor',
						'signup_email','gear2:validateEmail','')" maxlength="50" value="" class='irackit-1-tbox' id="signup_email">
                </td>
            </tr>
            <tr>
                <td>Country</td>
                <td>
                    {$countries}
                </td>
            </tr>
            <tr>
                <td>DOB</td>
                <td>
                    {$years}
                    {$months}
                    {$dates}
                </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                    {$gender}
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <div  id="process_signup" class='irackit-2-button'>Create Account</div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>


</div>


<script language='javascript'>

</script>