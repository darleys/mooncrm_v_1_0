<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Moon CRM</title>
    {$header}
    {Optimizer::psLNKs(array("um"),"JS",1)}
    <script language='javascript'>
        $(function() {
            initMoon();
        });
    </script>
</head>
<body>
<div class="container login-bg">

    <form class="login-form-signin" action="#">
        <div class="login-logo"><img src="images/logo.png"></div>
        <h2 class="login-form-signin-heading">Login Your Account</h2>
        <div class="login-wrap">
            <input type="text" class="form-control" placeholder="User ID" id="username" autofocus="">
            <input type="password" class="form-control" placeholder="Password" id="password">
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>
            <button class="btn btn-lg btn-primary btn-block" id='signin_link' type="submit">Sign in</button>


            <div class="registration">
                Don't have an account yet?
                <a href="index.html">Create an account</a>
            </div>

        </div>



    </form>

</div>
</body>
</html>