<div class="margintop">
	<h3>Users</h3>
    <div class="margintop"><a href="/user_form" class="moon-but-1">Add User</a></div>
    <div class="margintop"><input type="text" placeholder="search" id="search_keyword_user"></div>
    
    <div class="margintop">
        <table border="1" cellspacing="0" cellpadding="5" width="800px;" id="user_list">
            <thead>
                <th>First name</th>
                <th>Last name</th>
                <th>Email</th>
                <th>Action</th>
            </thead>
            
            {$users=Foundry_Memory::get("users")}
            {foreach from=$users item=resource }
                <tr id="user_{$resource['id']}">
                    <td>{$resource['myfname']}</td>
                    <td>{$resource['mylname']}</td>
                    <td>{$resource['myemail']}</td>
                    <td>
                    	<a href="/user_form?id={$resource['id']}">Edit</a> |
                        <a href="javascript:;" class="delete_user" rel="{$resource['id']}">Delete</a>
                    </td>
                </tr>
            {/foreach}
        </table>
        
        <!-- hidden input to set temporary id for deleting record -->
        <input type="hidden" id="id" value="" />
    </div>
</div>