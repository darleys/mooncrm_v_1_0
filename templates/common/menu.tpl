{$dwellers=Foundry_Memory::get("dwellers")}
{$allowed_modules = Foundry_Memory::get('allowed_modules')}

<div id="menu">
    {foreach from=$menuList item=menu}
        <a name="{$menu['link']}" class="moon-menu-link">{$menu['title']}</a>
    {/foreach}
    <a class="moon-menu-link" id="logout_link">Logout</a>
</div>
<div class="clear"></div>