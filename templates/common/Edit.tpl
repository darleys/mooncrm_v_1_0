<script type='text/javascript'>var module='{$module}';</script>
<script type='text/javascript' src='common/special_js/Edit.js'></script>
<div class="panel-body">

    <h3>{if $id==""} Add {else} Edit {/if} {$module}</h3>

    <form action="/{$module}/save" method="post" id="save_form">
        <input type="hidden" id="id" name="id" value="{$id}" />

        <div>
            {foreach $layout as $name }
                    <table cellpadding="5" cellspacing="5" border="0" class="table-bordered">
                    {foreach $name['fields'] as $fields }
                            {if $fields@index is even}<tr>{/if}
                                <th>{Mold_Mis::getFieldLabel($fields['name'],$module)}</th>
                                <td>{Mold_Mis::renderField($fields['name'],$record[$fields['name']], $module,$action)}</td>
                            {if $fields@index is odd}</tr>{elseif $fields@last}</tr>{/if}
                    {/foreach}
                    </table>
            {/foreach}
        </div>
        <br><br>
        <div class="margintop">
            <span class="moon-but-1 btn btn-primary" id="opcsave" name="opcsave">Save</span>
            <a href="{$module}" class='moon-but-1 btn btn-default opc_req'>Back</a>
        </div>
    </form>
</div>