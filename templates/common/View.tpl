<script type='text/javascript'>var module='{$module}';</script>
<script type='text/javascript' src='common/special_js/Edit.js'></script>

<div class="panel-body">

        <h3>{$module} - {Mold_Mis::renderField('name',$record['name'], $module,$action)}</h3>

        <div class="margintop">
            <a href="{$module}/edit" class="opc_req moon-but-1 btn btn-taskA">
                <i class="fa fa-plus"></i>
                Add New</a> |
            <a href="{$module}/edit/{$record['id']}" class="opc_req btn btn-default btn-tumblr bg">
                <i class="fa fa-edit"></i>Edit</a>  |
            <a href="{$module}" class='opc_req btn btn-default'>Back</a>
        </div>

        <div id="tabs" class="margintop">

                {foreach $layout as $name }

                    <table cellpadding="5" cellspacing="5" border="0" class="table-bordered">
                        {foreach $name['fields'] as $fields }
                            {if $fields@index is even}<tr>{/if}
                                    <th>{Mold_Mis::getFieldLabel($fields['name'],$module)}</th>
                                    <td>{Mold_Mis::renderField($fields['name'],$record[$fields['name']], $module,$action)}</td>
                                {if $fields@index is odd}</tr>{elseif $fields@last}</tr>{/if}
                        {/foreach}
                    </table>

                {/foreach}

        </div>
</div>