{if $total_pages > 1}
<div id="opc_pagination" class="opc_pagination">
    {for $i=1; $i<=$total_pages; $i++}
        {if $page_num == $i}
            <a class="active btn btn-pageA" ">{$i}</a>
        {else}
            <a class="btn btn-info" href="{$url}?page_num={$i}">{$i}</a>
        {/if}
    {/for}
    <div class="clear"></div>
</div>
{/if}