<script type='text/javascript'>var module='{$module}';</script>
<script type='text/javascript' src='common/special_js/Lists.js'></script>

<div class="panel-body">
	<h3>{$module}</h3>
    <div class="margintop">
    	    <a class='opc_req moon-but-1 btn btn-taskA' href="{$module}/edit" class="moon-but-1 opc_req">
                <i class="fa fa-plus"></i>
                Add {$module}
            </a>
    </div>
    <div class="margintop">
    	<form action="{$module}" method="post" id="search_form">
            <table border="0" cellspacing="0" cellpadding="5" width="800px;" class="table-bordered">
                    {foreach $search as $name }
                        {if $name@index is even}<tr>{/if}
                        <td>{$i}{Mold_Mis::getFieldLabel($name['name'],$module)}</td><td>{Mold_Mis::renderField($name['name'],$resource[$name['name']], $module,'Search')}</td>
                            {if $name@index is odd}</tr>{elseif $name@last}</tr>{/if}
                    {/foreach}
                <tr>
                    <td><span class="moon-but-1 btn btn-default" id="search">Search</span></td>
                    <td><span class="moon-but-1 btn btn-default" id="reset-search">Clear</span></td>
                </tr>
            </table>
        </form>
    </div>
    <br><br>
    <div class="margintop">
        <table border="1" cellspacing="0" cellpadding="5" width="100%;" class="table-bordered">
            <thead>
            	<th align="left" width="10%"><input type="checkbox" class="check_all" onchange="checkAll(this)" /> Select All</th>
                    {foreach from=$lists item=name }
                        <th>{Mold_Mis::getFieldLabel($name['name'],$module)}</th>
                    {/foreach}
                <th>Action</th>
            </thead>

            {foreach from=$records item=resource }
                <tr id="{$module}_{$resource['id']}" class="{$module}_row">
                	<td><input type="checkbox" class="checkbox_cls" value="{$resource['id']}" /></td>
                        {foreach from=$lists item=name }
                            <td>{Mold_Mis::renderField($name['name'],$resource[$name['name']], $module,$action)}</td>
                        {/foreach}
                    <td>
                            <a href="{$module}/view/{$resource['id']}" id="view_record" class="btn btn-default btn-tumblr bg">
                                <i class="fa fa-eye"></i>
                                View</a> |
                            <a href="{$module}/edit/{$resource['id']}" id="edit_record" class="btn btn-default btn-tumblr bg">
                                <i class="fa fa-edit"></i>
                                Edit</a>
                    </td>
                </tr>
            {/foreach}
        </table>
        <br>
        <!-- pagination links -->
        {include file="common/pagination.tpl" url={$module}}

        <!-- hidden input to set temporary id for deleting record -->
        <input type="hidden" id="id" value="" />

        <input type="hidden" id="total_num" value="{$total_num}" />
    </div>
</div>

<div id="import_dialog" title="Import {$module}" style="display:none;">
	<div class="dialog_padding">
        <form action="{$module}_import" id="import_form" method="post" enctype="multipart/form-data">
            <div>
            	Select File Type :
            	<label><input type="radio" name="fileType" class="fileType" value="xml" checked="checked" /> XML</label>
            	<label><input type="radio" name="fileType" class="fileType" value="csv" /> CSV</label>
            </div>

            <div class="margintop">File : <input type="file" name="importFile" id="importFile" /></div>
            <div class="margintop"><a id="import_submit" class="moon-but-1">Submit</a></div>
        </form>
    </div>
</div>

<div id="export_dialog" title="Export {$module}" style="display:none;">
	<div class="dialog_padding">
        <form action="{$module}_export" id="export_form" method="post">
            <div>Select File Type : </div>
            <div><label><input type="radio" name="fileType" value="XML" checked="checked" /> XML</label></div>
            <div><label><input type="radio" name="fileType" value="CSV" /> CSV</label></div>
            <div class="margintop"><a id="export_submit" class="moon-but-1">Submit</a></div>

            <input type="hidden" name="export_ids" id="export_ids" value="" />
            <input type="hidden" name="search_keyword" id="search_keyword" value="" />
        </form>
    </div>
</div>