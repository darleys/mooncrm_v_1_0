<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Moon CRM</title>

    {$header}
    <script language='javascript'>
        $(function() {
            initMoon();
        });
    </script>
</head>
<body>

<!--Left navbar start-->
<div id="nav">
    <!--logo start-->
    <div class="profile">
        <div class="logo"><a href="/dashboard"><img alt="" src="images/logo.png"></a></div>
    </div>
    <!--logo end-->

    <!--navigation start-->
    <ul class="navigation">
        {foreach from=$menuList key=index item=menu}
                {if $menu['submenus']|@count gt 0}
                    <li class="sub"><a href="" class="moon-menu-link"><span></span>{$menu['title']}</span></a>
                    <ul class="navigation-sub">
                        {foreach from=$menu['submenus'] item=submenu}
                            <li><a href="javascript:void(0);" name="{$submenu['link']}" class="moon-menu-link"><span></span>{$submenu['title']}</span></a></li>
                        {/foreach}
                    </ul>
                    </li>
                {else}
                    <li class="sub"><a href="javascript:void(0);" name="{$menu['link']}" class="moon-menu-link"><span></span>{$menu['title']}</span></a></li>
                {/if}
            </li>
        {/foreach}

        <li class="sub"> <a href="#"><i class="fa fa-smile-o"></i><span>UI Elements</span></a>
            <ul class="navigation-sub">
                <li><a href="buttons.html"><i class="fa fa-power-off"></i><span>Button</span></a></li>
                <li><a href="grids.html"><i class="fa fa-columns"></i><span>Grid</span></a></li>
                <li><a href="icons.html"><i class="fa fa-flag"></i><span>Icon</span></a></li>
                <li><a href="tab-accordions.html"><i class="fa fa-plus-square-o"></i><span>Tab / Accordion</span></a></li>
                <li><a href="nestable.html"><i class="fa  fa-arrow-circle-o-down"></i><span>Nestable</span></a></li>
                <li><a href="slider.html"><i class="fa fa-font"></i><span>Slider</span></a></li>
                <li><a href="timeline.html"><i class="fa fa-filter"></i><span>Timeline</span></a></li>
                <li><a href="gallery.html"><i class="fa fa-picture-o"></i><span>Gallery</span></a></li>
            </ul>
        </li>
        <li class="sub"><a href="#"><i class="fa fa-list-alt"></i><span>Forms</span></a>
            <ul class="navigation-sub">
                <li><a href="form-components.html"><i class="fa fa-table"></i><span>Components</span></a></li>
                <li><a href="form-validation.html"><i class="fa fa-leaf"></i><span>Validation</span></a></li>
                <li><a href="form-wizard.html"><i class="fa fa-th"></i><span>Wizard</span></a></li>
                <li><a href="input-mask.html"><i class="fa fa-laptop"></i><span>Input Mask</span></a></li>
                <li><a href="muliti-upload.html"><i class="fa fa-files-o"></i><span>Multi Upload</span></a></li>
            </ul>
        </li>
        <li class="sub"><a href="#"><i class="fa fa-table"></i><span>Table</span></a>
            <ul class="navigation-sub">
                <li><a href="basic-tables.html"><i class="fa fa-table"></i><span>Basic Table</span></a></li>
                <li><a href="data-tables.html"><i class="fa fa-columns"></i><span>Data Table</span></a></li>
            </ul>
        </li>
        <li class="sub"><a href="#"><i class="fa fa fa-envelope"></i><span>Mail</span></a>
            <ul class="navigation-sub">
                <li><a href="mail.html"><i class="fa fa-inbox"></i><span>Inbox</span></a></li>
                <li><a href="mail-compose.html"><i class="fa fa-envelope-o"></i><span>Compose Mail</span></a></li>
            </ul>
        </li>
        <li class="sub"><a href="#"><i class="fa fa-bar-chart-o"></i><span>Charts</span></a>
            <ul class="navigation-sub">
                <li><a href="jqplot.html"><i class="fa fa-book"></i><span>jQplot</span></a></li>
                <li><a href="morris.html"><i class="fa fa-compass"></i><span>Morris</span></a></li>
                <li><a href="chartjs.html"><i class="fa fa-eraser"></i><span>ChartJS</span></a></li>
            </ul>
        </li>
        <li class="sub"><a href="#"><i class="fa fa-folder-open-o"></i><span>Pages</span></a>
            <ul class="navigation-sub">
                <li><a href="fullcalendar.html"><i class="fa fa-calendar"></i><span>Calendar</span></a></li>
                <li><a href="404-error.html"><i class="fa fa-warning"></i><span>404 Error</span></a></li>
                <li><a href="500-error.html"><i class="fa fa-warning"></i><span>500 Error</span></a></li>
                <li><a href="balnk-page.html"><i class="fa fa-copy"></i><span>Blank Page</span></a></li>
                <li><a href="profile.html"><i class="fa fa-user"></i><span>Profile</span></a></li>
                <li><a href="login.html"><i class="fa fa-sign-out"></i><span>Login</span></a></li>
            </ul>
        </li>
    </ul>
    <!--navigation end-->
</div>
<!--Left navbar end-->

<!--main start-->
<div id="main">
<div class="head-title">
    <div class="menu-switch"><i class="fa fa-bars"></i></div>
    <!--row start-->
    <div class="row">
        <!--col-md-12 start-->
        <div class="col-md-12">
            <!--profile dropdown start-->
            <ul class="user-info pull-right fadeInLeftBig animated">
                <li class="hidden-xs">
                    <input type="text" class="form-control page-search" placeholder=" Search">
                </li>
                <li class="profile-info dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="images/avatar.jpg" alt="" class="img-circle">Scott henderson </a>
                    <ul class="dropdown-menu">
                        <li class="caret"></li>
                        <li> <a href="edit-profile.html"> <i class="fa fa-user"></i> Edit Profile </a> </li>
                        <li> <a href="mail.html"> <i class="fa fa-inbox"></i> Inbox </a> </li>
                        <li> <a href="fullcalendar.html"> <i class="fa fa-calendar"></i> Calendar </a> </li>
                        <li> <a href="login.html"> <i class="fa fa-clipboard"></i> Log Out </a> </li>
                    </ul>
                </li>
                <li class="hidden-xs"><a href="javascript:;" class="toggle-menu menu-right push-body jPushMenuBtn rightbar-switch"><i class="fa fa-bars"></i></a></li>
            </ul><!--profile dropdown end-->

            <!--top nav start-->
            <ul class="nav top-menu hidden-xs notify-row fadeInRightBig animated">

                <li id="moon-opcm-link"> <a href="administration"> <i class="fa fa-microphone"></i>  </a></li>
                <li id="moon-opcm-link"> <a href="workshop"> <i class="fa fa-microphone"></i>  </a></li>
                <!--notification end-->
                <!--task start-->
                <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="fa fa-tasks"></i> <span class="badge bg-success">6</span> </a>
                    <ul class="dropdown-menu extended tasks-bar">
                        <div class="notify-arrow notify-arrow-red"></div>
                        <li>
                            <p class="red">You have 4 pending tasks</p>
                        </li>
                        <li> <a href="#">
                                <div class="task-info">
                                    <div class="desc">Dashboard v1.3</div>
                                    <div class="percent">40%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete</span> </div>
                                </div>
                            </a> </li>
                        <li> <a href="#">
                                <div class="task-info">
                                    <div class="desc">Database Update</div>
                                    <div class="percent">60%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                                </div>
                            </a> </li>
                        <li> <a href="#">
                                <div class="task-info">
                                    <div class="desc">Iphone Development</div>
                                    <div class="percent">87%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 87%"> <span class="sr-only">87% Complete</span> </div>
                                </div>
                            </a> </li>
                        <li> <a href="#">
                                <div class="task-info">
                                    <div class="desc">Mobile App</div>
                                    <div class="percent">33%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 33%"> <span class="sr-only">33% Complete (danger)</span> </div>
                                </div>
                            </a> </li>
                        <li> <a href="#">
                                <div class="task-info">
                                    <div class="desc">Dashboard v1.3</div>
                                    <div class="percent">45%</div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%"> <span class="sr-only">45% Complete</span> </div>
                                </div>
                            </a> </li>
                        <li class="external"> <a href="#">See All Tasks</a> </li>
                    </ul>
                </li><!--task end-->

                <!--message start-->
                <li id="header_inbox_bar" class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="fa fa-envelope-o"></i> <span class="badge bg-important">5</span> </a>
                    <ul class="dropdown-menu extended inbox">
                        <div class="notify-arrow notify-arrow-red"></div>
                        <li>
                            <p class="red">You have 7 new messages</p>
                        </li>
                        <li> <a href="#"> <span class="photo"><img alt="avatar" src="images/avatar2.jpg"></span> <span class="subject"> <span class="from">Jonathan Smith</span> <span class="time">Just now</span> </span> <span class="message"> consectetur adipiscing elit </span> </a> </li>
                        <li> <a href="#"> <span class="photo"><img alt="avatar" src="images/avatar2.jpg"></span> <span class="subject"> <span class="from">John Doe</span> <span class="time">20 mins</span> </span> <span class="message">consectetur adipiscing elit </span> </a> </li>
                        <li> <a href="#"> <span class="photo"><img alt="avatar" src="images/avatar2.jpg"></span> <span class="subject"> <span class="from">Jonathan Smith</span> <span class="time">5 hrs</span> </span> <span class="message"> This is awesome dashboard. </span> </a> </li>
                        <li> <a href="#"> <span class="photo"><img alt="avatar" src="images/avatar2.jpg"></span> <span class="subject"> <span class="from">John Doe</span> <span class="time">Just now</span> </span> <span class="message"> consectetur adipiscing elit </span> </a> </li>
                        <li class="external"> <a href="#">See all messages</a> </li>
                    </ul>
                </li><!--message end-->

                <!--notification start-->
                <li id="header_notification_bar" class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="fa fa-bell-o"></i> <span class="badge bg-warning">7</span> </a>
                    <ul class="dropdown-menu extended notification">
                        <div class="notify-arrow notify-arrow-red"></div>
                        <li>
                            <p class="red">You have 3 new notifications</p>
                        </li>
                        <li> <a href="#"> <span class="label label-danger"><i class="fa fa-bolt"></i></span> Server #3 overloaded. <span class="small italic">34 mins</span> </a> </li>
                        <li> <a href="#"> <span class="label label-warning"><i class="fa fa-bell"></i></span> Server #10 not respoding. <span class="small italic">1 Hours</span> </a> </li>
                        <li> <a href="#"> <span class="label label-danger"><i class="fa fa-bolt"></i></span> Database overloaded 24%. <span class="small italic">4 hrs</span> </a> </li>
                        <li> <a href="#"> <span class="label label-success"><i class="fa fa-plus"></i></span> New user registered. <span class="small italic">Just now</span> </a> </li>
                        <li> <a href="#"> <span class="label label-info"><i class="fa fa-bullhorn"></i></span> Application error. <span class="small italic">10 mins</span> </a> </li>
                        <li class="external"> <a href="#">See all notifications</a> </li>
                    </ul>
                </li><!--notification end-->
            </ul><!--top nav end-->
        </div>
        <!--col-md-12 end-->
    </div>
    <!--row end-->
</div>
<!--margin-container start-->
<div class="margin-container">
    {include file='../common/inc_body.tpl'}
</div>
<!--margin-container end-->
</div>
<!--main end-->
