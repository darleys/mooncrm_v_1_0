<div class="margintop">
	{$myid=Foundry_Memory::get("id")}
    {$user_detail=Foundry_Memory::get("user_detail")}
    
	<h3>{if $myid==""} Add {else} Edit {/if} User</h3>
    
    <div class="form_data">
    	<table cellpadding="5" cellspacing="5">
        	<tr>
                <td>Reports To : </td>
                <td>
                	<input type="text" class="search_user" value="{$user_detail['report_to_name']}" data-callback="user_form" />
				    <input type="hidden" id="reports_to" value="{$user_detail['reports_to']}" />
                </td>
            </tr>
        	<tr>
                <td>First Name : </td>
                <td><input type="text" id="myfname" value="{$user_detail['myfname']}" /></td>
            </tr>
            <tr>
                <td>Last Name : </td>
                <td><input type="text" id="mylname" value="{$user_detail['mylname']}" /></td>
            </tr>
            <tr>
                <td>Email : </td>
                <td><input type="text" id="myemail" value="{$user_detail['myemail']}" /></td>
            </tr>
            <tr>
                <td>Username : </td>
                <td><input type="text" id="myusername" value="{$user_detail['myusername']}" /></td>
            </tr>
            <tr>
                <td>Password : </td>
                <td><input type="text" id="mypassword" value="" /></td>
            </tr>
            <tr>
                <td>Address 1 : </td>
                <td><input type="text" id="myaddress1" value="{$user_detail['myaddress1']}" /></td>
            </tr>
            <tr>
                <td>Address 2 : </td>
                <td><input type="text" id="myaddress2" value="{$user_detail['myaddress2']}" /></td>
            </tr>
            <tr>
                <td>City : </td>
                <td><input type="text" id="mycity" value="{$user_detail['mycity']}" /></td>
            </tr>
            <tr>
                <td>State : </td>
                <td><input type="text" id="mystate" value="{$user_detail['mystate']}" /></td>
            </tr>
            <tr>
                <td>Zip : </td>
                <td><input type="text" id="myzip" value="{$user_detail['myzip']}" /></td>
            </tr>
            <tr>
                <td>Country : </td>
                <td><input type="text" id="mycountry" value="{$user_detail['mycountry']}" /></td>
            </tr>
            <tr>
                <td></td>
                <td>
                	<span id='save_user' class='moon-but-1'>Save</span>
                    <a href="/users" class='moon-but-1'>Back</a>
                </td>
            </tr>
        </table>
        
        <!-- hidden input to pass group id -->
        <input type="hidden" id="id" value="{$myid}" />
    </div>
</div>