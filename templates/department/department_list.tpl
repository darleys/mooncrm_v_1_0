{if  mold_ac::validateAccess(47,$iActions)}
<div class="margintop">
	<h3>Departments</h3>
    <div class="margintop"><a class="opc_req" name="department_form" href="/department_form">Add Department</a></div>
    
    <div class="margintop">
        <table border="1" cellspacing="0" cellpadding="5" width="800px;">
            <thead>
                <th>Name</th>
                <th>Description</th>
                <th>Parent</th>
                <th>Action</th>
            </thead>

            {foreach from=$departments_list item=department }
                <tr id="department_{$department['department_id']}">
                    <td>{$department['name']}</td>
                    <td>{$department['description']}</td>
                    <td>{$department['name']}</td>
                    <td>
                    	<a href="/department_form?id={$department['department_id']}">Edit</a> |
                        <a href="javascript:;" class="delete_department" rel="{$department['department_id']}">Delete</a>
                    </td>
                </tr>
            {/foreach}
        </table>
        
        <!-- hidden input to set temporary department_id for deleting record -->
        <input type="hidden" id="department_id" value="" />
    </div>
</div>
{/if}