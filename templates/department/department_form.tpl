<script type='text/javascript' src='common/special_js/department/department.js'></script>
{if  mold_ac::validateAccess(47,$iActions)}
<div class="margintop">
	{$department_id=Foundry_Memory::get("department_id")}
    {$department_detail=Foundry_Memory::get("department_detail")}
    
	<h3>{if $department_id==""} Add {else} Edit {/if} Department</h3>

    <div id="form_data">
    	<table cellpadding="5" cellspacing="5">
            <tr>
                <td>Name : </td>
                <td><input type="text" id="name" value="{$department_detail['name']}" /></td>
            </tr>
            <tr>
                <td>Description : </td>
                <td><textarea id="description">{$department_detail['description']}</textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                	<span id='save_department' class='btn btn-default btn-tumblr bg' >Save</span>
                    <a href="/departments" class='moon-but-1'>Back</a>
                </td>
            </tr>
        </table>

        <!-- hidden input to pass group id -->
        <input type="hidden" id="department_id" value="{$department_id}" />
    </div>
</div>
{/if}