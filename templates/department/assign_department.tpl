<div class="margintop">
    <label>Select User : </label>
    <input type="text" class="search_user" data-callback="assign_department" />
    <input type="hidden" id="user_id" />
</div>

<h3>Departments</h3>
<div class="margintop"><a id="add_department" class="moon-menu-link">Add Department</a></div>

<div class="margintop">
    <table cellpadding="5" id="user_departments">
        <tr>
            <td>
            	<select class="department_id">
                    <option value="">Select Department</option>
                    
                    {$departments=Foundry_Memory::get("departments")}
                    {foreach from=$departments item=resource }
                        <option value="{$resource['department_id']}">{$resource['name']}</option>
                    {/foreach}
                </select>
            </td>
            <td><a href="javascript:;" class="remove_department">Remove</a></td>
        </tr>
        <tr class="save_btns">
            <td colspan="2">
                <span id='save_departments' class='moon-but-1'>Save</span>
            </td>
        </tr>
    </table>
    
    <!-- hidden inputs to store department ids -->
    <input type="hidden" id="department_ids" />
</div>