<script type='text/javascript'>var module='workshop/Manage_Fields';</script>
<script type='text/javascript' src='common/special_js/Lists.js'></script>
<script type='text/javascript' src='common/special_js/Edit.js'></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language='javascript'>
    function openURL(url1) {
            $.ajax({
                crossDomain:true,
                type: 'POST',
                url: url1,
                dataType: "html"
            }).done(function( html ) {
                $( ".body-content" ).html( html );
            });
    }

    $(function() {
        $( "#accordion" ).accordion();
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();

        $("#name").datepicker();
    });

</script>

<div class="panel-body">

    <h3>Manage Fields - {$module}</h3>
    <a href="javascript:void(0);" class="moon-menu-link btn btn-info" onclick="openURL('{$smarty.const.PROCESS_OPC_LINK}/workshop/Manage_Fields/Add_New/{$module}')">Add New Field</a>
    <div id="accordion">
        {foreach from=$records item=resource }
            <h3>{$resource['name']}</h3>
                <div><form action="{$module}/save" method="post" id="save_form_{$resource['id']}">
                    <table class="table-bordered" width="100%" cellspacing="0" cellpadding="5" border="0">
                        <tr><td>FIELD : </td><td> <b>{$resource['name']}</b><input type="hidden" id="name" name="name" value="{$resource['name']}"></td></tr>
                        <tr><td>LABEL :</td><td> <input type="text" id="label" name="label" value="{$resource['label']}"></td></tr>
                        <tr><td>TYPE :</td><td> <b>{$resource['type']}</b><input type="hidden" id="type" name="type" value="{$resource['type']}"></td></tr>
                        {if $resource['type'] eq 'multiselect' || $resource['type'] eq 'dropdown'}
                            <tr><td>DROPDOWN-OPTION: </td><td>
                                {html_options name=extra1 options=$dropdowns selected={$resource['extra1']}}
                            </td></tr>
                            <tr><td>DEFAULT-VALUE :</td><td>
                                    {html_options name=default_value options=$dropdown_values[{$resource['extra1']}] selected={$resource['default_value']}}
                               </td></tr>
                        {elseif $resource['type'] eq 'relation'}
                            <tr><td>MODULE : </td><td> <b>{$resource['extra1']}</b></td></tr>
                        {else}
                            <tr><td>DEFAULT-VALUE : </td><td> <input type="text" id="default_value" name="default_value" value="{$resource['default_value']}"></td></tr>
                        {/if}

                        <tr><td> REQUIRED : </td><td>
                            {if $resource['required'] eq 1}
                                <input id="required" type="checkbox" name="required" checked="true" value="1">
                            {else}
                                <input id="required" type="checkbox" name="required"  value="1">
                            {/if}
                        </td></tr>
                       <tr><td> IS ACTIVE ? : </td><td>
                            {if $resource['is_active'] eq 1}
                                <input id="is_active" type="checkbox" name="is_active" checked="true" value="1">
                            {else}
                                <input id="is_active" type="checkbox" name="is_active"  value="1">
                            {/if}
                        </td></tr>
                        <tr><td> DELETE : </td><td>
                                <input id="delete" type="checkbox" name="delete"  value="1">
                        </td></tr>
                        <tr><td><input type="hidden" id="id" name="id" value="{$resource['id']}"><input type="hidden" id="module" name="module" value="{$module}">
                            </td><td><span class="moon-but-1 btn btn-primary" id="opcsave[{$resource['id']}]" name="opcsave[{$resource['id']}]" onclick="form_post({$resource['id']})">Save</span></td>
                        </tr>
                    </table>
                    </form></div>

        {/foreach}

        <!-- pagination links -->
        {include file="common/pagination.tpl" url="workshop/Manage_Fields/$module"}
        <input type="hidden" id="total_num" value="{$total_num}" />
    </div>
</div>
