<script type='text/javascript'>var url="workshop/Dropdown_Manager/New";</script>
<script type='text/javascript' src='common/special_js/Edit.js'></script>
<script type='text/javascript' src='common/special_js/workshop.js'></script>

<div class="panel-body">

    <h3>Manage Dropdowns</h3>
    {if $errors|@count gt 0}
        <div class="error-box">
            <ul>
            {foreach $errors as $error }
                <li>{$error}</li>
            {/foreach}
                </ul>
        </div>
    {/if}

    <form action="" method="post" id="save_form">
        <table cellpadding="8" cellspacing="5" border="0" class="table-bordered" width="100%">
            <tr><td>New Dropdown</td><td><input type="text" value="" id="title" name="title"></td></tr>
            <tr><td colspan="2"><span class="moon-but-1 btn btn-primary" id="opcpost" name="opcpost">Create Dropdown</span></td></tr>
        </table>
    </form>

    <br><br>

    <table cellpadding="8" cellspacing="5" border="0" class="table-bordered" width="100%">
            <tr>
            <td>Dropdowns</td>
            <td>
                <select id="dropdowns" name="dropdowns" onchange="getDropdownOptions(this.value)">
                    {html_options options=$dropdowns}
                </select>
            </td></tr>
            <tr><td colspan="2">
                    <form action="" method="post" id="save_form_Add_Option">
                    <table cellpadding="8" cellspacing="5" border="0"  width="100%" id="dropdown_values">
                        </table>
                    </form>
            </td></tr>
    </table>


</div>