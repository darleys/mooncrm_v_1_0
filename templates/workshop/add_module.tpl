<script type='text/javascript'>var module='workshop/{$module}';</script>
<script type='text/javascript' src='common/special_js/Edit.js'></script>

<div class="panel-body">

    <h3>Add New Module</h3>

    <form action="{$module}/save" method="post" id="save_form">
        <input type="hidden" id="id" name="id" value="{$id}" />

        <div>

            <div class="margintop">
                    <table cellpadding="8" cellspacing="5" border="0" class="table-bordered" width="100%">
                        <tr>
                        <th>Module Name:</th>
                        <td><input type="text" value="" id="name" name="name"></td>
                        </tr>
                        <tr>
                            <th>Label:</th>
                            <td><input type="text" value="" id="label" name="label"></td>
                        </tr>
                        <tr>
                        <th>Description:</th>
                        <td><textarea id="description" name="description"></textarea></td>
                        </tr>
                        <tr>
                            <th>Is Active:</th>
                            <td><input id="is_active" type="checkbox" name="is_active" value="1"></td>
                        </tr>
                        <tr>
                            <th>Module Type :</th>
                            <td><select id="is_admin" name="is_admin">
                                    <option value="0">Custom</option>
                                    <option value="1">Admin</option>
                                    <option value="2">Part Custom</option>
                                </select>
                            </td></tr>
                        <tr>
                        <th>Actions :</th>
                            <td><select MULTIPLE id="actions" name="actions[]">
                                {html_options options=$actions}
                            </select>
                        </td></tr>
                    </table>
            </div>
        </div>
        <br>
        <div class="margintop">
            <span class="moon-but-1 btn btn-primary" id="opcsave" name="opcsave" url="workshop/Manage_Modules/Save">Save</span>
            <a href="{$module}" class='moon-but-1 btn btn-info opc_req'>Back</a>
        </div>
    </form>
</div>