<script type='text/javascript'>var module='workshop/Access';</script>
<script type='text/javascript' src='common/special_js/workshop/access.js'></script>

<div class="panel-body">

<form action="" method="post" id="get_Access">
    <input type="hidden" id="accessType" name="accessType" value="{$accessType}">
    <select name='accessList' id='accessList' onchange="getAccess(this);">
        {html_options options=$accessList selected=$accessListID}
    </select>
</form>
<br>
{if $accessListID neq ''}
    {if empty($security_list)}
        <h4>NO CURRENT ACCESS GIVEN</h4>
        {if $accessType eq 'Users'}
            <h4>
                Currently using <a class="opc_req" href="workshop/Access/GetAccess/{$current_access['type']}s/{$current_access['id']}"> {$current_access['type']} Level Access </a>
                <span class="moon-but-1 btn btn-info"><a class="opc_req" href="workshop/Access/CopyAccess/{$current_access['type']}s/{$current_access['id']}/{$accessListID}"> Copy {$current_access['type']} Access To User </a></span>
            </h4>
        {/if}
    {else}
        <a class="opc_req moon-but-1 btn btn-info" href="workshop/access/revoke/{$accessType}/{$accessListID}">Revoke Access</a>
        <br>
        {if $accessType neq 'Users'}
            {if $users|@count gt 1}
                <h4>Currently Users using this security</h4>
                <select name='usersList' id='usersList' onchange="getUserAccess(this);">
                    {html_options options=$users}
                </select>
            {else}
                <h4>Currently No Users are using this security</h4>
            {/if}
        {/if}
    {/if}
    <br><br>
    <form action="" method="post" id="save_form_access">
        <table border="0" cellspacing="0" cellpadding="5" width="800px;" class="table">
            {foreach from=$modules key=mid item=module}
                <tr>
                    <th>
                        {$module}
                        <input type="hidden" id="module_id[]" name="mid[]" value="{$mid}">
                        <input type="hidden" id="accessListID" name="accessListID" value="{$accessListID}">
                        <input type="hidden" id="accessType" name="accessType" value="{$accessType}">
                    </th>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="5" width="100%;">
                            {foreach from=$module_actions[$mid] key=maid item=maction}
                                <tr><td>{$actions[$maction]}<input type="hidden" id="maction{$mid}[]" name="maction{$mid}[]" value="{$maction}"></td>
                                    <td>
                                        <select name="levels{$mid}[]" id="levels{$mid}[]">
                                            {if $security_list[$mid][$maction] eq ''}
                                                {html_options options=$security_levels selected=5}
                                            {else}
                                                {html_options options=$security_levels selected={$security_list[$mid][$maction]}}
                                            {/if}
                                        </select>
                                    </td></tr>
                            {/foreach}
                        </table>
                    </td>
                </tr>
            {/foreach}
            <tr>
                <td><span class="moon-but-1 btn btn-primary" id="opcsave" name="opcsave" onclick="form_post('access')">Save</span></td>
            </tr>
        </table>
    </form>
{/if}
</div>