
<script>
    $(function() {
        $( "#accordion" ).accordion();
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
    });
    function form_post(id)
    {
        submitFormToOPCAjax('workshop/Manage_Layouts/Save', id);
    }
</script>
<div class="panel-body">

<div id="accordion">
    {if $layouts->Search eq 1}
        <h3>Search Layout</h3>
        <div>
            <form action="" method="post" id="save_form_Search_Layout">
                <table class="table-bordered" border="0" cellspacing="0" cellpadding="5" width="800px;">
                    <tr><th>Field Name</th><th>Order</th></tr>
                    {foreach from=$layouts->Search item=resource }
                            <tr>
                                    <td>{$resource->name}</td>
                                    <td>ORDER :<br> <input type="text" id="sort_order[]" name="sort_order[]" value="{$resource->sort_order}">
                                    <input type="hidden" id="id[]" name="id[]" value="{$resource->id}">
                                    </td>
                            </tr>
                    {/foreach}
                    <tr><td><input type="hidden" id="module" name="module" value="{$module}">
                            <input type="hidden" id="layout" name="layout" value="Search">
                            <span class="moon-but-1 btn btn-primary" id="Search_Layout" name="opcsave" onclick="form_post('#save_form_Search_Layout')">Save</span></td>
                        <td>&nbsp;</td></tr>
                </table>
            </form>
        </div>
    {/if}
    {if $layouts->Lists eq 1}
        <h3>List Layout</h3>
        <div class="margintop">
            <form action="" method="post" id="save_form_Lists_Layout">
                <table class="table-bordered" border="0"  cellspacing="0" cellpadding="5" width="800px;">
                    <tr><th>Field Name</th><th>Order</th></tr>
                    {foreach from=$layouts->Lists item=resource }
                        <tr>
                            <td>{$resource->name}</td>
                            <td>ORDER :<br> <input type="text" id="sort_order[]" name="sort_order[]" value="{$resource->sort_order}">
                                <input type="hidden" id="id[]" name="id[]" value="{$resource->id}">
                            </td>
                        </tr>
                    {/foreach}
                    <tr><td><input type="hidden" id="module" name="module" value="{$module}">
                            <input type="hidden" id="layout" name="layout" value="Lists">
                            <span class="moon-but-1 btn btn-primary" id="Lists_Layout" name="opcsave" onclick="form_post('#save_form_Lists_Layout')">Save</span></td>
                        <td>&nbsp;</td></tr>
                </table>
            </form>
        </div>
    {/if}
    {if $layouts->Edit eq 1}
        <h3>Edit Layout</h3>
        <div class="margintop">
            <form action="" method="post" id="save_form_Edit_Layout">
                <table class="table-bordered" border="0"  cellspacing="0" cellpadding="5" width="800px;">
                    <tr><th>Field Name</th><th>Order</th></tr>
                    {foreach from=$layouts->Edit item=resource }
                        <tr>
                            <td>{$resource->name}</td>
                            <td>ORDER :<br> <input type="text" id="sort_order[]" name="sort_order[]" value="{$resource->sort_order}">
                                <input type="hidden" id="id[]" name="id[]" value="{$resource->id}">
                            </td>
                        </tr>
                    {/foreach}
                    <tr><td><input type="hidden" id="module" name="module" value="{$module}">
                            <input type="hidden" id="layout" name="layout" value="Edit">
                            <span class="moon-but-1 btn btn-primary" id="Edit_Layout" name="opcsave" onclick="form_post('#save_form_Edit_Layout')">Save</span></td>
                        <td>&nbsp;</td></tr>
                </table>
            </form>
        </div>
    {/if}
    {if $layouts->View eq 1}
        <h3>Details Layout</h3>
        <div class="margintop">
            <form action="" method="post" id="save_form_View_Layout">
                <table class="table-bordered" border="0"  cellspacing="0" cellpadding="5" width="800px;">
                    <tr><th>Field Name</th><th>Order</th></tr>
                    {foreach from=$layouts->View item=resource }
                        <tr>
                            <td>{$resource->name}</td>
                            <td>ORDER :<br> <input type="text" id="sort_order[]" name="sort_order[]" value="{$resource->sort_order}">
                                <input type="hidden" id="id[]" name="id[]" value="{$resource->id}">
                            </td>
                        </tr>
                    {/foreach}
                    <tr><td><input type="hidden" id="module" name="module" value="{$module}">
                            <input type="hidden" id="layout" name="layout" value="View">
                            <span class="moon-but-1 btn btn-primary" id="View_Layout" name="opcsave" onclick="form_post('#save_form_View_Layout')">Save</span></td>
                        <td>&nbsp;</td></tr>
                </table>
            </form>
        </div>
    {/if}
    {if $layouts->Export eq 1}
        <h3>Export Layout</h3>
        <div class="margintop">
            <form action="" method="post" id="save_form_View_Layout">
                <table class="table-bordered" border="1" cellspacing="0" cellpadding="5" width="800px;">
                    <tr><th>Field Name</th><th>Order</th></tr>
                    {foreach from=$layouts->Export item=resource }
                        <tr>
                            <td>{$resource->name}</td>
                            <td>ORDER :<br> <input type="text" id="sort_order[]" name="sort_order[]" value="{$resource->sort_order}">
                                <input type="hidden" id="id[]" name="id[]" value="{$resource->id}">
                            </td>
                        </tr>
                    {/foreach}
                    <tr><td><input type="hidden" id="module" name="module" value="{$module}">
                            <input type="hidden" id="layout" name="layout" value="View">
                            <span class="moon-but-1 btn btn-primary" id="View_Layout" name="opcsave" onclick="form_post('#save_form_View_Layout')">Save</span></td>
                        <td>&nbsp;</td></tr>
                </table>
            </form>
        </div>
    {/if}
    {if $layouts->Import eq 1}
        <h3>Import Layout</h3>
        <div class="margintop">
            <form action="" method="post" id="save_form_View_Layout">
                <table class="table-bordered" border="0"  cellspacing="0" cellpadding="5" width="800px;">
                    <tr><th>Field Name</th><th>Order</th></tr>
                    {foreach from=$layouts->Import item=resource }
                        <tr>
                            <td>{$resource->name}</td>
                            <td>ORDER :<br> <input type="text" id="sort_order[]" name="sort_order[]" value="{$resource->sort_order}">
                                <input type="hidden" id="id[]" name="id[]" value="{$resource->id}">
                            </td>
                        </tr>
                    {/foreach}
                    <tr><td><input type="hidden" id="module" name="module" value="{$module}">
                            <input type="hidden" id="layout" name="layout" value="View">
                            <span class="moon-but-1 btn btn-primary" id="View_Layout" name="opcsave" onclick="form_post('#save_form_View_Layout')">Save</span></td>
                        <td>&nbsp;</td></tr>
                </table>
            </form>
        </div>
    {/if}
</div>
</div>