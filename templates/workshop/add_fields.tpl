<script type='text/javascript'>var module='workshop/Manage_Fields';</script>
<script type='text/javascript' src='common/special_js/Edit.js'></script>

<div class="panel-body">

    <h3>Add New Field - {$module}</h3>

    <form action="{$module}/save" method="post" id="save_form">
        <input type="hidden" id="id" name="id" value="{$id}" />

            <div>
                <table cellpadding="5" cellspacing="5" border="0" class="table-bordered">
                    <tr>
                        <th>Field Name:</th>
                        <td><input type="text" value="" id="name" name="name"></td>
                    </tr>
                    <tr>
                        <th>Label:</th>
                        <td><input type="text" value="" id="label" name="label"></td>
                    </tr>
                    <tr>
                        <th>Type:</th>
                        <td>{html_options name=type id=type options=$types onchange='renderExtraField()'}</td>
                    </tr>
                    <tr>
                        <th>Required :</th>
                        <td><input id="required" type="checkbox" name="required"  value="1"></td>
                    </tr>
                    <tr class="extra1_module" style="display:none;">
                        <th>Module:</th>
                        <td>{html_options name=extra1_module id=extra1_module options=$modules}</td>
                    </tr>
                    <tr class="extra1_ddlist" style="display:none;">
                        <th>DropDown List:</th>
                        <td>{html_options name=extra1_ddlist id=extra1_ddlist options=$dropdown_lists}</td>
                    </tr>
                    <tr id="default_tr" style="display:none;">
                        <th id="default_th"></th>
                        <td id="default_td"></td>
                    </tr>
                </table>
            </div>

        <br>
        <div class="margintop">
            <input type="hidden" id="module" name="module" value="{$module}">
            <span class="moon-but-1 btn btn-primary" id="opcsave" name="opcsave" url="workshop/Manage_Fields/Save">Save</span>
            <a href="workshop/Manage_Fields/{$module}" class='moon-but-1 opc_req btn btn-info'>Back</a>
        </div>
    </form>
</div>