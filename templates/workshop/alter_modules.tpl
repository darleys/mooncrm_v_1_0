<script type='text/javascript'>var module='workshop/{$module}';</script>
<script type='text/javascript' src='common/special_js/Lists.js'></script>
<script type='text/javascript' src='common/special_js/Edit.js'></script>

<div class="panel-body">
    <h3>Manage Modules</h3>

    <div class="panel-body">
        {foreach from=$records item=resource }
                <form action="{$module}/save" method="post" id="save_form_{$resource['id']}">
                    <table border="1" cellspacing="0" cellpadding="3" class="table">
                        <td>NAME :<br> <b>{$resource['name']}</b><input type="hidden" id="name" name="name" value="{$resource['name']}"></td>
                        <td>LABEL :<br> <input type="text" id="label" name="label" value="{$resource['label']}"></td>
                        <td>DESCRIPTION :<br> <textarea value="" id="description" name="description">{$resource['description']}</textarea></td>
                        <td> IS ACTIVE :<Br>
                            {if $resource['is_active'] eq 1}
                                <input id="is_active" type="checkbox" name="is_active" checked="true" value="1">
                            {else}
                                <input id="is_active" type="checkbox" name="is_active"  value="1">
                            {/if}
                        </td>
                        <td> DELETE :<Br>
                            <input id="delete" type="checkbox" name="delete"  value="1">
                        </td>
                        <td>ACTIONS :<br> <select MULTIPLE id="actions" name="actions[]">
                                {html_options options=$actions selected=$modules_actions[$resource['id']]}
                            </select></td>
                        <td>MODULE SHORTCUTS:<br> <select MULTIPLE id="module_shortcuts" name="module_shortcuts[]">
                                {html_options options=$modules}
                            </select></td>
                        <td><input type="hidden" id="id" name="id" value="{$resource['id']}">
                        <span class="moon-but-1 btn btn-primary" id="opcsave[{$resource['id']}]" name="opcsave[{$resource['id']}]" onclick="form_post({$resource['id']})">Save</span></td>
                    </table>
                </form>
        {/foreach}

        <!-- pagination links -->
        {include file="common/pagination.tpl" url='workshop/Manage_Modules/Alter'}
        <input type="hidden" id="total_num" value="{$total_num}" />
    </div>
</div>

