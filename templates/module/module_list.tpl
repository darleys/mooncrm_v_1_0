<div class="margintop">
	<h3>Modules</h3>
    <div class="margintop"><a href="/module_form" class="moon-but-1">Add Module</a></div>
    
    <div class="margintop">
        <table border="1" cellspacing="0" cellpadding="5" width="800px;">
            <thead>
                <th>Name</th>
                <th>Description</th>
                <th>Action</th>
            </thead>
            
            {$modules=Foundry_Memory::get("modules")}
            {foreach from=$modules item=resource }
                <tr id="module_{$resource['module_id']}">
                    <td>{$resource['name']}</td>
                    <td>{$resource['description']}</td>
                    <td>
                    	<a href="/module_form?id={$resource['module_id']}">Edit</a> | 
                        <a href="javascript:;" class="delete_module" rel="{$resource['module_id']}">Delete</a>
                    </td>
                </tr>
            {/foreach}
        </table>
        
        <!-- hidden input to set temporary module_id for deleting record -->
        <input type="hidden" id="module_id" value="" />
    </div>
</div>