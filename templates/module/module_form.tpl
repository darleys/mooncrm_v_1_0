<div class="margintop">
	{$module_id=Foundry_Memory::get("module_id")}
    {$module_detail=Foundry_Memory::get("module_detail")}
    
	<h3>{if $module_id==""} Add {else} Edit {/if} Module</h3>
    
    <div class="form_data">
    	<table cellpadding="5" cellspacing="5">
            <tr>
                <td>Name : </td>
                <td><input type="text" id="name" value="{$module_detail['name']}" /></td>
            </tr>
            <tr>
                <td>Description : </td>
                <td><textarea id="description">{$module_detail['description']}</textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                	<span id='save_module' class='moon-but-1'>Save</span>
                    <a href="/modules" class='moon-but-1'>Back</a>
                </td>
            </tr>
        </table>
        
        <!-- hidden input to pass group id -->
        <input type="hidden" id="module_id" value="{$module_id}" />
    </div>
</div>