<div class="margintop">
	<h3>Assign Roles</h3>
    
	<div class="form_data">
    	<table cellpadding="5" cellspacing="5">
        	<tr>
                <td>Select User : </td>
                <td>
                	<input type="text" class="search_user" data-callback="assign_roles" />
				    <input type="hidden" id="user_id" />
                </td>
            </tr>
        	<tr>
                <td valign="top">Eactions : </td>
                <td>
                	<select id="eactions" multiple="multiple" style="height:150px;">
                        {$eactions=Foundry_Memory::get("eactions")}
                        {foreach from=$eactions item=resource }
                            <option value="{$resource['id']}">{$resource['name']}</option>
                        {/foreach}
                    </select>
               	</td>
            </tr>
            <tr>
                <td></td>
                <td>
                	<span id='save_user_roles' class='moon-but-1'>Save</span>
                </td>
            </tr>
        </table>
   </div>
</div>